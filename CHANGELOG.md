# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Output json file containing newick tree and snp matrix results

### Changed

- Python package dependency from cgelib to cgecore >= 2.0.0

## [1.6.1] - 2024-09-02

### Changed

- The location of the Cromwell log and execution directory is now inside the temporary directy ("tmp/" per default).

## [1.6.0] - 2024-08-27

### Added

- Dockerfile and uploaded Docker image. Along with the web version this will currently be the only supported way of running CSI Phylogeny.

### Changed

- CSI Phylogeny now always creates both a traditional SNP matrix and a pairwise SNP matrix.
- Type and looks of all plots.
- How plots are created. In earlier version, an R script was used to create plots. In this version a new python module (plots) create all plots using matplotlib, seaborn and polars.
- BWA to version 0.7.18
- Mummer to version 3.23
- FastTree to version 2.1.11
- BedTools to version 2.30.0
