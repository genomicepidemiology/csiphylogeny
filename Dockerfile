FROM broadinstitute/cromwell:88-ee35a96

ENV INSIDE_DOCKER=true
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -qq; \
    apt-get install --no-install-recommends -y -qq git \
    build-essential \
    libncurses5-dev \
    libncursesw5-dev \
    unzip \
    wget \
    libz-dev \
    procps \
    python3 \
    python3-pip \
    libstatistics-descriptive-perl \
    libbio-alignio-stockholm-perl \
    bedtools \
    fasttree \
    ; \
    rm -rf /var/cache/apt/* /var/lib/apt/lists/*;

# Uninstall samtools from base image
RUN apt-get remove -y -qq samtools; \
    rm -rf /var/cache/apt/* /var/lib/apt/lists/*;

# Install cgelib
COPY ./requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt;

# Move cromwell to /docker_app and prepare directory structure.
# This is necessary as all other CGE tools are created to be run from the /app
# directory, which is not possible if the tools is also installed inside /app.
# Which they are per default in the base image.
RUN mv /app /docker_app; \
    mkdir /app; \
    cd /docker_app; \
    rm cromwell.jar; \
    ln -s cromwell-* cromwell.jar; \
    cd /usr/bin; \
    ln -s /docker_app/cromwell.jar cromwell; \
    cd /docker_app;

RUN git clone -b v0.7.18 https://github.com/lh3/bwa.git; \
    cd bwa; \
    make; \
    mv bwa /usr/bin/; \
    cd ..; rm -rf bwa;

RUN wget https://sourceforge.net/projects/samtools/files/samtools/0.1.18/samtools-0.1.18.tar.bz2/download; \
    tar -xjf download; \
    cd samtools-0.1.18; \
    make; \
    mv samtools /usr/bin/; \
    mv bcftools/bcftools /usr/bin/; \
    mv bcftools/vcfutils.pl /usr/bin/; \
    cd ..; rm -rf download samtools-0.1.18/;

RUN wget https://sourceforge.net/projects/mummer/files/mummer/3.23/MUMmer3.23.tar.gz/download; \
    tar -xzf download; \
    cd MUMmer3.23; \
    make; \
    mv nucmer /usr/bin/; \
    mv delta-filter /usr/bin/; \
    mv show-coords /usr/bin/; \
    mv show-snps /usr/bin/; \
    cd ..; rm -rf download;

RUN mkdir /docker_app/csiphylogeny;
COPY . /docker_app/csiphylogeny
RUN cd /docker_app; ln -s csiphylogeny/csi_phylogeny.py csiphylogeny.py;

WORKDIR /app

# Execute program when running the container
ENTRYPOINT ["python3", "/docker_app/csiphylogeny.py", "--cromwell_path", "/docker_app/cromwell.jar", "--cromwell_cfg", "/docker_app/csiphylogeny/cromwell.cfg"]