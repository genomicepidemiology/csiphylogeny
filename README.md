# README

**important**: We will only provide support for running CSI Phylogeny using Docker with the published image in [dockerhub](https://hub.docker.com/) or through the web interface [CSI Phylogeny web interface](https://cge.food.dtu.dk/services/CSIPhylogeny/).
The dependencies for running CSI Phylogeny locally without docker can be a little complex and can be done in many ways. We have described how to do it in a general way, but we are are small team, and do not have the resources to help everybody with their individual setups. If you want to help improve this README, then you are of cause more than welcome to create a pull request or and issue in the issue tracker.

## General usage

### Required options

```

  -i FAST(Q|A) [FAST(Q|A) ...], --input_files FAST(Q|A) [FAST(Q|A) ...]
                        Assemblies in FASTA format or paired/single-end raw
                        data.
  -o PATH, --output_dir PATH
                        The results of the workflow will be written to this
                        directory.
  -r PATH, --reference PATH
                        Path to fasta file to be used as reference for
                        alignments.

EXAMPLE (docker):
docker run -v "$(pwd):/app" genomicepidemiology/csiphylogeny:latest -i data/cluster01/mcr1_* -r data/cluster01/ref.fa -o csi_run

EXAMPLE (non-docker)
python /path/to/csiphylogeny/csi_phylogeny.py -i data/cluster01/mcr1_* -r data/cluster01/ref.fa -o csi_run

```

### Popular options

```

  -d INT, --depth INT   Required depth for SNP calling. Default: 10.
  
  -e INT, --relative_depth INT
                        Required depth relative to average depth for SNP
                        calling (in pct). If for some sample the relative
                        depth results in a threshold lower than the thresshold
                        specified for depth, then the higher threshold for
                        depth will be used instead. Default: 10.
  
  -p INT, --prune INT   Minimum required distance between SNPs. This option
                        also sets --prune_assembly and --prune_raw.Default:
                        10.
  
  -x INT, --mapping_qual INT
                        Minimum required mapping quality for reads.Default:
                        25.
  
  -y INT, --snp_qual INT
                        Minimum required SNP quality. Default: 30.
  
  -z FLOAT, --z_score FLOAT
                        Required Z score for SNPs. Default: 1.96.
  
  -t, --ignore_heterozygous
                        If set, will ignore heterozygous SNPs.

```

### Less used options

```

  --temp_dir PATH       Files needed while running the application is stored
                        in this directory. It can be deleted after execution.
                        Default "tmp" directory within output directory.

  --clean               If set, will not use previously calculated results.

  --prune_raw INT       Minimum required distance between SNPs found using
                        alignment of reads. Overwrites --prune option if
                        set.Default: Same as --prune.

  --prune_assembly INT  Minimum required distance between SNPs. using
                        alignment of contigs. Overwrites --prune option if
                        set.Default: Same as --prune.

  -s INT, --end_distance INT
                        SNP minimum distance from contig end. Default: 0.

  -l INT, --min_length INT
                        Only call SNPs in contigs of at least this length.
                        Default: 0.

  --timeout INT         Maximum time (sec) the job should run before it is
                        killed. Default: 86400 (24 hours).

  --force_neighbors     If issues are encountered due to read headers being too
                        similar, then enabling forced neighbors may solve the
                        issue as it requires alle pairs to be "alphabetical"
                        neighbors.

```

### Options only relevant for running without Docker

Note that all these and most of the previous options can also be set using environment variables. Pleas see the "Local Installation" section.

```

  --cromwell_cfg PATH   Path to cromwell configuration file. If not set
                        Cromwell will run directly on the current system.
  --aligner_path PATH   Path of alignment application. Not needed if
                        application is in your PATH and has the aligner name.
  --samtools_path PATH  Path of samtools application. Not needed if
                        application is in your PATH
  --vcfutils_path PATH  Path of vcfutils.pl application. Not needed if
                        application is in your PATH
  --bcftools_path PATH  Path of bcftools application. Not needed if
                        application is in your PATH
  --genomeCoverageBed_path PATH
                        Path of genomeCoverageBed application. Not needed if
                        application is in your PATH
  --perl_path PATH      Path of perl interpreter. Not needed if interpreter is
                        in your PATH and named "perl"
  --fasttree_path PATH  Path of FastTree application. Not needed if
                        application is in your PATH
  --nucmer_path PATH    Path of nucmer application. Not needed if application
                        is in your PATH
  --show_snps_path PATH
                        Path of show_snps application. Not needed if
                        application is in your PATH
  --show_coords_path PATH
                        Path of show_coords application. Not needed if
                        application is in your PATH
  --delta_filter_path PATH
                        Path of delta_filter application. Not needed if
                        application is in your PATH
  --cromwell_path PATH  Path of cromwell jar file. Not needed if "cromwell" is
                        in your PATH
  --java_path PATH      Path of java. Not needed if java is in your PATH
  --force_neighbors     If issues are encountered due to read headers being to
                        similar, then enabling forced neighbors may solve the
                        issue as it requires alle pairs to be "alphabetical"
                        neighbors.

```

## Using Docker

If you want to run CSI Phylogeny locally, it is strongly recommended to run it using Docker and the official image published at docker hub: [CSI Phylogeny docker hub image](https://hub.docker.com/r/genomicepidemiology/csiphylogeny). If you run in to any issues with the image please create an issue in the issue tracker.

```bash

# You can start by pulling the image
docker pull genomicepidemiology/csiphylogeny:latest

# Or you can simply run it and it will pull automatically
docker run -v "$(pwd):/app" genomicepidemiology/csiphylogeny:latest -i data/cluster01/mcr1_* -r data/cluster01/ref.fa -o csi_run

```

### Mounting data directories for Docker

You must mount the location where your input data and reference data is found, and the location where you want the output data to be stored, so that Docker can access it. This is done with the `-v` flag.

The argument to `-v` looks like so: `<LOCAL PATH>:<DOCKER PATH>`. The `<LOCAL PATH>` is the path on your system. The `<DOCKER PATH>` is the path Docker (and in turn CSI Phylogeny) can access your data. The `<DOCKER PATH>` should always start with `/app`, as this is the directory in the Docker container where CSI Phylogeny is executed from and will look for data and write data to.

It is important, that when you write paths to CSI Phylogeny, to remember that it should always be with respect to the `<DOCKER PATH>` and not the `<LOCAL PATH>`. If you need to mount several different locations to the Docker conatiner, then you add more `-v` flags. One `-v` flag cannot mount several locations.

Below is and example with several mount points.

```bash

docker run -v "$(pwd):/app/" -v "$(pwd)/data/:/app/data/" -v "$(pwd)/data2/:/app/data2/" --rm csi -i data/cluster01/mcr1_* -r data2/ref.fa -o csi_run

```

The example will mount `/current/working/directory/` to `/app/`, `/current/working/directory/data/` to `/app/data/` and `/current/working/directory/data2/` to `/app/data2/`.
CSI Phylogeny will in this example look in the two data directories as specified by the command and write the output directory to the current working directory. Below is the directory structure after execution.

```bash

ls -l
total 8
drwxr-xr-x 3 165536 165536   34 Aug 23 11:30 cromwell-executions
drwxr-xrwx 2 165536 165536   10 Aug 23 11:31 cromwell-workflow-logs
drwxr-xr-x 3 165536 165536 4096 Aug 23 11:32 csi_run
drwxr-xr-x 3 user   users  4096 Jul  1 12:00 data
drwxr-xr-x 2 user   users   159 Aug 22 11:28 data2

```

More information on mounting volumes can be found on the Docker website [here](https://docs.docker.com/engine/storage/volumes/).

### Controlling how much CPU Docker uses

Per default Docker will use all available CPUs on the system. If you wish to control this you need to set the `--cpus=<value>` option. Many more runtime options are documented in the Docker documentation [here](https://docs.docker.com/engine/containers/resource_constraints/).

```bash

docker run --cpus=4 -v "$(pwd):/app" genomicepidemiology/csiphylogeny:latest -i data/cluster01/mcr1_* -r data/cluster01/ref.fa -o csi_run

```

## Local installation

### Dependencies

* Cromwell
* Python version 3.10+
* Perl
    - Module: Statistics::Descriptive
    - Module: Bio::AlignIO 
* BWA
* genomeCoverageBed (BEDTools-Version-2.16.2)
* Samtools version 0.1.18
* VCFUtils (samtools)
* bcftools (samtools)
* [MUMMER](https://mummer4.github.io) (nucmer, show_snps, show_coords, delta_filter)

### Install CSI Phylogeny

After installing all dependencies clone the CSI Phylogeny repository.

```bash

git clone git@bitbucket.org:genomicepidemiology/csiphylogeny.git

```

Create a virtual environment for CSI Phylogeny and install Python requiremnts.

```bash

cd csiphylogeny
python -m venv env
source env/bin/activate
pip install -r requirements.txt

```

### Environment Variables

Environment variables recognised by CSIPhylogeny, the flag they replace and the default value for the flag. Provided command line flags will always take precedence. Set environment variables takes precedence over default flag values.

#### Environment Variables Table

| Environment Variabel       | Flag                   | Default Value     |
|----------------------------|------------------------|-------------------|
| CGE_CSI_ALIGNER            | aligner                | bwa               |
| CGE_CSI_ALIGNER_PATH       | aligner_path           | bwa               |
| CGE_CSI_DEPTH              | depth                  | 10                |
| CGE_CSI_REL_DEPTH          | relative_depth         | 10                |
| CGE_CSI_PRUNE              | prune                  | 10                |
| CGE_CSI_PRUNE_ASSEMBLY     | prune_assembly         | 10                |
| CGE_CSI_PRUNE_RAW          | prune_raw              | 10                |
| CGE_CSI_MAPQ               | mapping_qual           | 25                |
| CGE_CSI_SNPQ               | snp_qual               | 30                |
| CGE_CSI_ZSCORE             | z_score                | 1.96              |
| CGE_CSI_IGNORE_HETERO      | ignore_heterozygous    | False             |
| CGE_CSI_END_DIST           | end_distance           | 0                 |
| CGE_CSI_MIN_LENGTH         | min_length             | 0                 |
| CGE_CSI_SAMTOOLS           | samtools_path          | samtools          |
| CGE_CSI_VCFUTILS           | vcfutils_path          | vcfutils.pl       |
| CGE_CSI_BCFTOOLS           | bcftools_path          | bcftools          |
| CGE_GENOMECOVERAGEBED      | genomeCoverageBed_path | genomeCoverageBed |
| CGE_PERL                   | perl_path              | perl              |
| CGE_FASTTREE               | fasttree_path          | FastTree          |
| CGE_NUCMER                 | nucmer_path            | nucmer            |
| CGE_SHOW_SNPS              | show_snps_path         | show-snps         |
| CGE_SHOW_COORDS            | show_coords_path       | show-coords       |
| CGE_DELTA_FILTER           | delta_filter_path      | delta-filter      |
| CGE_CROMWELL               | cromwell_path          | cromwell          |
| CGE_JAVA                   | java_path              | java              |
| CGE_FORCE_NEIGHBORS        | force_neighbors        | False             |

### Custom script example for running CSI Phylogeny

This script is an example on how CSI Phylogeny can be executed on our HPC.

**run_csi_phylogeny.sh**

```bash

#!/bin/bash

set +u

module purge
source /home/apps/csiphylogeny/env/bin/activate

export CGE_CSI_ALIGNER_PATH="/home/apps/bwa/0.7.10/bwa"
export CGE_CSI_SAMTOOLS="/services/tools/samtools/0.1.18/bin/samtools"
export CGE_CSI_VCFUTILS="/services/tools/samtools/0.1.18/bin/vcfutils.pl"
export CGE_CSI_BCFTOOLS="/services/tools/samtools/0.1.18/bin/bcftools"
export CGE_GENOMECOVERAGEBED="/services/tools/bedtools/2.26.0/bin/genomeCoverageBed"
export CGE_PERL="/services/tools/perl/5.20.1/bin/perl"
export CGE_FASTTREE="/services/tools/fasttree/2.1.11/FastTree"
export CGE_NUCMER="/services/tools/mummer/3.23/nucmer"
export CGE_SHOW_SNPS="/services/tools/mummer/3.23/show-snps"
export CGE_SHOW_COORDS="/services/tools/mummer/3.23/show-coords"
export CGE_DELTA_FILTER="/services/tools/mummer/3.23/delta-filter"
export CGE_CROMWELL="/services/tools/cromwell/72/cromwell-72.jar"
export CGE_JAVA="/services/tools/jdk/18.0.1/bin/java"

python3 /home/apps/csiphylogeny/csi_phylogeny.py "$@"

```

### Note for HPCs

If your cromwell run fails on an HPC it can be a little difficult to figure out exactely what the problem is. Sometimes you just get the message that some intermediate file could not be found.

The most common issue we've encountered is with paths to the different tools. Depending on the HPC setup, the environment in which you run Cromwell might be different from the environment in which your child jobs are executed. This can often be solved by providing absolute paths to the different tools.

## Known issues

* Resource allocations (walltime, memory etc) is hard coded, should be input parameters.