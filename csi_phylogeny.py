#!/usr/bin/env python3

import logging
from argparse import ArgumentParser
import subprocess
import json
import os.path
import sys


from lib.config import Config
from lib.input_csv import InputCSV
from lib.plots import get_data_files, IgnoredSNPsPlots, CoveragePlots
from lib.output import Output


WORKFLOW = 'CSIPhylogeny'
WORKFLOW_KEYS = [
    'inputSamplesFile',         # 0
    'aligner',
    'alignerPath',
    'tmpDir',
    'outDir',
    'clean',
    'reference',                # 6
    'depth',
    'relativeDepth',
    'rawPrune',
    'mapQuality',               # 10
    'snpQuality',
    'zThresshold',
    'heterozygous',
    'assemblyPrune',
    'assemblyEndDistance',      # 15
    'assemblyLength',
    'samtoolsPath',
    'snpCallerPath',
    'vcfutilsPath',
    'fastatoolPath',            # 20
    'genomeCoverageBedPath',
    'bcftoolsPath',
    'perlPath',
    'snpFilterPath',
    'snp_vcf2fastaPath',        # 25
    'fastTreePath',
    'snpMatrixPath',
    'snpCallerAssemblyPath',
    'nucmerPath',               # 30
    'show_snpsPath',
    'show_coordsPath',
    'delta_filterPath',
    'intersectionPath',         # 34
]


def get_workflow_complete_keys():
    wf_keys = []
    for key in WORKFLOW_KEYS:
        wf_key = "{}.{}".format(WORKFLOW, key)
        wf_keys.append(wf_key)
    return wf_keys


def create_input_dict(args, input_csv_path):
    wf_keys = get_workflow_complete_keys()
    local_script_paths = load_local_script_paths()
    input_dict = {}

    input_dict[wf_keys[0]] = Config.get_abs_path_and_check(input_csv_path)
    input_dict[wf_keys[1]] = args.aligner
    input_dict[wf_keys[2]] = args.aligner_path
    input_dict[wf_keys[3]] = Config.get_abs_path_and_check(args.temp_dir)
    input_dict[wf_keys[4]] = Config.get_abs_path_and_check(args.output_dir)
    input_dict[wf_keys[5]] = str(args.clean)
    input_dict[wf_keys[6]] = Config.get_abs_path_and_check(args.reference)
    input_dict[wf_keys[7]] = str(args.depth)
    input_dict[wf_keys[8]] = str(args.relative_depth)
    input_dict[wf_keys[9]] = str(args.prune_raw)
    input_dict[wf_keys[10]] = str(args.mapping_qual)
    input_dict[wf_keys[11]] = str(args.snp_qual)
    input_dict[wf_keys[12]] = str(args.z_score)
    input_dict[wf_keys[13]] = str(args.ignore_heterozygous)
    input_dict[wf_keys[14]] = str(args.prune_assembly)
    input_dict[wf_keys[15]] = str(args.end_distance)
    input_dict[wf_keys[16]] = str(args.min_length)
    input_dict[wf_keys[17]] = args.samtools_path
    input_dict[wf_keys[18]] = local_script_paths["snpCallerPath"]
    input_dict[wf_keys[19]] = args.vcfutils_path
    input_dict[wf_keys[20]] = local_script_paths["fastatoolPath"]
    input_dict[wf_keys[21]] = args.genomeCoverageBed_path
    input_dict[wf_keys[22]] = args.bcftools_path
    input_dict[wf_keys[23]] = args.perl_path
    input_dict[wf_keys[24]] = local_script_paths["snpFilterPath"]
    input_dict[wf_keys[25]] = local_script_paths["snp_vcf2fastaPath"]
    input_dict[wf_keys[26]] = args.fasttree_path
    input_dict[wf_keys[27]] = local_script_paths["snpMatrixPath"]
    input_dict[wf_keys[28]] = local_script_paths["snpCallerAssemblyPath"]
    input_dict[wf_keys[29]] = args.nucmer_path
    input_dict[wf_keys[30]] = args.show_snps_path
    input_dict[wf_keys[31]] = args.show_coords_path
    input_dict[wf_keys[32]] = args.delta_filter_path
    input_dict[wf_keys[33]] = local_script_paths["intersectionPath"]

    return input_dict


def load_local_script_paths():
    local_script_paths = {}
    app_root = os.path.dirname(os.path.realpath(__file__))
    local_script_paths["snpCallerPath"] = ("{}/lib/scripts/snpcaller_bwa.pl"
                                           .format(app_root))
    local_script_paths["fastatoolPath"] = ("{}/lib/scripts/Ks_fasta_tool.pl"
                                           .format(app_root))
    local_script_paths["snpFilterPath"] = ("{}/lib/scripts/snpfilter.pl"
                                           .format(app_root))
    local_script_paths["intersectionPath"] = ("{}/lib/scripts/intersection.pl"
                                              .format(app_root))
    local_script_paths["snp_vcf2fastaPath"] = (
        "{}/lib/scripts/snp_vcf2fasta.pl".format(app_root))
    local_script_paths["snpMatrixPath"] = ("{}/lib/scripts/snpmatrix.pl"
                                           .format(app_root))
    local_script_paths["snpCallerAssemblyPath"] = (
        "{}/lib/scripts/snpcaller_nucmer.pl".format(app_root))
    return local_script_paths


def set_cromwell_cmd(conf):
    cromwell_cmd = [conf.java_path]

    if(conf.cromwell_cfg):
        cromwell_cmd.append(f"-Dconfig.file={conf.cromwell_cfg}")

    cromwell_cmd.extend(["-jar", conf.cromwell_path, "run", conf.csi_wdl,
                         "--inputs", conf.in_json_path])

    return cromwell_cmd


if __name__ == '__main__':

    parser = ArgumentParser(allow_abbrev=False)
    parser.add_argument('-i', '--input_files',
                        help=('Assemblies in FASTA format or paired/single-end '
                              'raw data.'),
                        required=True,
                        nargs='+',
                        metavar='FAST(Q|A)')
    parser.add_argument('-o', '--output_dir',
                        help=('The results of the workflow will be written to '
                              'this directory.'),
                        required=True,
                        metavar='PATH')
    parser.add_argument('--temp_dir',
                        help=('Files needed while running the application is '
                              'stored in this directory. It can be deleted '
                              'after execution. Default "tmp" directory within'
                              ' output directory.'),
                        default=None,
                        metavar='PATH')

    parser.add_argument('--clean',
                        help=('If set, will not use previously calculated '
                              'results.'),
                        action='store_true',
                        default=False)

    parser.add_argument('-r', '--reference',
                        help=('Path to fasta file to be used as reference for '
                              'alignments.'),
                        required=True,
                        default=None,
                        metavar='PATH')

    parser.add_argument('-d', '--depth',
                        help=('Required depth for SNP calling. Default: 10.'),
                        default=None,
                        type=int,
                        metavar='INT')

    parser.add_argument('-e', '--relative_depth',
                        help=('Required depth relative to average depth for SNP'
                              ' calling (in pct). If for some sample the '
                              'relative depth results in a threshold lower than'
                              ' the thresshold specified for depth, then the '
                              'higher threshold for depth will be used instead.'
                              ' Default: 10.'),
                        default=None,
                        type=int,
                        metavar='INT')

    parser.add_argument('-p', '--prune',
                        help=('Minimum required distance between SNPs. '
                              'This option also sets --prune_assembly and '
                              '--prune_raw.'
                              'Default: 10.'),
                        default=10,
                        type=int,
                        metavar='INT')

    parser.add_argument('--prune_raw',
                        help=('Minimum required distance between SNPs found '
                              'using alignment of reads. Overwrites --prune '
                              'option if set.'
                              'Default: Same as --prune.'),
                        default=None,
                        type=int,
                        metavar='INT')

    parser.add_argument('--prune_assembly',
                        help=('Minimum required distance between SNPs. '
                              'using alignment of contigs. Overwrites --prune '
                              'option if set.'
                              'Default: Same as --prune.'),
                        default=None,
                        type=int,
                        metavar='INT')

    parser.add_argument('-x', '--mapping_qual',
                        help=('Minimum required mapping quality for reads.'
                              'Default: 25.'),
                        default=None,
                        type=int,
                        metavar='INT')

    parser.add_argument('-y', '--snp_qual',
                        help=('Minimum required SNP quality. Default: 30.'),
                        default=None,
                        type=int,
                        metavar='INT')

    parser.add_argument('-z', '--z_score',
                        help=('Required Z score for SNPs. Default: 1.96.'),
                        default=None,
                        type=float,
                        metavar='FLOAT')

    parser.add_argument('-s', '--end_distance',
                        help=('SNP minimum distance from contig end. '
                              'Default: 0.'),
                        default=None,
                        type=int,
                        metavar='INT')

    parser.add_argument('-t', '--ignore_heterozygous',
                        help=('If set, will ignore heterozygous SNPs.'),
                        action='store_true',
                        default=False)

    parser.add_argument('-l', '--min_length',
                        help=('Only call SNPs in contigs of at least this '
                              'length. Default: 0.'),
                        default=None,
                        type=int,
                        metavar='INT')

    parser.add_argument('--timeout',
                        help=('Maximum time (sec) the job should run before it '
                              'is killed. Default: 86400 (24 hours).'),
                        default=None,
                        type=int,
                        metavar='INT')

    parser.add_argument('--cromwell_cfg',
                        help=('Path to cromwell configuration file. If not set'
                              ' Cromwell will run directly on the current '
                              'system.'),
                        default=None,
                        metavar='PATH')

    parser.add_argument('--aligner_path',
                        help=('Path of alignment application. Not needed if '
                              'application is in your PATH and has the aligner '
                              'name.'),
                        default=None,
                        metavar='PATH')
    parser.add_argument('--samtools_path',
                        help=('Path of samtools application. Not needed if '
                              'application is in your PATH'),
                        default=None,
                        metavar='PATH')
    parser.add_argument('--vcfutils_path',
                        help=('Path of vcfutils.pl application. Not needed if '
                              'application is in your PATH'),
                        default=None,
                        metavar='PATH')
    parser.add_argument('--bcftools_path',
                        help=('Path of bcftools application. Not '
                              'needed if application is in your PATH'),
                        default=None,
                        metavar='PATH')
    parser.add_argument('--genomeCoverageBed_path',
                        help=('Path of genomeCoverageBed application. Not '
                              'needed if application is in your PATH'),
                        default=None,
                        metavar='PATH')
    parser.add_argument('--perl_path',
                        help=('Path of perl interpreter. Not needed if '
                              'interpreter is in your PATH and named "perl"'),
                        default=None,
                        metavar='PATH')
    parser.add_argument('--fasttree_path',
                        help=('Path of FastTree application. Not '
                              'needed if application is in your PATH'),
                        default=None,
                        metavar='PATH')
    parser.add_argument('--nucmer_path',
                        help=('Path of nucmer application. Not '
                              'needed if application is in your PATH'),
                        default=None,
                        metavar='PATH')
    parser.add_argument('--show_snps_path',
                        help=('Path of show_snps application. Not '
                              'needed if application is in your PATH'),
                        default=None,
                        metavar='PATH')
    parser.add_argument('--show_coords_path',
                        help=('Path of show_coords application. Not '
                              'needed if application is in your PATH'),
                        default=None,
                        metavar='PATH')
    parser.add_argument('--delta_filter_path',
                        help=('Path of delta_filter application. Not '
                              'needed if application is in your PATH'),
                        default=None,
                        metavar='PATH')

    parser.add_argument('--cromwell_path',
                        help=('Path of cromwell jar file. Not '
                              'needed if "cromwell" is in your PATH'),
                        default=None,
                        metavar='PATH')
    parser.add_argument('--java_path',
                        help=('Path of java. Not needed if java is in your '
                              'PATH'),
                        default=None,
                        metavar='PATH')

    parser.add_argument(
        '--force_neighbors',
        help=('If issues are encountered due to read headers being too '
              'similar, then enabling forced neighbors may solve the issue as '
              'it requires alle pairs to be "alphabetical" neighbors.'),
        action='store_true',
        default=False)

    args = parser.parse_args()
    args.aligner = "bwa"
    conf = Config(args)

    input_tsv_file = '{}/input.tsv'.format(conf.temp_dir)

    inputtsv = InputCSV(conf.input_files, seperator="\t",
                        force_neighbors=conf.force_neighbors)
    inputtsv.write_csv(input_tsv_file)

    input_dict = create_input_dict(conf, input_tsv_file)

    with open(conf.in_json_path, "w") as fh:
        json.dump(input_dict, fh, indent=2)

    cromwell_cmd = set_cromwell_cmd(conf)

    print(f"Running Cromwell: {cromwell_cmd}")
    proc = subprocess.run(cromwell_cmd, check=False, capture_output=True,
                          timeout=conf.timeout)
    print(f"Cromwell output: {proc.stdout.decode('utf-8')}")

    if(proc.returncode != 0):
        print("########### Error output #############")
        print(proc.stderr.decode('utf-8'))
        print("############ Std output ##############")
        print(proc.stdout.decode('utf-8'))
        print("! Return code: {}".format(proc.returncode))
        print("! Cromwell command that failed:\n{}"
              .format(" ".join(cromwell_cmd)))
        print("! Cromwell run failed.")
        sys.exit(1)
    else:
        print("Cromwell run succeded.")
        output = Output(conf)
        output.write_output()
        print("Creating plots.")
        sample_names, ignored_snps_files, coverage_files = get_data_files(conf.temp_dir)
        CoveragePlots(sample_names, coverage_files, conf.output_dir)
        IgnoredSNPsPlots(sample_names, ignored_snps_files, conf.output_dir)
        sys.exit(0)
