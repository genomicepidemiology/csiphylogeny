## Setup ##

### Get all options ###

#### General ####

* mapper
* queue_dir
* out dir
* temp_dir
* clean
* quiet

* tree_accu (necessary?)
* dry run (?)

* reference

#### Raw data ####

* depth
* relative_depth
* min_depth
* prune
* filter_map_quality
* filter_snp_quality
* filter_z
* filter_heterozygous

#### Assemblies ####

* prune
* as_end_distance
* as_length

#### Others ####

option to exclude list of files.
exclude_seq_str


#### Queue specific ####

* queue
* queue_mem

#### Web specific ####

* web (?)
* comment


## Input files ##

* Assume encoding 33 for now

1. Check existence of files
2. Register zipped files
3. determine fastq or fasta
    - If fasta clean it from windows and mac newlines (s/\r\n/\n/g and s/\r/\n/g).
    - If fasta ckean from all sequence that are not ATCGN (s/[^ATCGN]/n/gi)
