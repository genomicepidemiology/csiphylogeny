# Test Config class

## Setup

```python

>>> from lib.config import Config

>>> class DummyArgs():
...     def __init__(self):
...         self.input_files = None
...         self.output = None
...         self.output_dir = "test/rundir/config_test/"    ##TODO##
...         self.temp_dir = "test/rundir/config_test/tmp/"  ##TODO##
...         self.aligner = "bwa"
...         self.clean = False
...         self.reference = "test/data/cluster01/ref.fa"
...         self.depth = None # should default to 10
...         self.relative_depth = None # should default to 10
...         self.prune_assembly = None # should default to 10
...         self.prune_raw = None # should default to 10
...         self.mapping_qual = 25
...         self.snp_qual = 30
...         self.z_score = None # should default to 1.96
...         self.ignore_heterozygous = False
...         self.end_distance = 0
...         self.min_length = 0
...         self.aligner_path = None # should default to 'bwa'
...         self.samtools_path = "samtools"
...         self.vcfutils_path = "vcfutils.pl"
...         self.genomeCoverageBed_path = "genomeCoverageBed"
...         self.bcftools_path = "bcftools"
...         self.perl_path = "perl"
...         self.fasttree_path = "FastTree"
...         self.nucmer_path = "nucmer"
...         self.show_snps_path = "show-snps"
...         self.show_coords_path = "show_coords_path"
...         self.delta_filter_path = "delta-filter"
...         self.cromwell_path = "cromwell"
...         self.java_path = "java"
...         self.force_neighbors = False

>>> args = DummyArgs()
>>> args2 = DummyArgs()
>>> args3 = DummyArgs()

```

## Config(args)

### No Input

```python

>>> conf = Config(args)

```

### FASTQ

```python

>>> test_file_A1 = "{}/{}".format(conf.root_dir,
...     "test/data/ERR1512990_1.fastq.gz")
>>> test_file_A2 = "{}/{}".format(conf.root_dir,
...     "test/data/ERR1512990_2.fastq.gz")
>>> test_in_files = [test_file_A1, test_file_A2]
>>> args3.input_files = test_in_files
>>> conf3 = Config(args3)
>>> conf3.input_files[0]
... #doctest: +ELLIPSIS
'/.../test/data/ERR1512990_1.fastq.gz'

```

## get_abs_path_and_check(path)

```python

>>> Config.get_abs_path_and_check(conf.reference)
... #doctest: +ELLIPSIS
'/...test/data/cluster01/ref.fa'

>>> Config.get_abs_path_and_check("/file/not/found")
Traceback (most recent call last):
SystemExit: ERROR: Path not found: /file/not/found

```

## _set_default_values(args)

```python

>>> Config._set_default_values(args)
>>> args.relative_depth
10
>>> args.z_score
1.96
>>> args.aligner_path
'bwa'

>>> args.relative_depth = 50
>>> Config._set_default_values(args)
>>> args.relative_depth
50

```

## set_default_and_env_vals(args, env_def_filepath)

```python

>>> env_def_filepath = "{}/{}".format(conf.root_dir, "README.md")

>>> print(args2.depth)
None
>>> import os
>>> os.environ["CGE_CSI_PRUNE_RAW"] = "70"
>>> args2.prune_assembly = 90

>>> Config.set_default_and_env_vals(args2, env_def_filepath)
>>> args2.depth
10
>>> args2.prune_raw
'70'
>>> args2.prune_assembly
90

>>> env_def_fail_filepath = "{}/{}".format(conf.root_dir,
...     "test/data/env_var_test_fail.md")
>>> Config.set_default_and_env_vals(args2, env_def_fail_filepath)
... #doctest: +ELLIPSIS
Traceback (most recent call last):
SystemExit: ERROR: A flag set...alignerWrong.

```
