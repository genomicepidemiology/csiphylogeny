#!/usr/bin/env python3

import os.path
import os
import sys
import subprocess

from cgecore.utils.loaders_mixin import LoadersMixin


class Config():

    README = "README.md"
    CSI_WDL = "lib/csiphylogeny.wdl"
    INPUT_JSON = "input.json"

    DEFAULT_VALS = {
        "input_files": None,
        "output_dir": None,
        "temp_dir": None,  # Default value set by _set_default_values()
        "aligner": "bwa",
        "aligner_path": "bwa",
        "clean": False,
        "reference": None,
        "depth": 10,
        "relative_depth": 10,
        "prune_assembly": 10,
        "prune_raw": 10,
        "mapping_qual": 25,
        "snp_qual": 30,
        "z_score": 1.96,
        "ignore_heterozygous": False,
        "end_distance": 0,
        "min_length": 0,
        "timeout": 86400,
        "cromwell_cfg": None,
        "samtools_path": "samtools",
        "vcfutils_path": "vcfutils.pl",
        "genomeCoverageBed_path": "genomeCoverageBed",
        "bcftools_path": "bcftools",
        "perl_path": "perl",
        "fasttree_path": "FastTree",
        "nucmer_path": "nucmer",
        "show_snps_path": "show-snps",
        "show_coords_path": "show-coords",
        "delta_filter_path": "delta-filter",
        "cromwell_path": "cromwell",
        "java_path": "java",
        "force_neighbors": False
    }

    def __init__(self, args):

        # Directoy of config.py substracted the last dir 'lib'
        self.root_dir = os.path.dirname(os.path.realpath(__file__))[:-3]
        self.readme_file = "{}{}".format(self.root_dir, Config.README)
        self.csi_wdl = "{}{}".format(self.root_dir, Config.CSI_WDL)

        if args.prune_assembly is None:
            args.prune_assembly = args.prune
        if args.prune_raw is None:
            args.prune_raw = args.prune

        Config.set_default_and_env_vals(args, self.readme_file)

        self.set_general_opts(args)
        self.set_app_paths(args)
        if(args.input_files):
            self.set_input_files(args)

    def set_general_opts(self, args):
        self.output_dir = os.path.abspath(args.output_dir)
        os.makedirs(self.output_dir, exist_ok=True)

        self.temp_dir = os.path.abspath(args.temp_dir)
        os.makedirs(self.temp_dir, exist_ok=True)
        os.environ["CGE_CSI_CROM_EXE_DIR"] = f"{self.temp_dir}/cromwell-executions"
        os.environ["CGE_CSI_CROM_LOG_DIR"] = f"{self.temp_dir}/cromwell-workflow-logs"

        if(args.cromwell_cfg):
            self.cromwell_cfg = Config.get_abs_path_and_check(args.cromwell_cfg)
        else:
            self.cromwell_cfg = None

        self.in_json_path = "{}/{}".format(self.temp_dir, Config.INPUT_JSON)

        if(args.aligner != "bwa"):
            sys.exit("ERROR: This aligner is currently only allowed to be 'bwa'"
                     " value found was: {}".format(args.aligner))
        self.aligner = args.aligner

        self.clean = bool(args.clean)

        self.reference = Config.get_abs_path_and_check(args.reference)

        if(args.relative_depth < 0 or args.relative_depth > 100):
            sys.exit("ERROR: relative depth is a percentage and must be "
                     "betwween 0 and 100. Value given was: {}"
                     .format(args.relative_depth))
        else:
            self.relative_depth = int(args.relative_depth)

        self.depth = int(args.depth)
        self.prune_assembly = int(args.prune_assembly)
        self.prune_raw = int(args.prune_raw)
        self.mapping_qual = int(args.mapping_qual)
        self.snp_qual = int(args.snp_qual)
        self.z_score = float(args.z_score)
        self.ignore_heterozygous = bool(args.ignore_heterozygous)
        self.end_distance = int(args.end_distance)
        self.min_length = int(args.min_length)
        self.timeout = int(args.timeout)

    def set_app_paths(self, args):
        # TODO: Test if you can find and execute apps.

        self.samtools_path = args.samtools_path
        self.aligner_path = args.aligner_path
        self.samtools_path = args.samtools_path
        self.vcfutils_path = args.vcfutils_path
        self.genomeCoverageBed_path = args.genomeCoverageBed_path
        self.bcftools_path = args.bcftools_path
        self.perl_path = args.perl_path
        self.fasttree_path = args.fasttree_path
        self.nucmer_path = args.nucmer_path
        self.show_snps_path = args.show_snps_path
        self.show_coords_path = args.show_coords_path
        self.delta_filter_path = args.delta_filter_path
        self.cromwell_path = args.cromwell_path
        self.java_path = args.java_path
        self.force_neighbors = args.force_neighbors

    def set_input_files(self, args):
        self.input_files = []
        for seq_file in args.input_files:
            seq_file_path = Config.get_abs_path_and_check(seq_file)
            self.input_files.append(seq_file_path)

    @staticmethod
    def get_abs_path_and_check(path, allow_exit=True):
        abs_path = os.path.abspath(path)
        if(not os.path.isfile(abs_path) and not os.path.isdir(abs_path)):
            if(allow_exit):
                sys.exit("ERROR: Path not found: {}".format(path))
            else:
                raise FileNotFoundError
        return abs_path

    @staticmethod
    def set_default_and_env_vals(args, env_def_filepath):

        known_envs = LoadersMixin.load_md_table_after_keyword(
            env_def_filepath, "#### Environment Variables Table")

        # Set flag values defined in environment variables
        for var, entries in known_envs.items():

            try:
                cli_val = getattr(args, entries[0])
                # Flags set by user will not be None, default vals will be None.
                if(cli_val is not None):
                    continue

                var_val = os.environ.get(var, None)
                if(var_val is not None):
                    setattr(args, entries[0], var_val)

            except AttributeError:
                sys.exit("ERROR: A flag set in the Environment Variables Table "
                         "in the README file did not match any valid flags in "
                         "CSI Phylogeny. Flag not recognized: {}."
                         .format(entries[0]))

        Config._set_default_values(args)

    @staticmethod
    def _set_default_values(args):
        for flag, def_val in Config.DEFAULT_VALS.items():
            val = getattr(args, flag)
            if(val is None):
                setattr(args, flag, def_val)
        if(args.temp_dir is None):
            args.temp_dir = "{}/tmp/".format(args.output_dir)
