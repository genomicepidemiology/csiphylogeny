import 'wdl/index.wdl'
import 'wdl/call.wdl'
import 'wdl/filter.wdl'
import 'wdl/intersect.wdl'
import 'wdl/tree.wdl'
import 'wdl/matrix.wdl'

workflow CSIPhylogeny {
    String inputSamplesFile
    Array[Array[File]] inputSamples = read_tsv(inputSamplesFile)

    # User options
    String aligner
    String tmpDir
    String outDir
    Boolean clean
    String reference
    Int depth
    Int relativeDepth
    Int rawPrune
    Int mapQuality
    Int snpQuality
    Float zThresshold
    Boolean heterozygous
    Int assemblyEndDistance
    Int assemblyLength

    # Set by app
    String snpCallerPath
    String alignerPath
    String vcfutilsPath
    String fastatoolPath
    String genomeCoverageBedPath
    String samtoolsPath
    String bcftoolsPath
    String perlPath
    String snpFilterPath
    String intersectionPath
    String snp_vcf2fastaPath
    String fastTreePath
    String snpMatrixPath
    String snpCallerAssemblyPath
    String nucmerPath
    String show_snpsPath
    String show_coordsPath
    String delta_filterPath


    call index.IndexReference {
        input: reference=reference,
            aligner=aligner,
            alignerPath=alignerPath,
            samtoolsPath=samtoolsPath
    }

    scatter (sample in inputSamples) {
        String sampleName = sample[0]
        String samplePath1 = sample[1]
        String sampleType = sample[2]

        if (sampleType == 'single' || sampleType == 'paired') {
            String sampleEncoding = sample[3]
        }

        if (sampleType == 'paired') {
            String samplePath2 = sample[4]
        }

        call call.SNPCall {
            input: sampleName=sampleName,
                inputPath1=samplePath1,
                seqType=sampleType,
                inputPath2=samplePath2,
                outDir=tmpDir,
                filterPrune=rawPrune,
                filterSNPQuality=snpQuality,
                filterMapQuality=mapQuality,
                filterZ=zThresshold,
                filterRelativeDepth=relativeDepth,
                filterHeterozygous=heterozygous,
                minDepth=depth,
                minAssemblyLength=assemblyLength,
                minAssemblyEndDistance=assemblyEndDistance,
                reference=reference,
                snpCaller=snpCallerPath,
                vcfutils=vcfutilsPath,
                bwa=alignerPath,
                fastatool=fastatoolPath,
                genomeCoverageBed=genomeCoverageBedPath,
                samtools=samtoolsPath,
                bcftools=bcftoolsPath,
                perl=perlPath,
                snpCallerAssembly=snpCallerAssemblyPath,
                nucmer=nucmerPath,
                show_snps=show_snpsPath,
                show_coords=show_coordsPath,
                delta_filter=delta_filterPath,
                samtoolsIndexFile=IndexReference.samtoolsIndexFile
        }
    }

    call filter.FilterSNPs {
        input: ignoredSnpsFiles=SNPCall.ignored_snps_file,
            snpFilter=snpFilterPath,
            filterSNPQuality=snpQuality,
            filterMapQuality=mapQuality,
            filterRelativeDepth=relativeDepth,
            minDepth=depth,
            outDir=outDir,
            perl=perlPath,
    }

    call intersect.GenomeIntersection {
        input: ignoredSnpsFiles=SNPCall.ignored_snps_file,
        intersection=intersectionPath,
        outDir=outDir,
        perl=perlPath,
    }

    call tree.InferTree {
        input: vcfs=FilterSNPs.filteredVCFs,
            tempDir=tmpDir,
            outDir=outDir,
            perl=perlPath,
            snp_vcf2fasta=snp_vcf2fastaPath,
            fastTree=fastTreePath
    }

    call matrix.SNPMatrix {
        input: fastaAln=InferTree.snpAln,
            perl=perlPath,
            outDir=outDir,
            snpMatrix=snpMatrixPath
    }

}
