# InputCSV class tests

## Setup

```python

>>> import os.path
>>> import inspect

>>> from lib.input_csv import InputCSV

>>> lib_file = inspect.getfile(InputCSV)
>>> # Directoy of config.py substracted the last dir 'lib'
>>> root_dir = os.path.dirname(os.path.realpath(lib_file))[:-3]

>>> test_data_dir = "{}/{}".format(root_dir, "test/data/")
>>> test_filenames = ["ERR1512990_1.fastq.gz", "ERR1512990_2.fastq.gz",
...                   "SRR5063020.fastq.gz",
...                   "SRR5633389_1.fastq.gz",
...                   "SRR5633389_2.fastq.gz",
...                   "DTU2017-1115-PRJ1066-CPH-Sewage-141_R1_001.fq.gz",
...                   "DTU2017-1115-PRJ1066-CPH-Sewage-141_R2_001.fq.gz"]
>>> test_input_data = []
>>> for filename in test_filenames:
...     test_input_data.append(test_data_dir + filename)

```

## __init__(input_files, seperator=";")

```python

>>> inputcsv = InputCSV(test_input_data, seperator=";")
>>> init_test_input_correct = [False] * 4
>>> for line in inputcsv.csv_string.split("\n"):
...     if(line.endswith("ERR1512990_2.fastq.gz;unknown")):
...         init_test_input_correct[0] = True
...     elif(line.endswith("SRR5063020.fastq.gz;single;;unknown")):
...         init_test_input_correct[1] = True
...     elif(line.endswith("SRR5633389_2.fastq.gz;unknown")):
...         init_test_input_correct[2] = True
...     elif(line.endswith("DTU2017-1115-PRJ1066-CPH-Sewage-141_R2_001.fq.gz;unknown")):
...         init_test_input_correct[3] = True
>>> assert(all(init_test_input_correct))

```
