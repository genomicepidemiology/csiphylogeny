#!/usr/bin/env python3

import logging
from argparse import ArgumentParser


from lib.seqfilehandler import SeqFile


class InputCSV():
    def __init__(self, input_files, seperator=";", force_neighbors=False):
        # TODO: Do not assume phred encoding.
        seqfiles = SeqFile.parse_files(input_files, phred="unknown",
                                       force_neighbour=force_neighbors)

        self.csv_string = InputCSV.seqfiles2string(seqfiles, sep=seperator)

    def write_csv(self, output):
        with open(output, "w") as fh:
            fh.write(self.csv_string)

    @staticmethod
    def seqfiles2string(seqfiles, sep=";"):
        csv_list = []

        for seqfile in seqfiles:

            if(seqfile.seq_format == "assembly"):
                file2 = ""
                encoding = ""
            else:
                encoding = str(seqfile.phred)

                if(seqfile.seq_format == "single"):
                    file2 = ""
                elif(seqfile.seq_format == "paired"):
                    file2 = seqfile.pe_file_reverse
                else:
                    quit("Sequence format not recognised for file: {}"
                         .format(seqfile.path))

            row_list = [seqfile.filename, seqfile.path, seqfile.seq_format,
                        encoding, file2]
            row = sep.join(row_list)
            csv_list.append(row)

        return "\n".join(csv_list)


if __name__ == '__main__':

    parser = ArgumentParser(allow_abbrev=False)
    parser.add_argument('input_files',
                        help=('Assemblies in FASTA format or paired/single-end '
                              'raw data.'),
                        nargs='+',
                        metavar='FAST(Q|A)')
    parser.add_argument('-o', '--output',
                        help=('Output file is written to this path.'),
                        required=True,
                        metavar='PATH')
    parser.add_argument('-s', '--seperator',
                        help=('Seperator used in csv file output.'),
                        default=";",
                        metavar='CHAR')
    parser.add_argument('--tsv',
                        help=('Create a tab seperated file (tsv) instead of a '
                              'csv file.'),
                        action='store_true',
                        default=False)

    args = parser.parse_args()

    if(args.tsv):
        args.seperator = "\t"

    inputcsv = InputCSV(args.input_files, args.seperator)
    inputcsv.write_csv(args.output)
