#!/usr/bin/env python3
import os.path

from lib.config import Config
from cgecore.output.result import Result

SNP_TREE_FILENAME = "snp_tree.newick"
SNP_MATRIX_FILENAME = "snp_matrix.txt"

class Output:
    def __init__(self, conf: Config):
        self.result = Result.init_software_result("CSI Phylogeny", conf.root_dir)
        self.config = conf
        result_data = {
            "snp_tree": self.load_snp_tree(),
            "snp_matrix": self.load_snp_matrix()
        }
        self.result.add(**result_data)

    def write_output(self):
        output_file = os.path.join(self.config.output_dir, "output.json")
        with open(output_file, "w") as f:
            f.write(self.result.json_dumps())

    def load_snp_tree(self) -> str:
        snp_tree_file = os.path.join(self.config.output_dir, SNP_TREE_FILENAME)
        with open(snp_tree_file, "r") as f:
            snp_tree_list = f.readlines()
        return "".join(snp_tree_list)

    def load_snp_matrix(self) -> dict:
        matrix_info = {}
        snp_matrix_file = os.path.join(self.config.output_dir, SNP_MATRIX_FILENAME)
        with open(snp_matrix_file, "r") as f:
            snp_matrix_list = f.readlines()

        headers = snp_matrix_list[0].strip().split("\t")
        matrix_info["seq_ids"] = headers
        matrix_info["matrix"] = []
        for line in snp_matrix_list[1:-1]:
            matrix_info["matrix"].append(line.strip().split("\t")[1:])
        return matrix_info
