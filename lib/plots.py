#!/usr/bin/env python3

import os
import sys
import gzip
from argparse import ArgumentParser
import warnings
import matplotlib.pyplot as plt
import seaborn.objects as so
import seaborn as sns
import polars as pl

sns.set_theme()

DTU_RED = [153 / 256, 0 / 256, 0 / 256]


class IgnoredSNPsPlots:
    def __init__(self, sample_names: list, ignored_snps_files: list, output_dir: str):
        self.sample_names = sample_names
        self.output_dir = output_dir
        self.ignored_snps_files = ignored_snps_files
        ignored_snps, pruned_snps, heterozygous_snps = self.load_ignored_snps()
        self.plot_ignored_snps(ignored_snps, "Ignored_snps.png")
        self.plot_pruned_snps(pruned_snps, "Pruned_snps.png")
        self.plot_heterozygous_snps(heterozygous_snps, "Heterozygous_snps.png")

    def load_ignored_snps(self):
        ignored_snps = []
        pruned_snps = []
        heterozygous_snps = []
        for i, ignored_snps_file in enumerate(self.ignored_snps_files):
            snps_pruned = 0
            ref_pos_missing = 0
            ref_pos_insuf_depth = 0
            ref_pos_insuf_Q13 = 0
            heterozygous_snps_found = False
            with open(ignored_snps_file, "r") as f:
                for line in f:
                    if line.startswith("# SNPs pruned:"):
                        snps_pruned = int(line.split(": ")[1])
                    elif line.startswith("# Reference positions missing: "):
                        ref_pos_missing = int(line.split(": ")[1])
                    elif line.startswith(
                        "# Reference positions with insufficient depth: "
                    ):
                        ref_pos_insuf_depth = int(line.split(": ")[1])
                    elif line.startswith(
                        "# Reference positions with insufficient ref/alt Q13 base depth: "
                    ):
                        ref_pos_insuf_Q13 = int(line.split(": ")[1])
                    elif line.startswith("# SNPs filtered on 0/1 flag in VCF file: "):
                        heterozygous_snps.append(int(line.split(": ")[1]))
                        heterozygous_snps_found = True
                    elif not line.startswith("#"):
                        break
            ignored_snps.append(
                snps_pruned + ref_pos_missing + ref_pos_insuf_depth + ref_pos_insuf_Q13
            )
            pruned_snps.append(snps_pruned)
            if not heterozygous_snps_found:
                heterozygous_snps.append(0)
        return ignored_snps, pruned_snps, heterozygous_snps

    def plot_ignored_snps(self, ignored_snps: list, output_filename: str):
        df = pl.DataFrame({"snps": ignored_snps, "sample": self.sample_names})
        _, ax = plt.subplots(figsize=(8, 8))
        plt.xticks(rotation=45)
        ax.set_title("Ignored SNPs")
        sns.barplot(
            data=df,
            y="snps",
            x="sample",
            ax=ax,
            color=DTU_RED,
        )
        plt.tight_layout()
        plt.savefig(
            f"{self.output_dir}/{output_filename}", bbox_inches="tight", dpi=300
        )

    def plot_pruned_snps(self, pruned_snps: list, output_filename: str):
        df = pl.DataFrame({"snps": pruned_snps, "sample": self.sample_names})
        _, ax = plt.subplots(figsize=(8, 8))
        plt.xticks(rotation=45)
        ax.set_title("Pruned SNPs")
        sns.barplot(
            data=df,
            y="snps",
            x="sample",
            ax=ax,
            color=DTU_RED,
        )
        plt.tight_layout()
        plt.savefig(
            f"{self.output_dir}/{output_filename}", bbox_inches="tight", dpi=300
        )

    def plot_heterozygous_snps(self, heterozygous_snps: list, output_filename: str):
        df = pl.DataFrame({"snps": heterozygous_snps, "sample": self.sample_names})
        _, ax = plt.subplots(figsize=(8, 8))
        plt.xticks(rotation=45)
        ax.set_title("Heterozygous SNPs")
        sns.barplot(
            data=df,
            y="snps",
            x="sample",
            ax=ax,
            color=DTU_RED,
        )
        plt.tight_layout()
        plt.savefig(
            f"{self.output_dir}/{output_filename}", bbox_inches="tight", dpi=300
        )


def get_data_files(directory):
    sample_names = []
    ignored_snps_files = []
    coverage_files = []
    for dir_entry in sorted(list(os.scandir(directory)), key=lambda x: x.name):
        filename = dir_entry.name
        if filename.endswith(".ignored_snps.txt"):
            ignored_snps_files.append(f"{directory}/{filename}")
            sample_names.append(filename[: -len(".ignored_snps.txt")])
        elif filename.endswith(".coverage.txt.gz"):
            coverage_files.append(f"{directory}/{filename}")
    return (sample_names, ignored_snps_files, coverage_files)


class CoveragePlots:
    def __init__(self, sample_names: list, coverage_files: list, output_dir: str):
        self.output_dir = output_dir
        self.coverage_files = coverage_files
        self.sample_names = sample_names

        sum_df, combined_df = self.summarize_coverage()
        self.plot_sum_coverage(sum_df, "Summarized_coverage.png")
        self.plot_combined_coverage(combined_df, "Per_sample_coverage.png")

    def summarize_coverage(self):
        sum_df = None
        combined_df = None

        for i, cov_file in enumerate(self.coverage_files):
            # Load the content into a Polars DataFrame
            with gzip.open(cov_file, "rb") as f:

                # Surpress warning from Polars:
                # Polars found a filename. Ensure you pass a path to the file
                # instead of a python file object when possible for best
                # performance.
                # As Polars does not support reading from a gzip file object
                # directly, we have to use the file object here.
                with warnings.catch_warnings():
                    warnings.simplefilter("ignore")
                    df = pl.read_csv(
                        f, has_header=False, new_columns=[self.sample_names[i]]
                    )
                    if sum_df is None:
                        sum_df = df
                        combined_df = df
                    else:
                        sum_df += df
                        combined_df = combined_df.hstack(df)

        return (sum_df, combined_df)

    def plot_sum_coverage(self, coverage_df: pl.DataFrame, output_filename: str):
        f, ax = plt.subplots(figsize=(8, 2))
        ax.set_title("Summarized coverage")
        sns.heatmap(
            coverage_df.transpose(),
            cmap="crest",
            annot=False,
            vmin=0,
            vmax=len(self.coverage_files),
            ax=ax,
            yticklabels=False,
        )
        plt.tight_layout()
        plt.savefig(
            f"{self.output_dir}/{output_filename}", bbox_inches="tight", dpi=300
        )

    def plot_combined_coverage(self, combined_df: pl.DataFrame, output_filename: str):
        f, ax = plt.subplots(figsize=(8, 6))
        ax.set_title("Per sample coverage")
        sns.heatmap(
            combined_df.transpose(),
            cmap="crest",
            annot=False,
            vmin=0,
            vmax=len(self.coverage_files),
            ax=ax,
            yticklabels=self.sample_names,
        )
        plt.tight_layout()
        plt.savefig(
            f"{self.output_dir}/{output_filename}", bbox_inches="tight", dpi=300
        )


def main():
    parser = ArgumentParser(allow_abbrev=False)
    parser.add_argument(
        "-i",
        "--input_dir",
        help=("Path to the tmp directory created by the pipeline"),
        required=True,
        metavar="PATH",
    )
    parser.add_argument(
        "-o",
        "--output_dir",
        help=("Path to the output directory"),
        required=True,
        metavar="PATH",
    )
    args = parser.parse_args()

    sample_names, ignored_snps_files, coverage_files = get_data_files(args.input_dir)
    CoveragePlots(sample_names, coverage_files, args.output_dir)
    IgnoredSNPsPlots(sample_names, ignored_snps_files, args.output_dir)


if __name__ == "__main__":
    sys.exit(main())
