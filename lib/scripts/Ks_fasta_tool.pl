#! /usr/bin/perl

# By: Rolf Sommer Kaas
# Date: 22/9 - 2010
# Version: SHOULD NOT BE USED BY ANYONE! :P
#
# -d --divide 				: Divides a multi fasta file up into single fasta files. The name of the new files will be the header of the fasta entries.
# -n --name_header <STRING> (<STRING>)	: Renames headers with the given string. Two uppercase X's ("XX") will become an increasing variable starting at XX=1. Only one variable is allowed. If a second string is given, the application will look for this string in the header and substitute it with the first.
# -i --input <FASTA FILE>		: Default is STDIN.
# -o --output <OUTPUT FILE>		: Default is STDOUT.
# -f --format <INTEGER>			: Formats the fasta file to a width of <INTEGER> bps. An integer of 0 or less will result in removal of all line endings. The fasta header will always be kept in a single line. 
# -c --change ['phylip']		: Changes the fasta format to phylip format.
# --get_base_count			: Outputs number of bases in sequence. Will not work with other options.
# --concat				: Concats sequences in a multi fasta file into one.

# MAYBE MAKE METHODS TO HANDLE VARIABLES AND NOT STREAMS... CAN A STREAM BE REDIRECTED? EG. VAR-->INSTREAM...


use strict;
use warnings;
use diagnostics;

use Getopt::Long;


# Retrieving options.
my $opt_input = "";
my $opt_output = "";
my $opt_divide = "";
my @opt_name_header = ();
my $opt_format = "";
my $opt_change = "";
my $opt_get_base_count = "";
my $opt_concat = "";
GetOptions (
	"input=s" => \$opt_input,
	"output=s" => \$opt_output,
	"divide" => \$opt_divide, 
	"name_header=s{,}" => \@opt_name_header,
	"format=i" => \$opt_format,
	"c|change=s" => \$opt_change,
	"get_base_count" => \$opt_get_base_count,
	"concat" => \$opt_concat
);

# Storing options for recursive calling and option counting.
my @options;
if( @opt_name_header == 1 ){
	push(@options, "-n $opt_name_header[0]");}
elsif( @opt_name_header == 2 ){
	push(@options, "-n $opt_name_header[0] $opt_name_header[1]");}
if($opt_divide){
	push(@options, "-d");}
if($opt_format ne ""){
	push(@options, "-f $opt_format");}
if($opt_change ne ""){
	push(@options, "-c $opt_change");}
if($opt_get_base_count){
	push(@options, "--get_base");}
if($opt_concat){
	push(@options, "--concat");}

# Handling input/output.
if($opt_input){
	$opt_input = "cat $opt_input | ";}
if($opt_output){
	$opt_output = " >$opt_output";}

# Script called with a single argument but with either --input and/or --output.
if(@options == 1 && ($opt_input || $opt_output)){
	system($opt_input."perl $0 $options[0]$opt_output");
}
# Calling functions doing the actual work.
elsif(@options == 1){
	if($opt_divide){
		&divide_file();}
	elsif(@opt_name_header == 1){
		&rename_header($opt_name_header[0]);}
	elsif(@opt_name_header == 2){
		&rename_header($opt_name_header[0], $opt_name_header[1]);}
	elsif($opt_format ne ""){
		&format_lines($opt_format);}
	elsif($opt_change ne ""){
		if($opt_change eq "phylip"){
			&change_dnadist();}
		else{
			die "Did not understand the argument \"$opt_change\" for the option -c/--change";}
	}
	elsif($opt_get_base_count){
		&count_bases();}
	elsif($opt_concat){
		&concat();}
}
# Organizing pipe for a recursive call.
elsif(@options > 1){
	my $command;

	if($opt_concat){
		$command = $command."| perl $0 --concat ";}
	if(@opt_name_header == 1){
		$command = $command."| perl $0 -n $opt_name_header[0] ";}
	elsif(@opt_name_header == 2){
		$command = $command."| perl $0 -n $opt_name_header[0] $opt_name_header[1] ";}
	if($opt_format){
		$command = $command."| perl $0 -f $opt_format ";}
	if($opt_divide){
		$command = $command."| perl $0 -d ";}

	$command = substr($command, 2);
	$command = "$opt_input$command$opt_output";
	
	system("$command");
}
else{
	die("No options given.\n");}



### FUNCTIONS DOING THE ACTUAL WORK ###

sub rename_header
# Argument: 	<STRING> (<STRING>) New name of header. Or input two string 
#		arguments. Then all apperances of the second string in a header 
#		will be replaced with the first string. 
# Input:	<STDIN> 
# Output:	<STDOUT>
#
# Description:	Reads from the standard input and finds FASTA headers. These 
#		headers are then replaced with the input string given as 
#		argument. If the string given as arguments contains an XX this
#		will be replaced by a FASTA header count. All none header data
#		will simply be printed to standard output as is.
#
# Note: 	If multiple XX's are found only one of them will be replaced by
#		a FASTA count. Which one is determined by the regular 
#		expression match algorithm. Probably the farthest to the right.
{
	my $name = $_[0];
	my ($name_counter, $name_left, $name_right);

	my $search_string;
	my $temp_left = ""; 
	my $temp_right = "";
	my $is_found = 0;
	if(@_ == 2){
		$search_string = $_[1];}

	# Finds occurences of XX's (if any).
	if($name =~ /(.*)XX(.*)/){
		$name_left = $1;
		$name_right = $2;
		$name_counter = 1;
	}

	while(<STDIN>){
		if(/^>.+/){
			if(defined($search_string)){
				if(/^>(.*)$search_string(.*)/){
					$temp_left = $1;
					$temp_right = $2;
					$is_found = 1;
				}
				elsif(/^>(.*)/){
					$temp_left = $1;
					$temp_right = "";
					$is_found = 0;
				}
			}
			if($name_counter && $search_string){
				if($is_found){
					print(">$temp_left$name_left$name_counter$name_right$temp_right\n");
					$name_counter++;
				}
				else{
					print(">$temp_left$temp_right\n");}
			}
			elsif($name_counter){
				# name_left or right is empty if no XX's are found
				print(">$name_left$name_counter$name_right\n");
				# name_counter isn't relevant if no XX's.
				$name_counter++;
			}
			elsif($search_string){
				if($is_found){
					print(">$temp_left$name$temp_right\n");}
				else{
					print(">$temp_left$temp_right\n");}
			}
			else{
				print(">$name\n");}
		}
		else{
			print($_);}
	}
}

sub divide_file
# Argument: 	None
# Input:	<STDIN>
# Output:	Multiple files.
#
# Description:	Creates a single sequence FASTA file from a multiple FASTA 
#		file. The file will be named after the header in the file.
#		Identical headers will cause the function to overwrite the 
#		previous file. The function will give a warning of this 
#		happening to STDERR.
#
# Note:		Doesn't check for illegal characters in filename.
{
	my $is_first_loop = 1;
	my $out_file_name;
	my $name_counter = 0;

	while(<STDIN>){
		if(/^>(.+?)\s+/){
			if($is_first_loop){
				$out_file_name = $1;
				open(OUT, ">$out_file_name.fasta");
				print OUT ($_);
				$is_first_loop = 0;
			}
			else{
				close(OUT);
				$out_file_name = $1;
				while(-e "$out_file_name.fasta"){
					$name_counter++;
					$out_file_name = $out_file_name.".$name_counter";
				}
				open(OUT, ">$out_file_name.fasta");
				print OUT ($_);
			}
		}
		else{
			print OUT ($_);
		}
	}
	close(OUT);
}


sub format_lines
{
	my($line_width) = @_;
	
	my $line_buffer = "";	

	while(<STDIN>){

		# Handling headers & sequence tails.
		if(/^>/){
			# Printing the last line of the previous fasta entry.
			if($line_buffer){
				print("$line_buffer\n");
				$line_buffer = "";
			}
			print($_);
		}
		# Handling sequence.
		else{
			chomp;
			$line_buffer = $line_buffer.$_;
			
			while( length($line_buffer) > $line_width && $line_width > 0){
				print(substr($line_buffer,0,$line_width)."\n");
				$line_buffer = substr($line_buffer,$line_width);
			}
		}
	}
	# Prints last line of file.
	if($line_buffer){
		print($line_buffer);}
	print("\n");
}


sub change_dnadist
{
	my @headers;
	my @seqs;
	my $seq_counter = -1;

	while(<STDIN>){
		chomp;
		if(/^>(.+)/){
			push(@headers, $1);
			push(@seqs, "");
			$seq_counter++;
			print STDERR "Reading sequence $headers[$seq_counter]\n";
		}
		else{
			print STDERR ("\t".(length($seqs[$seq_counter]))." bp\r");
			$seqs[$seq_counter] = $seqs[$seq_counter].$_;}
	}
	
	print("\t".@headers."\t".(length($seqs[0]))."\n");
	for(my $i=0; $i<@seqs; $i++ ){
		print STDERR ("Length: ".(length($seqs[$i]))."\n");
		if($headers[$i] =~ /^(.{10})/){
			my $identifier = $1;
			$identifier =~ s/\s/_/g;
			print("$identifier\t$seqs[$i]\n");
		}
	}
}


sub count_bases
{
	my $length;

	while(<STDIN>){
		chomp;
		if($_){
			if(/^>(.+)/){
				if(defined($length)){
					print("$length\n");}
				$length = 0;
			}
			else{
				$length = length($_)+$length;}
		}
	}
	print("$length\n");
}


sub concat
{
	my $header = <STDIN>;
	print($header);
	while(<STDIN>){
		unless(/>/){
			print( $_ );}
	}
}





