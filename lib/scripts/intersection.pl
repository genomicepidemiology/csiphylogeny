#!/tools/bin/perl

# Description:
# Calculates the amount of genome positions that are found in all genomes
# analyzed.

# Input is the "ignored file" from the mapper in the pipeline.

use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use File::Basename;
use File::Spec;


my $opt_output;
my $opt_web;

GetOptions (
	"o|output=s"         => \$opt_output,
) or die("$!\n");

# Get coverage files.
my %cov_files;
my @cov_file_array;
my %valid_pos;
my $reference;

foreach my $file (@ARGV){
	open(FILE, "<$file") or die("Couldn't open file: $file\n");
	while(<FILE>){
		chomp;
		if(/# Coverage file: (\S+)/){
			push(@cov_file_array, $1);
			$cov_files{$file} = $1;
		}
		elsif(/# Valid positions: (\d+) \((.+)%\)/){
			my @tmp;
			push(@tmp, $1);
			push(@tmp, $2);
			$valid_pos{$file} = \@tmp;
		}
		elsif(/Reference: (\S+)/){
			$reference = $1;
		}

		unless(/^#/){
			last;
		}
	}
	close(FILE);
}

# Find size of genome
chomp(my $size = `gunzip -c $cov_file_array[0] | wc -l`);
# Initialize array
my @cov_array = ("0") x ($size-1);

# count non-zero genome pos.
my $debug;
foreach my $cov_file (@cov_file_array){
	my $pos = 0;
	open(COV, "gunzip -c $cov_file | ") or die("Couldn't unzip coverage file: $cov_file\n");
	while(<COV>){
		chomp;
		if($_ != 0){
			$cov_array[$pos]++;
		}
		else{
			$debug++;
		}
		$pos++;
	}
	close(COV);
}

# Handle multiple reference sequences
my %ref_offsets;
my $offset = 0;
my($header, $seq);
open(REF, "<$reference") or die("Couldn't open reference file: $reference\n");
while(<REF>){
	chomp;
	if(/^>(\S+)/){
		if($seq){
			$ref_offsets{$header} = $offset;
			$offset += length($seq);
			$seq = "";
		}
		$header = $1;
	}
	elsif($_){
		$seq .= $_;
	}
}
if($seq){
	$ref_offsets{$header} = $offset;
	$offset += length($seq);
	$seq = "";
}
close(REF);

# decrement pos in all ignored positions
foreach my $file (@ARGV){
	# Get coverage array
	my $cov_file = $cov_files{$file};
	open(COV, "gunzip -c $cov_file | ") or die("$!\n");
	chomp(my @coverage = <COV>);
	close(COV);

	open(FILE, "<$file") or die("Couldn't open file: $file\n");
	while(<FILE>){
		chomp;
		next if(/^#/);

		my($chr, $pos) = split(/\t+/);
		my $ref_ofset = $ref_offsets{$chr};
		$pos += $ref_ofset;
		my $pos_seq_coverage = $coverage[$pos-1];
		# Only substract if ignored position has been registered as coverage.
		# Some ignored SNPs are not.
		if($pos_seq_coverage > 0){
			$cov_array[$pos-1]--;
		}
	}
	close(FILE);
}

# Find pos covered by all genomes
my $genome_count = scalar(@ARGV);
open(COV_HIST, ">$opt_output.genome_cov.hist.txt") or die("Couldn't write to genome cov file: $opt_output.genome_cov.hist.txt\n");
my $index = 0;
my $tot_valid_pos = 0;
foreach my $pos (@cov_array){
	$index++;
	if($pos > $genome_count){
		die("$pos > $genome_count at pos $index\n");
	}
	elsif($pos < 0){
		die("$pos < 0 at pos $index\n");
	}
	elsif($pos == $genome_count){
		$tot_valid_pos++;
	}

	print COV_HIST ("$pos\n");
}
close(COV_HIST);

open(RESUlTS, ">$opt_output.genome_cov.results.txt") or die("Couldn't write to results file: $opt_output.genome_cov.results.txt\n");
my $valid_pct = ($tot_valid_pos/$size)*100;
print RESUlTS ("# Total valid positions: $tot_valid_pos / $size ($valid_pct)\n");
print RESUlTS ("\n# file\tvalid pos\tpct\n");

foreach (keys(%valid_pos)){
	my($filename, $directories, $suffix) = fileparse($_, qr/\.[^.]*/);
	my($count, $pct) = @{ $valid_pos{$_} };
	print RESUlTS ("$filename\t$count\t$pct\n");
}
close(RESUlTS);

exit;
