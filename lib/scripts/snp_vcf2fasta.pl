#!/tools/bin/perl -w

use strict;
use Bio::AlignIO;
use File::Temp qw/ mktemp tempfile tempdir /;
use Getopt::Long qw(:config no_ignore_case);
use File::Spec::Functions qw(rel2abs);

use constant PROGRAM_NAME            => 'snp_vcf2fasta.pl';
use constant PROGRAM_NAME_LONG       => 'Converts vcf file(s) to fasta alignment';
use constant VERSION                 => '1.0';

# Paths to required tools
my $Unzip      = `which gunzip | perl -pe 'chomp'`." -c";


# --------------------------------------------------------------------
# %% Get Input Options %%
#

my $Help;
my $Missing = "?";
my $MinCount = 1;
my $outprefix = "vcfwiz_output";
my @SampleOrder;
my $Temp_dir;

&GetOptions (
	"temp_dir:s"    => \$Temp_dir,
	"c|count=i"     => \$MinCount,    # The minimum number of samples a SNP must be in to be printed
	"o|output=s"    => \$outprefix,
	"h|help"        => \$Help         # Prints the help text
);

# Want help?
print_help() if (defined $Help);
print_usage() if (!scalar @ARGV && -t STDIN);

# --------------------------------------------------------------------
# %% Main Program %%
#

# Loop over input file(s)
my %SNPdata = process_vcf(@ARGV);

# Set working directory
my $start_dir = `pwd | perl -pe 'chomp'`;
unless(defined($Temp_dir)){
	$Temp_dir = $start_dir;
}
chdir $Temp_dir;
my $work_dir = tempdir( PROGRAM_NAME.".XXXXXXXX", CLEANUP => 1 );
print("Temp dir is: $work_dir\n");
chdir $work_dir;

# Sort each sample and concatenate snps into alignment
my $Aln_file = concatenate_snps(%SNPdata);
my $AIOin  = Bio::AlignIO->new( -file => $Aln_file,  -format => 'fasta' );
my $Aln = $AIOin->next_aln;

chdir $start_dir;
my $AIOout = Bio::AlignIO->new( -file => ">$outprefix.aln.fa", -format => 'fasta' );
$AIOout->write_aln($Aln);


exit;


# --------------------------------------------------------------------
# %% Land of the Subroutines %%
#

# Join all SNPs together into one SimpleAlign object

sub concatenate_snps {
	my %config = @_;
	my ($fh, $file) = tempfile( "alignment.XXXXXXXX" );
	for my $sample (@SampleOrder) {
		my $out;
		for my $chrom (keys %config) { # NOTE: If multiple references exist, all sequence pr. sample gets concatinated. Order is preserved, but splice points are not
			for my $pos (sort {$a <=> $b} keys %{ $config{$chrom} }) {
				$out .= exists $config{$chrom}->{$pos}->{SNP}->{$sample} ? $config{$chrom}->{$pos}->{SNP}->{$sample} : lc $config{$chrom}->{$pos}->{ref}
					if ($config{$chrom}->{$pos}->{count} >= $MinCount);
			}
		}
		print $fh ">$sample\n";
		while ($out =~ s/(.{1,60})//) {
			print $fh $1, "\n";
		}
	}
	close $fh;
	return $file;
}


# Loop over input file(s)

sub process_vcf {
	my %config;
	for my $file (@_) {
		my $vcf = ( $file =~ /.gz$/ ? "$Unzip $file" : "cat $file" );  # Check if file is zipped and unzip
		open (VCF, "$vcf |") || die "Could not open $file";
		my $SampleName = pop @{[ split /\t|\n/, `$vcf | grep "^#CHROM" | head -1` ]};;
		if(length $SampleName){
			push(@SampleOrder, $SampleName);
		}
		else{
			my($filename, $directories, $suffix) = fileparse($file, qr/\.[^.]*/);
			push(@SampleOrder, $filename);
		}
		printf STDERR "# Processing Sample: [ %-35s ]...   ", $SampleOrder[$#SampleOrder];
		while (<VCF>) {
			next if (/^#/);
			my ($chrom, $pos, $id, $ref, $alt, $qual, $filter, $info, $format, @sample) = split /\t|\n/, $_;
			next if ($info =~ /^INDEL;/); # Indels are ignored!

            #
            # This part is copied from snpfilter.pl.
            # It has to be there twice in order to keep the VCF files in the
            # right format.
            #
            if($alt =~ /,/){
                my @alts = split(/,/, $alt);
                my @gt_info = split(/:/, $sample[0]);
                my @gt_scores = split(/,/, $gt_info[1]);

                my $min_score = 1000;
                my $alt_index;

                # Only these three scores matter, since we assume haploid organism.
                # The scores represent alternative 1, 2 and 3 respectively. Found in
                # the ALT column of the VCF file.
                my @possible_indeces = qw(2 5 9);

                foreach(@possible_indeces){
                    if ($_ > @gt_scores){
                        last;
                    }
                    if($gt_scores[$_] < $min_score){
                        $min_score = $gt_scores[$_];
                        if($_ == 2){
                            $alt_index = 0;
                        }
                        elsif($_ == 5){
                            $alt_index = 1;
                        }
                        elsif($_ == 9){
                            $alt_index = 2;
                        }
                    }
                }

                $alt = $alts[$alt_index];
            }

			die "Something fishy here, Different nucleotides given for same position in reference (".$config{$chrom}->{$pos}->{ref}." vs. ".$ref.")"
				if (exists $config{$chrom}->{$pos}->{ref} && $config{$chrom}->{$pos}->{ref} ne $ref);
			$config{$chrom}->{$pos}->{ref} = $ref;
			$config{$chrom}->{$pos}->{SNP}->{$SampleOrder[$#SampleOrder]} = $alt;
			$config{$chrom}->{$pos}->{count}++;
		}
		close VCF;
		print STDERR "Done!\n";
	}
	return %config;
}


# Wait for the slave to finish

sub wait_slave {
	for (@_) {
		while (! -s $_ ) {
			sleep 5;
		}
	}
}




# --------------------------------------------------------------------
# %% Help Page/Documentation %%
#

sub print_usage {
  my $ProgName     = PROGRAM_NAME;
  die "Usage: $ProgName [Options] <File(s)>\nUse $ProgName -h for help.\n";
}


sub print_help {
  my $ProgName     = PROGRAM_NAME;
  my $ProgNameLong = PROGRAM_NAME_LONG;
  my $Version      = VERSION;
  open LESS, "| less";
  print LESS <<EOH;

NAME
	$ProgName - $ProgNameLong

SYNOPSIS
	$ProgName [Options] <File(s)>

DESCRIPTION
	Converts one or more vcf files into one file similar to a multiple
	alignment fasta file. One sample is assumed pr. vcf file each resulting
	in one entry in the alignment. Sequence output is produced for every
	position in the reference where a SNP is observed (in any sample)
	concatenated together in the same order for all entries.

	Output is a dendrogram visualizing the alignment and the alignment file
	itself, useful as input for most phylogenetic programs to create your own
	SNP trees.

	If a SNP is not observed in a given sample, the nucleotide from the
	reference sequence is given instead - in lower case - because it is
	impossible in the vcf format to distinguish if the nucleotide is missing
	in the sample or if the sample truly had the same nucleotide as the
	reference. Thus the user can subsequently filter the data as needed.

	Indels are currently ignored because there is no practical way to handle
	them given the simplicity of the output. Also, ambigious SNPs where the
	vcf file reported multiple possible nucleotides, present a problem, and
	the first base provided in the vcf file is arbitrarily chosen.

	NOTE: The program will run even on vcf files created using several
	reference sequences. Sequence data for each given reference and sample
	set is simply concatenated into one entry for each sample. If data is
	unavailable, the reference is used in lower case. This behaviour is
	sensible for a reference consisting of several contigs; the result may
	not be meaningful if distinctly different references are used.

OPTIONS

     -c or --count [integer]
	Set the minimum number of samples in which a given SNP must be observed
	for it to be printed.

     -o [string]
	Set the output prefix.
	This is the prefix for all output files. The script will produce the
	following files:
	out/pre/fix.main_tree.newick
	out/pre/fix.aln.fa

     -h or --help
	Prints this text.

VERSION
    Current: $Version

ORIGINAL AUTHOR
    Carsten Friis, carsten\@cbs.dtu.dk,

CURRENT AUTHOR
    Rolf Sommer Kaas, rkmo\@food.dtu.dk,

EOH
  close LESS;
  exit;
}
