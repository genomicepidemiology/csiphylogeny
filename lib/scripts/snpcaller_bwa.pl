#!/tools/bin/perl

# By: Rolf Kaas Mortensen
# Date: 6/3 - 2012
#
# Description:
#

use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use File::Basename;
use File::Spec;
use Cwd;
use File::Spec::Functions qw(rel2abs);
use Statistics::Descriptive;
use POSIX qw(floor);


#
# Hard coded paths
#
my $prg_bwa = "bwa";
my $prg_genomeCoverageBed = "genomeCoverageBed";
my $prg_Ks_fasta_tool = "Ks_fasta_tool.pl";
my $prg_vcfutils = "vcfutils.pl";
my $prg_samtools = "samtools";
my $prg_bcftools = "bcftools";
my $perl = "perl";


my($opt_reference, $opt_samfile, $opt_phred64, $opt_help);
my $opt_min_depth = 10;
my $opt_filter_prune = 10;
my $opt_mapper = "bwa";
my $opt_force_short_reads;
my $opt_filter_snp_quality = 0;
my $opt_filter_z = 1.96; # opt_filter_z = 1.96 -> p = 0,001
my $opt_filter_relative_depth;
my $opt_filter_map_quality = 20;
my $opt_check_length;
my $opt_quiet;
my $opt_filter_heterozygous;

GetOptions (
	"fp|filter_prune=i"           => \$opt_filter_prune,
	"fsq|filter_snp_quality:i"    => \$opt_filter_snp_quality,
	"fmq|filter_map_quality:i"    => \$opt_filter_map_quality,
	"fz|filter_z:s"               => \$opt_filter_z,
	"frd|filter_relative_depth:i" => \$opt_filter_relative_depth,
	"fh|filter_heterozygous"      => \$opt_filter_heterozygous,
	"md|min_depth=i"              => \$opt_min_depth,
	"r|reference=s"               => \$opt_reference,
	"S|Sam_file=s"                => \$opt_samfile,
	"short"                       => \$opt_force_short_reads,
	"phred64"                     => \$opt_phred64,

	"cl|check_length"             => \$opt_check_length,

	"quiet"                       => \$opt_quiet,

	"h|help"                      => \$opt_help,

	"vcfutils:s"										=> \$prg_vcfutils,
	"bwa:s"													=> \$prg_bwa,
	"fastatool:s"										=> \$prg_Ks_fasta_tool,
	"genomeCoverageBed:s"           => \$prg_genomeCoverageBed,
	"samtools:s"										=> \$prg_samtools,
	"bcftools:s"  									=> \$prg_bcftools,
	"perl:s"                        => \$perl,

) or die("USAGE: $0 --reference <fasta> <file1> [file2] \n--help for more details.\n");


#
# Handle options
#
if($opt_help){
#	&usage();
	print STDERR ("NO help yet!\n");
	exit;
}

my $phred = "--phred33";
if($opt_mapper eq "bwa"){
	$phred = "";
}

if($opt_phred64 and $opt_mapper eq "bwa"){
	$phred = "-I";
}

if($opt_filter_relative_depth){
	$opt_filter_relative_depth = ($opt_filter_relative_depth/100);
}

# Make absolute paths
$opt_reference = rel2abs($opt_reference);
$opt_samfile = rel2abs($opt_samfile);
$ARGV[0] = rel2abs($ARGV[0]);
if($ARGV[1]){
	$ARGV[1] = rel2abs($ARGV[1]);
}
my($sam_filename, $sam_directories, $sam_suffix) = fileparse($opt_samfile, qr/\.[^.]*/);

#
# Check length of read file
#
my $is_short_reads = 0; # Short is less than 70 bps.
if($opt_check_length and not $opt_force_short_reads){
	print STDERR ("# Getting read lengths.\n") unless $opt_quiet;
	my($filename1, $directories1, $suffix1) = fileparse($ARGV[0], qr/\.[^.]*/);

	my $extract = "cat ";
	$extract = "gunzip -c " if($suffix1 eq ".gz");
	open(READ_FILE, "$extract $ARGV[0] |") or die("Couldn't open read file: $ARGV[0]\n");
	my $prev_is_plus = 1;
	my $prev_length;
	my $lenght_tot = 0;
	my $read_counter = 0;
	while(<READ_FILE>){
		if(/^@/ and not $prev_is_plus){
			$prev_is_plus = 0;
			$read_counter++;
			my $dna = <READ_FILE>;
			unless($dna){
				warn ("! Read file seems truncated.\n");
				warn ("!  - Truncated file: $ARGV[0]\n");
				last;
			}
			chomp($dna);
			$lenght_tot += length($dna);
		}
		elsif(/^\+/){
			$prev_is_plus = 1;
		}
		else{
			$prev_is_plus = 0;
		}
		if($read_counter >= 1000){
			last;
		}
	}
	close(READ_FILE);

	my $avg_length = $lenght_tot/$read_counter;
	if($avg_length < 70){
		$is_short_reads = 1;
		print STDERR ("#\tShort ($avg_length < 70): $filename1\n") unless $opt_quiet;
	}
	else{
		print STDERR ("#\tLong ($avg_length > 70): $filename1\n") unless $opt_quiet;
	}
}

if($opt_force_short_reads){
	$is_short_reads = 1;
	print STDERR ("# Force short option\n") unless $opt_quiet;
}

# Get reference base count
my $ref_size = 0;
open(COUNT, "$perl $prg_Ks_fasta_tool --get_base_count < $opt_reference |") or die("Couldn't read reference: $opt_reference\n");
while(<COUNT>){
	chomp;
	$ref_size += $_;
}
close(COUNT);

# Map to reference
# Use BWA MEM
if($opt_mapper eq "bwa" and not $is_short_reads){
	print STDERR ("# Mapper: bwa mem\n") unless $opt_quiet;
	if( scalar(@ARGV) == 2 and not -s "$sam_directories/$sam_filename.sorted.bam" ){
		my($read1_filename, $read1_directories, $read1_suffix) = fileparse($ARGV[0], qr/\.[^.]*/);
		my($read2_filename, $read2_directories, $read2_suffix) = fileparse($ARGV[1], qr/\.[^.]*/);
    print STDERR ("CMD: $prg_bwa mem $opt_reference $ARGV[0] $ARGV[1] > $opt_samfile\n");
		system("$prg_bwa mem $opt_reference $ARGV[0] $ARGV[1] > $opt_samfile");
	}
	elsif( scalar(@ARGV) == 1 and not -s "$sam_directories/$sam_filename.sorted.bam" ){
		my($read1_filename, $read1_directories, $read1_suffix) = fileparse($ARGV[0], qr/\.[^.]*/);
		system("$prg_bwa mem $opt_reference $ARGV[0] > $opt_samfile");
	}
	elsif(scalar(@ARGV) > 2 or scalar(@ARGV) == 0){
		die("Wrong number of arguments given. Please use only one for single end read files and two for paired end read files.\n");
	}
}
# Use BWA SAMPE or SAMSE if the read lengths are shorter than 70.
elsif($opt_mapper eq "bwa" or $opt_mapper eq "bwa_old"){
	print STDERR ("# Mapper: bwa aln\n") unless $opt_quiet;
	if( scalar(@ARGV) == 2 and not -s "$sam_directories/$sam_filename.sorted.bam" ){
		my($read1_filename, $read1_directories, $read1_suffix) = fileparse($ARGV[0], qr/\.[^.]*/);
		my($read2_filename, $read2_directories, $read2_suffix) = fileparse($ARGV[1], qr/\.[^.]*/);
		my $bwa_aln_file1 = "$sam_directories/$read1_filename.sai";
		my $bwa_aln_file2 = "$sam_directories/$read2_filename.sai";

		system("$prg_bwa aln $phred $opt_reference $ARGV[0] > $bwa_aln_file1");
		system("$prg_bwa aln $phred $opt_reference $ARGV[1] > $bwa_aln_file2");
		system("$prg_bwa sampe -a 1000 -f $opt_samfile $opt_reference $bwa_aln_file1 $bwa_aln_file2 $ARGV[0] $ARGV[1]") == 0 or die("Mapping failed for $ARGV[0] and $ARGV[1]\n");
		system("rm $bwa_aln_file1");
		system("rm $bwa_aln_file2");
	}
	elsif( scalar(@ARGV) == 1 and not -s "$sam_directories/$sam_filename.sorted.bam" ){
		my($read1_filename, $read1_directories, $read1_suffix) = fileparse($ARGV[0], qr/\.[^.]*/);
		my $bwa_aln_file1 = "$sam_directories/$read1_filename.sai";

		system("$prg_bwa aln $phred $opt_reference $ARGV[0] > $bwa_aln_file1");
		system("$prg_bwa samse -f $opt_samfile $opt_reference $bwa_aln_file1 $ARGV[0]") == 0 or die("Mapping failed for $ARGV[0]\n");
		system("rm $bwa_aln_file1");
	}
	elsif(scalar(@ARGV) > 2 or scalar(@ARGV) == 0){
		die("Wrong number of arguments given. Please use only one for single end read files and two for paired end read files.\n");
	}
}


#
# Call SNPs
#

# Index reference
if(not -s "$opt_reference.fai"){
	die("Missing samtools index file for: $opt_reference\n");
}
# Process Alignment
if(not -s "$sam_directories/$sam_filename.sorted.bam"){
	system("$prg_samtools view -bS $opt_samfile | $prg_samtools sort - $sam_directories/$sam_filename.sorted");
}
if(-s $opt_samfile){
	system("rm $opt_samfile");
}

#
# Create coverage stats for sample
#

# Reference contigs/genes are recorded
open(REF, "grep \">\" $opt_reference |") or die("Couldn't open reference file: $opt_reference.\n");
my @ref_headers;
while(<REF>){
	if(/>(\S+)/){
		push(@ref_headers, $1);
	}
}
close(REF);

open(REF_COUNT, "$perl $prg_Ks_fasta_tool --get_base_count < $opt_reference |") or die("Couldn't open reference file with Ks_fasta_tools.\n");
my $ref_index = -1;
my %ref_sizes;
while(<REF_COUNT>){
	$ref_index++;
	chomp;
	$ref_sizes{$ref_headers[$ref_index]} = $_;
}
close(REF_COUNT);

# Coverage file is created
$ref_index = -1;
my @coverage;

if(not -s "$sam_directories/$sam_filename.coverage.txt.gz"){

	# The coverage of each ref sequence is found
	# Note sequences with no coverage will not be found here
	open(COVERAGE, "$prg_genomeCoverageBed -ibam $sam_directories/$sam_filename.sorted.bam -d | awk '\{print \$1,\$3\}' |") or die("Couldn't open coverage pipe: $prg_genomeCoverageBed -ibam $sam_directories/$sam_filename.sorted.bam -d | awk {print $3} |");
	my $prev_header;
	my @current_cov;
	my %coverage_hash;

	while(<COVERAGE>){
		chomp;
		my($header,$cov) = split(/\s+/);
		if(not defined($prev_header) or $header eq $prev_header){
			push(@current_cov, $cov);
			$prev_header = $header;
		}
		elsif( defined($prev_header) ){
			my @tmp = @current_cov;
			$coverage_hash{$prev_header} = \@tmp;
			@current_cov = ($cov);
			$prev_header = $header;
		}
	}
	close(COVERAGE);

	# Stores last contig coverage
	if( defined($prev_header) ){
		my @tmp = @current_cov;
		$coverage_hash{$prev_header} = \@tmp;
	}

	# Coverage array is created
	# Sequences not found will be filled coverage 0
	@coverage = ();
	foreach my $ref_head (@ref_headers){
		if( $coverage_hash{$ref_head} ){
			push(@coverage, @{$coverage_hash{$ref_head}});
		}
		else{
			my $size = $ref_sizes{$ref_head};
			my @tmp = ("0") x $size;
			push(@coverage, @tmp);
		}
	}

	# Check if number of bp is that of the reference, else redo
	my $is_gzip_ok = 0;
	my $loop_count = 0;
	while(not $is_gzip_ok and $loop_count < 4){
		$loop_count++;

		my $bp_count = 0;
		open(COV_OUT, "| gzip -c >$sam_directories/$sam_filename.coverage.txt.gz") or die("Couldn't open gzip pipe: | gzip -c >$sam_directories/$sam_filename.coverage.txt.gz\n");
		foreach(@coverage){
			print COV_OUT ("$_\n");
			$bp_count++;
		}
		close(COV_OUT);

		# hmmm..? Bigger?
		if($bp_count >= $ref_size){
			$is_gzip_ok = 1;
		}
		else{
			print STDERR ("GZIP ERROR $sam_filename: $bp_count is not the expected size: $ref_size\n");
		}
	}

	die("GZIP failed 3 times... Giving up!\n") unless($is_gzip_ok);
}
else{
	open(COVERAGE, "gunzip -c $sam_directories/$sam_filename.coverage.txt.gz |") or die("Couldn't open gunzip pipe: gunzip -c $sam_directories/$sam_filename.coverage.txt.gz |\n");
	chomp(@coverage = <COVERAGE>);
	close(COVERAGE);
}

my $cov_stat = Statistics::Descriptive::Full->new();
$cov_stat->add_data(@coverage);
my $cov_std  = $cov_stat->standard_deviation();
my $cov_median = $cov_stat->median();
my $cov_mean = $cov_stat->mean();
my $rel_depth_thresshold = 0;
if($opt_filter_relative_depth){
	$rel_depth_thresshold = floor($cov_mean*$opt_filter_relative_depth);
}

my %ref_ignore_pos;

if(not -s "$sam_directories/$sam_filename.raw.bcf"){
	system("$prg_samtools mpileup -uf $opt_reference $sam_directories/$sam_filename.sorted.bam | $prg_bcftools view -bvcg - > $sam_directories/$sam_filename.raw.bcf");

	#
	# Check non-SNP positions
	#
	my $pos_stream_cmd = "$prg_samtools mpileup -uf $opt_reference $sam_directories/$sam_filename.sorted.bam | $prg_bcftools view - |";
	open(POS, $pos_stream_cmd) or die("Couldn't open stream: $pos_stream_cmd\n");
	my $ref_pos = 1;
	my $current_contig;
	my $prev_contig;
	my $prev_pos = 0;
	while(<POS>){
		next if(/^#/);
		my @entries = split(/\t+/);
		$current_contig = $entries[0];
		unless($prev_contig){
			$prev_contig = $current_contig;
		}
		my $current_pos = $entries[1];

		if($prev_contig ne $current_contig){
			$ref_pos = 1;
		}
		# Due to INDELS some positions can occur several times
		elsif($prev_pos == $current_pos){
			$ref_pos--;
		}
		my $pos_diff = $current_pos-$ref_pos;

		if($pos_diff > 0){
			# pos cursor not in sync some pos must be missing (no coverage)
			for(my $i=0; $i<$pos_diff; $i++){
				$ref_ignore_pos{$current_contig}->{$ref_pos} = 0; # 0 means "missing"
				$ref_pos++;
			}
		}

		# DEBUG
		if($ref_pos != $current_pos){
			die("A BUG somewhere caused the ref cursors to be out of sync!\nREF cursor: $ref_pos\nCUR cursor: $current_pos\n");
		}

		# Now ref and current are in sync, which means that some data i covering this position.
		my($depth_str, $info_str) = split(/;/, $entries[7]);
		my $ref_depth;
		if($depth_str =~ /DP=(\d+)/){
			$ref_depth = $1;
			# Check ref pos depth
			if($ref_depth < $opt_min_depth or $ref_depth < $rel_depth_thresshold){
				$ref_ignore_pos{$current_contig}->{$ref_pos} = "-$ref_depth"; # negative means ignored due to low depth
			}
			else{
				my @info;
				if($info_str =~ /^I16=(.+)/){
					@info = split(/,/, $1);
				}
				my $ref_qual_cov = $info[0]+$info[1];
				my $alt_qual_cov = $info[2]+$info[3];
				# Checks if both ref base calls and alt base calls are below coverage thressholds.
				# These coverage counts are ONLY of base calls with phred scores >= 13. (Q13 bases).
				if( ($ref_qual_cov<$opt_min_depth or $ref_qual_cov<$rel_depth_thresshold) and ($alt_qual_cov<$opt_min_depth or $alt_qual_cov<$rel_depth_thresshold) ){
					my $log_val;
					if($ref_qual_cov > $alt_qual_cov){
						$log_val = $ref_qual_cov;
					}
					else{
						$log_val = $alt_qual_cov;
					}
					$ref_ignore_pos{$current_contig}->{$ref_pos} = "$log_val";
				}
			}
		}

		$ref_pos++;
		$prev_contig = $current_contig;
		$prev_pos = $current_pos;
	}
	close(POS);

}
my $out_filter_qual_count = 0;
my $out_filter_z_count = 0;
my $out_filter_yx10 = 0;
my $out_filter_yx5 = 0;
my $out_filter_indel = 0;
my $out_filter_ambigious = 0;
my $out_filter_01 = 0;

## DEBUG ##

open(DEBUG, ">$sam_directories/$sam_filename.zscore_calc.txt");

##########

if(not -s "$sam_directories/$sam_filename.unpruned.flt.vcf"){
	# Q: Mapping quality d: min depth
	system("$prg_bcftools view $sam_directories/$sam_filename.raw.bcf | $prg_vcfutils varFilter -Q $opt_filter_map_quality -d $opt_min_depth > $sam_directories/$sam_filename.rough_flt.vcf");

	# Sorting on "Qual" column
	# Sorting on Oles statistic

	open(FILTERED, ">$sam_directories/$sam_filename.unpruned.flt.vcf") or die("Couldn't write to: $sam_directories/$sam_filename.unpruned.flt.vcf\n");
	open(UNPRUNED, "<$sam_directories/$sam_filename.rough_flt.vcf") or die("Couldn't open: $sam_directories/$sam_filename.rough_flt.vcf\n");

	while(<UNPRUNED>){
		if (/^#/){
			print FILTERED ("$_");
			next;
		}

		my @entries = split(/\t/);

		# Sorting out indels
		if($entries[7] =~ "^INDEL"){
			$out_filter_indel++;
			next;
		}

		# Qual column
		if ($entries[5] < $opt_filter_snp_quality){
			$out_filter_qual_count++;
			next;
		}

		# Relative depth
		if($opt_filter_relative_depth){
			if($entries[7] =~ /DP\=(\d+)\;/){
				my $depth = $1;
				next if ($depth < $rel_depth_thresshold);
			}
			else{
				die("Couldn't find DP in $entries[7] part of $_\n");
			}
		}

		# Choose only the most abundant variant.
		#    - Can be sorted out later
		if($entries[4] =~ /,/){
			$out_filter_ambigious++;
			next;
		}

		# Z-score
		if($opt_filter_z){
			if($entries[7] =~ /DP4\=(\d+)\,(\d+)\,(\d+)\,(\d+)/){
				my $ref_forward = $1;
				my $ref_reverse = $2;
				my $alt_forward = $3;
				my $alt_reverse = $4;

				my $y = $ref_forward + $ref_reverse;
				my $x = $alt_forward + $alt_reverse;

				my $z = ($x-$y)/(sqrt($x+$y));

				# DEBUG
				print DEBUG ("$_");
				print DEBUG ("$ref_forward - $ref_reverse - $alt_forward - $alt_reverse\n");
				print DEBUG ("($x - $y) / sqrt($x + $y)\n");
				print DEBUG ("Z = $z\n");
				#######

				if ($opt_filter_z > $z){
					$out_filter_z_count++;
					next;
				}

				# Filter out "heterozygous" alleles, prob. mix of colonies.
				if ($entries[9] =~ /^0\/1/){
					$out_filter_01++;
          if($opt_filter_heterozygous){
            next;
          }
				}

				# Disabled filter
				if($x < $y*5){
					$out_filter_yx5++;
					#next;
				}
				# Disabled filter
				if($x < $y*10){
					$out_filter_yx10++;
					#next;
				}
			}
			else{
				die("Couldn't find DP4 in $entries[7] part of $_\n");
			}
		}

		print FILTERED ("$_");
	}
	close(FILTERED);
	close(UNPRUNED);
}


## DEBUG ##

close(DEBUG);

##########

my %pruned_snps;
my $prev_pos = -1000000;
if(not -s "$sam_directories/$sam_filename.ignored_snps.txt"){
	# Note all SNPs before filtering
	open(VCF, "$prg_bcftools view $sam_directories/$sam_filename.raw.bcf |") or die("Couldn't open vcf file: $sam_directories/$sam_filename.raw.bcf\n");
	my %unfiltered_snps;
	my $prev_contig_id;

	while(<VCF>){
		chomp;
		next if (/^#/);
		my @entries = split(/\t/);

		my $contig_id = $entries[0];

		unless($prev_contig_id){
			$prev_contig_id = $contig_id;
		}

		my $pos = $entries[1];
		my $pos_dif = $pos-$prev_pos;
		$unfiltered_snps{$contig_id}->{$pos} = 1;

		if($prev_contig_id eq $contig_id and $pos_dif < $opt_filter_prune){
			$pruned_snps{$contig_id}->{$pos} = 1;
			$pruned_snps{$contig_id}->{$prev_pos} = 1;
		}

		$prev_contig_id = $contig_id;
		$prev_pos = $pos;

	}
	close(VCF);

	# Annotate quality SNPs and prune.
	my %filtered_snps;
	my $counter_prune = 0;
	my $counter_ambiguous = 0;
	open(PRUNED, ">$sam_directories/$sam_filename.flt.vcf") or die("Couldn't write pruned VCF file: $sam_directories/$sam_filename.flt.vcf\n");
	open(FLT, "<$sam_directories/$sam_filename.unpruned.flt.vcf") or die("Couldn't open filtered VCF file: $sam_directories/$sam_filename.unpruned.flt.vcf\n");

	while(<FLT>){
		if (/^#/){
			print PRUNED "$_";
			next;
		}
		chomp;
		my @entries = split(/\t/);

		# Sort out SNPs with several different base calls, and prune
		if(not $entries[7] =~ /^INDEL/){

			my $contig_id = $entries[0];

			# Prune
			if(not $pruned_snps{$contig_id}->{$entries[1]}){
				print PRUNED "$_\n";
				$filtered_snps{$contig_id}->{$entries[1]} = 1;
			}
			else{
				$counter_prune++;
			}
		}
		else{
			$counter_ambiguous++;
		}
	}
	close(FLT);
	close(PRUNED);

	# List ignored reference positions:
	my $bad_ref_pos = "";
	my $bad_ref_info = "";
	my $count_bad_depth = 0;
	my $count_ref_missing = 0;
	my $count_bad_qual = 0;
	foreach my $contig_id (keys(%ref_ignore_pos)){
		foreach my $pos (keys( %{$ref_ignore_pos{$contig_id}} )){
			my $val = $ref_ignore_pos{$contig_id}->{$pos};
			if(defined($val)){
				if($val == 0){
					$count_ref_missing++;
				}
				elsif($val < 0){
					$count_bad_depth++;
				}
				else{
					$count_bad_qual++;
				}
				$bad_ref_pos .= "$contig_id\t$pos\n";
			}
		}
	}

	$bad_ref_info .= "# Reference positions missing: $count_ref_missing\n";
	$bad_ref_info .= "# Reference positions with insufficient depth: $count_bad_depth\n";
	$bad_ref_info .= "# Reference positions with insufficient ref/alt Q13 base depth: $count_bad_qual\n";

	my $valid_positions = $ref_size - $count_ref_missing - $count_bad_depth - $count_bad_qual - $counter_prune;
	print STDERR ("CALC: $valid_positions = $ref_size - $count_ref_missing - $count_bad_depth - $count_bad_qual - $counter_prune");

	# Create list of SNPs left out
	my $bad_snps = "# File containing SNPs that was filtered out\n";
	$bad_snps .= "# Reference: $opt_reference\n";
	$bad_snps .= "# Sample(s): $ARGV[0]";
	if($ARGV[1]){
		$bad_snps .= ", $ARGV[1]";
	}
	$bad_snps .= "\n# Sorted BAM file: $sam_directories/$sam_filename.sorted.bam\n";
	$bad_snps .= "# VCF file of kept SNPs: $sam_directories/$sam_filename.flt.vcf\n";
	$bad_snps .= "# Coverage file: $sam_directories/$sam_filename.coverage.txt.gz\n";
	$bad_snps .= "# Coverage mean: $cov_mean\n";
	$bad_snps .= "# Coverage median: $cov_median\n";
	$bad_snps .= "# Coverage standard deviation: $cov_std\n";
	$bad_snps .= "# SNPs filtered on qual column: $out_filter_qual_count\n";
	$bad_snps .= "# SNPs filtered on Z-score: $out_filter_z_count\n";
	$bad_snps .= "# SNPs filtered on 0/1 flag in VCF file: $out_filter_01\n";
	$bad_snps .= "# INACTIVE FILTER: SNPs filtered on >y*10: $out_filter_yx10\n";
	$bad_snps .= "# INACTIVE FILTER: SNPs filtered on >y*5: $out_filter_yx5\n";
	$bad_snps .= "# INACTIVE FILTER: Ambigious SNPs filtered: $out_filter_ambigious\n";
	$bad_snps .= "# Pruning set to: $opt_filter_prune\n";
	$bad_snps .= "# SNPs pruned: $counter_prune\n";
	$bad_snps .= "# INDELS removed: $out_filter_indel\n";
	$bad_snps .= "# Valid positions: $valid_positions (".(($valid_positions/$ref_size)*100)."%)\n";
	$bad_snps .= $bad_ref_info;
	$bad_snps .= $bad_ref_pos;

	foreach my $contig_id (keys(%unfiltered_snps)){
		foreach my $pos (keys( %{$unfiltered_snps{$contig_id}} )){
			if(not defined($filtered_snps{$contig_id}->{$pos})){
				# Makes sure positions aren't ignored twice.
				unless(defined( $ref_ignore_pos{$contig_id}->{$pos} )){
					$bad_snps .= "$contig_id\t$pos\n";
				}
			}
		}
	}

	open(BAD, ">$sam_directories/$sam_filename.ignored_snps.txt") or die("Couldn't write to file: $sam_directories/$sam_filename.ignored_snps.txt");
	print BAD ($bad_snps);
	close(BAD);
}

print("Job completed\n");
exit;


#
# Functions
#

sub checkpath {
	my $path = shift;
	unless( -s $path ){
		die("$path path not found!\n");
	}
	return 0;
}


sub usage {
	while (<DATA>) {
		print $_;
	}
	close DATA;
 exit 1;
}
