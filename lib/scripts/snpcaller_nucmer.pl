#!/tools/bin/perl -w

use strict;
use diagnostics;
use File::Temp qw/ mktemp tempfile tempdir /;
use File::Basename;
use File::Spec;
use Getopt::Long;
use File::Spec::Functions qw(rel2abs);

use constant PROGRAM_NAME            => 'nucmer2vcf.pl';
use constant PROGRAM_NAME_LONG       => 'Runs nucmer, applies filtering and converts the result to VCF 4.0';
use constant VERSION                 => '0.1';

# Paths to required tools
my $perl = $^X;
my $nucmer = "nucmer";
my $show_snps = "show-snps";
my $show_coords = "show-coords";
my $delta_filter = "delta-filter";
my $prg_Ks_fasta_tool;
my $unzip = `which gunzip | perl -pe 'chomp'`." -c";

#
# Get Input Options
#

my ($opt_working_dir, $opt_reference, $opt_genome, $opt_sample, $help, $opt_ignored);
my $opt_prune = 0;
my $opt_length = 0;
my $opt_end_dist = 0;

&GetOptions (
	"r|reference=s"    => \$opt_reference,   # Path to reference file
	"g|genome=s"       => \$opt_genome,      # Path to genome file.
	"p|prune=i"        => \$opt_prune,       # max length between mismatches.
	"e|end_distance=i" => \$opt_end_dist,    # max length to sequence ends
	"l|length=i"       => \$opt_length,      # Min. seq length
	"i|ignored"        => \$opt_ignored,     # Used in conjunction with the SNP tree pipeline.
	"w|working_dir=s"  => \$opt_working_dir, # Use another working dir than current.
	"nucmer=s"         => \$nucmer,
	"show_snps=s"      => \$show_snps,
	"show_coords=s"    => \$show_coords,
	"delta_filter=s"   => \$delta_filter,
	"fastatool=s"      => \$prg_Ks_fasta_tool,
	"perl=s"           => \$perl,
	"h|help"           => \$help             # Prints the help text
);


if($help){
	&print_help();}


#
# Read reference genome
#
# The genome file is only used to make sure the IDs are the same. It is not
# really necessary.
my %genome_ids;
if($opt_genome){
	open(GENOME, "<$opt_genome") or die("Couldn't open genome file: $opt_genome.\n");
	while(<GENOME>){
		my @entry = split('\t');
		$genome_ids{$entry[0]} = $entry[1];
	}
	close(GENOME);
}

my %reference_ids;
my @ref_headers;
open(REF, "<$opt_reference") or die("Couldn't open reference file: $opt_reference.\n");
while(<REF>){
	if(/^>(.+?)\s+/){
		my $ref_id = $1;
		push(@ref_headers, $1);
		if($opt_genome){
			if(  exists($genome_ids{$ref_id})  ){
				$reference_ids{$ref_id} = 1;}
			else{
				die("Reference fasta header:$ref_id didn't match any of the IDs found in the genome file.\n");}
		}
		else{
			$reference_ids{$ref_id} = 1;}
	}
}
close(REF);

my $ref_size = 0;
my $ref_index_count = -1;
open(COUNT, "$perl $prg_Ks_fasta_tool --get_base_count < $opt_reference |") or die("Couldn't read reference: $opt_reference\n");
my %ref_sizes;
while(<COUNT>){
	$ref_index_count++;
	chomp;
	$ref_size += $_;
	$ref_sizes{$ref_headers[$ref_index_count]} = $_;
}
close(COUNT);

#
# nucmer all input files, store delta in temp
#
my $Wkdir = `pwd | perl -pe 'chomp'`;
if($opt_working_dir){
	$opt_working_dir = rel2abs($opt_working_dir);
	die("$opt_working_dir does not seem to be an existing directory.\n") unless(-d $opt_working_dir);
	$Wkdir = $opt_working_dir;
}
my $org_dir = $Wkdir;
#my $start_dir = "$Wkdir";
$Wkdir .= "/".tempdir( PROGRAM_NAME.".XXXXXXXX", CLEANUP => 1 );
chdir $Wkdir;
my @files = &get_and_unpack_files(@ARGV);

my $counter=0;
my @delta_files = ();
foreach my $fh (@files){
	$counter++;
	my $rnd_no = rand();
	my $tmp_delta = "nucmer_$counter"."_$rnd_no";
	push(@delta_files, $tmp_delta.".delta");
	my($filename, $directories, $suffix) = fileparse($fh, qr/\.[^.]*/);

	# Nucmer doesn't accept duplicate fasta headers. This is checked and handled
	open(QUERY, "<$fh") or die("Couldn't open $fh.\n");
	my %headers;
	my $douplicates_found = 0;
	while(<QUERY>){
		chomp;
		if(/>(.+)/){
			if(defined($headers{$1})){
				close(QUERY);
				$douplicates_found = 1;
				undef(%headers);
				last;
			}
			else{
				$headers{$1} = 1;}
		}
	}
	if($douplicates_found){
		open(RENAMED, ">$fh.renamed") or die("couldn't write renamed file:$fh.renamed.\n");
		open(QUERY, "<$fh") or die("Couldn't open $fh.\n");
		my $q_counter = 0;
		while(<QUERY>){
			chomp;
			if(/>(.+)/){
				my $new_header = $1;
				$new_header =~ s/\s+/_/g;
				$q_counter++;
				print RENAMED (">$new_header"."_INDEX_"."$q_counter\n");
			}
			else{
				print RENAMED ("$_\n");}
		}
		close(QUERY);
		close(RENAMED);

		$fh = "$fh.renamed";
	}

	unless($opt_reference =~ /^\// or $opt_reference =~ /^\~/){
		$opt_reference = "$org_dir/$opt_reference";}

	system("$nucmer --prefix=$tmp_delta $opt_reference $fh 2> /dev/null");
	system("$delta_filter -1 $tmp_delta.delta > nucmer_$counter"."_$rnd_no"."_flt.delta");
	system("$show_coords -crT nucmer_$counter"."_$rnd_no"."_flt.delta > nucmer_$counter"."_$rnd_no"."_flt.coords");
	close($fh);
}

#
# Create hash of aligned ref regions
#
my %aligned_refs;
foreach my $delta_file (@delta_files){
	my $genome_aligned = 0;
	my($filename, $directories, $suffix) = fileparse($delta_file, qr/\.[^.]*/);
	open(COORDS, "< $filename"."_flt.coords") or die("Couldn't open file: $filename"."_flt.coords");
	<COORDS>;
	<COORDS>;
	<COORDS>;
	<COORDS>;
	while(<COORDS>){
		chomp;
		my @entries = split(/\t/);
		# Hash: filename -> header -> start of aln = end of aln
		$aligned_refs{$delta_file}->{$entries[9]}->{$entries[0]} = $entries[1];
		$genome_aligned += $entries[4];
	}
	close(COORDS);
	$aligned_refs{$delta_file}->{genome_cov} = $genome_aligned;
}

#
# show-snps -> create vcf files (filter?)
#
my $counter_file = -1;
foreach (@delta_files){
	chomp;
	my $delta_file = $_;
	$counter_file++;
	my $filtered_snps = 0;
	my $called_snps = 0;

	my($filename, $directories, $suffix) = fileparse($files[$counter_file], qr/\.[^.]*/);
	my $output_file = "$org_dir/$filename.vcf";
	print STDERR ("Writing to: $org_dir/$filename.vcf\n");
	open(VCF, ">$output_file") or die("Couldn't write to: $output_file.\n");

	&print_vcf_header($filename);

	my $cmd = "$show_snps -CIlrT $_";
	open(SNPS, "$cmd |") or die("Couldn't open: $cmd.\n");
	my $id_prev = "";
	my $prev_pos = 0;
	my $buffer = "";
	my ($current_pos, $ref_snp, $alt_snp, $id_current);
	<SNPS>;
	<SNPS>;
	<SNPS>;
	<SNPS>;
	while(<SNPS>){
		chomp;
		my @entry = split('\t');
		$current_pos = $entry[0];
		$ref_snp = $entry[1];
		$alt_snp = $entry[2];
		my $mismatch_dist = $entry[4];
		my $end_dist = $entry[5];
		my $query_length = $entry[7];
		$id_current = $entry[10];
		# If length is good AND (mismatch is above thresshold OR nearest mismatch is a sequence end)
		if($query_length >= $opt_length and ( $mismatch_dist > $opt_prune || $mismatch_dist == $end_dist) and $end_dist >= $opt_end_dist){
			#STill need to note SNP positions in case:   SNP1_4bp_SNP2_2bp_END     This case should be discarded.

			#Same contig/sequence
			if($id_prev eq $id_current){
				if($prev_pos and $opt_prune >= ($current_pos-$prev_pos) ){
					$buffer = "";
					$prev_pos = $current_pos;
					$id_prev = $id_current;
					$filtered_snps++;
					next;
				}
				else{
					if($buffer){
						$called_snps++;
						print VCF ("$buffer\n");
					}
					$buffer = "$id_current\t$current_pos\t.\t$ref_snp\t$alt_snp\t.\t.\t.";
					$prev_pos = $current_pos;
					$id_prev = $id_current;
				}
			}
			else{
				if($buffer){
					$called_snps++;
					print VCF ("$buffer\n");
				}
				$buffer = "$id_current\t$current_pos\t.\t$ref_snp\t$alt_snp\t.\t.\t.";
				$prev_pos = $current_pos;
				$id_prev = $id_current;
			}
		}
		else{
			$filtered_snps++;
			next;
		}
	}
	if($buffer){
		$called_snps++;
		print VCF ("$buffer\n");
	}
	$buffer = "";

	if($opt_ignored){
		my $bad_snps = "# File containing SNPs that was filtered out\n";
		$bad_snps .= "# Reference: $opt_reference\n";
		$bad_snps .= "# Sample(s): $files[$counter_file]\n";
		$bad_snps .= "# VCF file of kept SNPs: $output_file\n";
		$bad_snps .= "# Coverage file: $org_dir/$filename.coverage.txt.gz\n";
		my $valid_positions = $aligned_refs{$delta_file}->{genome_cov};
		$bad_snps .= "# Valid positions: $valid_positions (".(($valid_positions/$ref_size)*100)."%)\n";

		open(IGN, ">$org_dir/$filename.ignored_snps.txt") or die("Couldn't write to file: $org_dir/$filename.ignored_snps.txt");
		print IGN ("$bad_snps");
		close(IGN);

		# Write ignored positions for all sites in reference that didn't align.
		my $coverage_str = "";
		foreach my $contig (@ref_headers){
			my $contig_size = $ref_sizes{$contig};
			my @contig_cov = (0) x $contig_size;

			my $pos = 0;
			# If any seq is aligned to this contig
			if(defined( $aligned_refs{$delta_file}->{$contig} )){
				my @aln_start_pos = sort {$a <=> $b} keys( %{$aligned_refs{$delta_file}->{$contig}} );

				for(my $i=0; $i<@aln_start_pos; $i++){
					my $aln_start = $aln_start_pos[$i];
					my $aln_end = $aligned_refs{$delta_file}->{$contig}->{$aln_start};

					# Make 0-index
					$aln_start--;
					$aln_end--;

					$pos = $aln_start;

					# Mark positions in aln
					# Note query contigs can overlap the same ref sequence.
					while($pos >= $aln_start and $pos <= $aln_end){
						$contig_cov[$pos]++;
						$pos++;
					}
				}
			}

			foreach(@contig_cov){
				$coverage_str .= "$_\n";
			}
		}
		open(COV, ">$org_dir/$filename.coverage.txt") or die("Couldn't write to coverage file: $org_dir/$filename.coverage.txt\n");
		print COV ($coverage_str);
		close(COV);
		system("gzip $org_dir/$filename.coverage.txt");
	}

	print STDERR ("$filename: Filtered out $filtered_snps SNPs.\n");
	print STDERR ("$filename: Called $called_snps SNPs.\n");

	close(VCF);
}

exit;



sub get_and_unpack_files{
	my @file_list = ();
	for my $file (@_) {
		chomp($file);
		if($file =~ /\.gz$/){
			unless($file =~ /^\// or $file =~ /^~/){
				$file = "$org_dir/$file";}
			my ($fh_tmp, $file_tmp) = tempfile( "unpackQ.XXXXXXXXX" );
			`$unzip $file >$fh_tmp`;
			push(@file_list, $file_tmp);
		}
		else{
			if($file =~ /^\// or $file =~ /^~/){
				push(@file_list, "$file");}
			else{
				push(@file_list, "$org_dir/$file");}
		}
	}
	return @file_list;
}


sub print_vcf_header{
	my $sample = shift;
	print VCF <<EOH;
##fileformat=VCFv4.0
##source=MUMmerVersion=3.23 with custom filter
##INFO=<ID=DP,Number=1,Type=Integer,Description="Raw read depth">
##INFO=<ID=DP4,Number=4,Type=Integer,Description="# high-quality ref-forward bases, ref-reverse, alt-forward and alt-reverse bases">
##INFO=<ID=MQ,Number=1,Type=Integer,Description="Root-mean-square mapping quality of covering reads">
##INFO=<ID=FQ,Number=1,Type=Float,Description="Phred probability of all samples being the same">
##INFO=<ID=AF1,Number=1,Type=Float,Description="Max-likelihood estimate of the site allele frequency of the first ALT allele">
##INFO=<ID=G3,Number=3,Type=Float,Description="ML estimate of genotype frequencies">
##INFO=<ID=HWE,Number=1,Type=Float,Description="Chi^2 based HWE test P-value based on G3">
##INFO=<ID=CI95,Number=2,Type=Float,Description="Equal-tail Bayesian credible interval of the site allele frequency at the 95% level">
##INFO=<ID=PV4,Number=4,Type=Float,Description="P-values for strand bias, baseQ bias, mapQ bias and tail distance bias">
##INFO=<ID=INDEL,Number=0,Type=Flag,Description="Indicates that the variant is an INDEL.">
##INFO=<ID=PC2,Number=2,Type=Integer,Description="Phred probability of the nonRef allele frequency in group1 samples being larger (,smaller) than in group2.">
##INFO=<ID=PCHI2,Number=1,Type=Float,Description="Posterior weighted chi^2 P-value for testing the association between group1 and group2 samples.">
##INFO=<ID=QCHI2,Number=1,Type=Integer,Description="Phred scaled PCHI2.">
##INFO=<ID=PR,Number=1,Type=Integer,Description="# permutations yielding a smaller PCHI2.">
#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\t$sample
EOH
#AP009048_AB001340_D10483_D26562_D83536_D90699-D90711_D90713     58      .       G       C       203     .       DP=17;AF1=1;CI95=1,1;DP4=0,0,17,0;MQ=60;FQ=-51  GT:PL:GQ        1/1:203,51,0:99
}


sub print_help {
  my $ProgName     = PROGRAM_NAME;
  my $ProgNameLong = PROGRAM_NAME_LONG;
  my $Version      = VERSION;
  print STDERR <<EOH;

NAME
	$ProgName - $ProgNameLong

SYNOPSIS
	$ProgName -r FASTA [-g GENOME -p INT -l INT] <File(s)>

DESCRIPTION
	Calls SNPs from query files (fasta format). Done by aligning the query
	files to a reference with "nucmer" then calling SNPs with "show-snps". The
	SNPs are then filtered based on the given options and written in VCF format
	to STDOUT. SNPs are always filtered out of repeat regions.
	NOTE: Only nucleotide sequences.
	MUMMER: version 2.23

OPTIONS
	-r or --reference [fasta file]
	Path to reference file. Must be in fasta format. Can be several sequences
	in one file.

	-g or --genome [genome file]
	Path to ".genome" file. The same type of file as given to genobox. Is not
	needed but if given, the script will check for naming errors in the input.

	-p or --prune [integer]
	SNPs found within the given -p of another mismatch will all be discarded.
	Mismatches includes SNPs, indels, etc. but NOT sequence ends.
	Default: $opt_prune

	-e or --end_distance [integer]
	SNPs found within the given number of basepairs from a sequence end will be
	filtered out.

	-l or --length [integer]
	Ignore SNPs found in query sequences smaller than -l.
	Default: $opt_length

	-h or --help
	Prints this text.

VERSION
	Current: $Version

AUTHOR
	Rolf Sommer Kaas, rkmo\@food.dtu.dk,

EOH
	exit;
}
