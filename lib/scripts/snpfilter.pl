#!/tools/bin/perl

use strict;
use warnings;
use diagnostics;

use Getopt::Long;
use File::Basename;
use File::Spec;
use Cwd;
use File::Spec::Functions qw(rel2abs);
use POSIX qw(floor ceil);
use IO::File;

use Statistics::Descriptive;


#
# Hard coded paths
#

my $opt_eps_sort;
my $opt_eps_ref;
my $opt_eps_large;

my($opt_ignore_ref, $opt_redundant, $opt_help, $opt_depth, $opt_output_dir, $opt_diff);
my $opt_mq = 20;
my $opt_vcf_qual = 20;
my $opt_relative_depth = 20;
my $opt_min_depth = 10;
my $opt_exclude_str;

GetOptions (
	"--ignore_ref"        => \$opt_ignore_ref,		# Do not check if reference is the same.
	"r|redundant"         => \$opt_redundant,		  # Throw out SNPs identical in all given samples.
	"o|output_dir=s"      => \$opt_output_dir,		# output will be named filename.filtered.vcf
	"d|depth=i"           => \$opt_depth,
	"rd|relative_depth=i" => \$opt_relative_depth,
	"md|min_depth=i"      => \$opt_min_depth,
	"mq=i"                => \$opt_mq,
	"q|quality=s"         => \$opt_vcf_qual,
	"ex|exclude=s"        => \$opt_exclude_str,
	"diff"                => \$opt_diff,
	"h|help"              => \$opt_help
) or die("USAGE: $0 --output_dir <directory> (--ignore_ref) (--redundant) (--depth <integer> | --relative_depth <integer>) (--help)\n");

#
# Handle options
#
if($opt_help){
#	&usage();
	print STDERR ("NO help yet!\n");
	exit;
}

# Minimum depht only makes sense if the depth is relatively defined.
if($opt_depth){
	$opt_min_depth = $opt_depth;
}

# If PostScript::Simple is not installed it will be ignored and no postscript
# output will be generated. Only the text file output will be generated.
my $is_ps_installed = 0;

if (eval {require PostScript::Simple;1;} ) {
	$is_ps_installed = 1;
	PostScript::Simple->import();
}
if($is_ps_installed == 0){
	print STDERR ("|! Perl module: \"PostScript::Simple\" not found.\n");
	print STDERR ("|! No postscript file will be generated. Only the text file output is created.\n");
}

$opt_output_dir = rel2abs($opt_output_dir);

$opt_relative_depth = $opt_relative_depth*0.01;

#
# Create hash of ignored SNPs and find all needed files.
#
my %file_hash;
my @sample_files;
my @assembled_files;
my $reference;
my %ignore_SNPs;
my %coverage_means;
my %ref_offsets;
my %ignored_SNPs_pr_isolate;
foreach my $file (@ARGV){

	open(IN, "<$file") or die("Couldn't open file given as argument: $file\n");
	my %SNP_info_files;
	my $key;
	my $sample;
	while(<IN>){
		chomp;
		# Store reference and check it
		if(/# Reference: (.+)/){
			if($reference){
				if($reference ne $1 and not $opt_ignore_ref){
					die("References does not seem to be the same.\nReferences $reference and $1 are not identical.\nIf you are certain that these are the same references, set the --ignore_ref flag.\n");
				}
			}
			else{
				&checkpath($1);
				$reference = $1;
			}

			# Handle multiple reference sequences
			my $offset = 0;
			my($header, $seq);
			open(REF, "<$reference") or die("Couldn't open reference file: $reference\n");
			while(<REF>){
				chomp;
				if(/^>(\S+)/){
					if($seq){
						$ref_offsets{$header} = $offset;
						$offset += length($seq);
						$seq = "";
					}
					$header = $1;
				}
				elsif($_){
					$seq .= $_;
				}
			}
			if($seq){
				$ref_offsets{$header} = $offset;
				$offset += length($seq);
				$seq = "";
			}
			close(REF);
			next;
		}
		# Store read file(s)
		elsif(/# Sample\(s\): ([^,]+)(\, (.+)){0,1}/){
			if($3){
				$SNP_info_files{reads} = "$1, $3";
				&checkpath($1);
				&checkpath($3);
			}
			else{
				&checkpath($1);
				$SNP_info_files{reads} = $1;
			}
			$sample = $1;
			# Key is changed if there is no BAM file (assembled seq)
			$key = $1;
			next;
		}
		# Store BAM file
		elsif(/# Sorted BAM file: (.+)/){
			&checkpath($1);
			$SNP_info_files{bam} = $1;
			next;
		}
		# Store VCF file containing the SNPs which was not filtered out
		elsif(/# VCF file of kept SNPs: (.+)/){
			&checkpath($1);
			$SNP_info_files{vcf} = $1;
			next;
		}
		elsif(/# Coverage file: (.+)/){
			&checkpath($1);
			$SNP_info_files{coverage} = $1;
			next;
		}
		elsif(/# Coverage mean: (.+)/){
			$coverage_means{$key} = $1;
			next;
		}
		# Ignore all other comments
		elsif(/^#/){
			next;
		}

		# Just need to be non-zero
		# The count is not used for anything other than implemented for future potential use.
		my($contig_id, $ignored_pos) = split(/\t/);
		my $corrected_pos = $ignored_pos + $ref_offsets{$contig_id};
		$ignore_SNPs{$corrected_pos}++;
		$ignored_SNPs_pr_isolate{$key}->{$corrected_pos}++;
	}
	close(IN);

	# Handle assembled genomes
	if(not defined($SNP_info_files{bam}) ){
		push(@assembled_files, $SNP_info_files{vcf});
		$key = $SNP_info_files{vcf};
	}
	else{
		push(@sample_files, $sample);
	}
	$file_hash{$key} = \%SNP_info_files;
}


#
# Handle excluded seq option  (line: 282)
#
my %exclude_seqs;
if($opt_exclude_str){
	my @ex_entries = split(/;/, $opt_exclude_str);
	foreach(@ex_entries){
		if(/(.+):(.+)/){
			my $cur_header = $1;
			my $cur_offset = $ref_offsets{$cur_header};
			my @start_n_stops = split(/,/, $2);
			foreach(@start_n_stops){
				if(/(\d+)\-(\d+)/){
					$exclude_seqs{$cur_header}->{ ($1+$cur_offset) } = $2+$cur_offset;
				}
			}
		}
	}
}


#
# Create hash of all kept SNPs
#
my %kept_SNPs;
my %kept_SNPs_pr_isolate;
my %all_SNPs;
my $log_out = "";
foreach my $key ( (@sample_files, @assembled_files) ){
	$log_out .= "$key\t";
	my($filename, $directories, $suffix) = fileparse($key, qr/\.[^.]*/);

	my $avg_mean;
	if($coverage_means{$key}){
		$avg_mean = $coverage_means{$key};
	}

	my $depth_limit;
	if($opt_depth){
		$depth_limit = $opt_depth;
	}
	elsif($avg_mean){
		$depth_limit = $avg_mean*$opt_relative_depth;
		if($depth_limit < $opt_min_depth){
			$depth_limit = $opt_min_depth;
		}
	}
	open(VCF, "<$file_hash{$key}->{vcf}") or die("Couldn't open VCF file: $file_hash{$key}->{vcf}\n");
	my $count_ambigious = 0;
	my $count_depth = 0;
	my $count_mq = 0;
	my $count_qual = 0;
	my $count_both = 0;

	VCF_FILE: while(<VCF>){
		next VCF_FILE if(/^#/);
		my @entries = split(/\t/);
		my $ref_header = $entries[0];
		my $ref_offset = $ref_offsets{$ref_header};
		my $corrected_pos = $entries[1]+$ref_offset;

		my $include_pos = 1;
		if($opt_exclude_str){
			if(defined($exclude_seqs{$ref_header}) or $exclude_seqs{"reference"}){
				my @excl_starts = keys( %{$exclude_seqs{$ref_header}} );
				foreach my $start_pos (@excl_starts){
					if($start_pos <= $corrected_pos){
						my $end_pos = $exclude_seqs{$ref_header}->{$start_pos};
						next VCF_FILE if($end_pos >= $corrected_pos);
					}
				}
			}
		}

		if( (not defined($avg_mean) or $entries[7] =~ /^DP=(\d+);/) and $include_pos){
			my $snp_depth = $1;
			my $snp_mq;
			my $snp_qual;
			if($entries[7] =~ /MQ=(\d+);/){
				$snp_mq = $1;
				$snp_qual = $entries[5];
			}
			# If genome is assembled, doesn't contain raw reads.
			# Keep all SNPs
			if(not defined($avg_mean)){
				my $bp = uc($entries[4]);
				if($bp eq "A"){
					$all_SNPs{$corrected_pos}->{A}++;
					$kept_SNPs{$corrected_pos}->{A}++ unless( defined($ignore_SNPs{$corrected_pos}) );
					$kept_SNPs_pr_isolate{$key}->{$corrected_pos}->{A}++;
				}
				elsif($bp eq "T"){
					$all_SNPs{$corrected_pos}->{T}++;
					$kept_SNPs{$corrected_pos}->{T}++ unless( defined($ignore_SNPs{$corrected_pos}) );
					$kept_SNPs_pr_isolate{$key}->{$corrected_pos}->{T}++;
				}
				elsif($bp eq "C"){
					$all_SNPs{$corrected_pos}->{C}++;
					$kept_SNPs{$corrected_pos}->{C}++ unless( defined($ignore_SNPs{$corrected_pos}) );
					$kept_SNPs_pr_isolate{$key}->{$corrected_pos}->{C}++;
				}
				elsif($bp eq "G"){
					$all_SNPs{$corrected_pos}->{G}++;
					$kept_SNPs{$corrected_pos}->{G}++ unless( defined($ignore_SNPs{$corrected_pos}) );
					$kept_SNPs_pr_isolate{$key}->{$corrected_pos}->{G}++;
				}
				else{
					die("Didn't recognize base in VCF file: $bp\nFile: $file_hash{$key}->{vcf}\n");
				}
			}
			# Sort out SNPs with several different base calls and below chosen depth and MQ thresshold
			elsif($snp_depth >= $depth_limit and $snp_mq >= $opt_mq and $snp_qual >= $opt_vcf_qual){
				my $bp = uc($entries[4]);

				# If several alternative SNP calls exist, choose the most likely one
				if($bp =~ /,/){
					my @alts = split(/,/, $bp);
					my @gt_info = split(/:/, $entries[9]);
					my @gt_scores = split(/,/, $gt_info[1]);

					my $min_score = 1000;
					my $alt_index;

					# Only these three scores matter, since we assume haploid organism.
					# The scores represent alternative 1, 2 and 3 respectively. Found in
					# the ALT column of the VCF file.
					my @possible_indeces = qw(2 5 9);

					foreach(@possible_indeces){
						if ($_ > @gt_scores){
							last;
						}
						if($gt_scores[$_] < $min_score){

							$min_score = $gt_scores[$_];
							if($_ == 2){
								$alt_index = 0;
							}
							elsif($_ == 5){
								$alt_index = 1;
							}
							elsif($_ == 9){
								$alt_index = 2;
							}
						}
					}

					$bp = $alts[$alt_index];
				}

				if($bp eq "A"){
					$all_SNPs{$corrected_pos}->{A}++;
					$kept_SNPs{$corrected_pos}->{A}++ unless( defined($ignore_SNPs{$corrected_pos}) );
					$kept_SNPs_pr_isolate{$key}->{$corrected_pos}->{A}++;
				}
				elsif($bp eq "T"){
					$all_SNPs{$corrected_pos}->{T}++;
					$kept_SNPs{$corrected_pos}->{T}++ unless( defined($ignore_SNPs{$corrected_pos}) );
					$kept_SNPs_pr_isolate{$key}->{$corrected_pos}->{T}++;
				}
				elsif($bp eq "C"){
					$all_SNPs{$corrected_pos}->{C}++;
					$kept_SNPs{$corrected_pos}->{C}++ unless( defined($ignore_SNPs{$corrected_pos}) );
					$kept_SNPs_pr_isolate{$key}->{$corrected_pos}->{C}++;
				}
				elsif($bp eq "G"){
					$all_SNPs{$corrected_pos}->{G}++;
					$kept_SNPs{$corrected_pos}->{G}++ unless( defined($ignore_SNPs{$corrected_pos}) );
					$kept_SNPs_pr_isolate{$key}->{$corrected_pos}->{G}++;
				}
				else{
					die("Didn't recognize base in VCF file: $bp\nFile: $file_hash{$key}->{vcf}\n");
				}
			}
			else{
				if($entries[4] =~ /,/){
					$count_ambigious++;
				}
				if($snp_depth < $depth_limit){
					$count_depth++;
				}
				if($snp_mq < $opt_mq){
					$count_mq++;
				}
				if($snp_qual < $opt_vcf_qual){
					$count_qual++;
				}
				if($entries[4] =~ /,/ and $snp_depth < $depth_limit and $snp_mq >= $opt_mq){
					$count_both++;
				}

				$ignore_SNPs{$corrected_pos} = 1;
				my $pos_deleted = delete( $kept_SNPs{$corrected_pos} );
			}
		}
		else{
			$ignore_SNPs{$corrected_pos} = 1;
			my $pos_deleted = delete( $kept_SNPs{$corrected_pos} );
		}
	}
	close(VCF);

	$log_out .= "$count_qual\t$count_mq\t$count_depth\t$count_ambigious\t$count_both\n";
}

print STDERR ($log_out);

#
# Filter out redundant/identical SNPs
# All SNPs are used in this filter, also the ones that was sorted out previously.
# Although only the SNPs that passed is tested.
#
my $snp_deleted = 0;
if($opt_redundant){
	my $count1 = scalar(@sample_files);
	my $count2 = scalar(@assembled_files);
	my $sample_count = $count1+$count2;
	my $count = 0;
	foreach my $pos (keys(%kept_SNPs)){
		if( defined($all_SNPs{$pos}->{A}) ){
			if($all_SNPs{$pos}->{A} == $sample_count){
				my $pos_deleted = delete( $kept_SNPs{$pos} );
				$count++;
				$snp_deleted = 1;
			}
		}
		elsif( defined($all_SNPs{$pos}->{T}) ){
			if($all_SNPs{$pos}->{T} == $sample_count){
				my $pos_deleted = delete( $kept_SNPs{$pos} );
				$count++;
				$snp_deleted = 1;
			}
		}
		elsif( defined($all_SNPs{$pos}->{C}) ){
			if($all_SNPs{$pos}->{C} == $sample_count){
				my $pos_deleted = delete( $kept_SNPs{$pos} );
				$count++;
				$snp_deleted = 1;
			}
		}
		elsif( defined($all_SNPs{$pos}->{G}) ){
			if($all_SNPs{$pos}->{G} == $sample_count){
				my $pos_deleted = delete( $kept_SNPs{$pos} );
				$count++;
				$snp_deleted = 1;
			}
		}

		$snp_deleted = 0;
	}
	print STDERR ("Removed redundant SNPs: $count\n");
}

#
# Filter out SNPs which are not covered by enough reads in all samples.
#
my $cov_stats_output = "Sample\tMean\tStd dev.\tMedian\tThresshold\tIgnored SNPs\n";
my %matrix;
foreach my $key ( (@sample_files, @assembled_files) ){

	# Store all SNP positions for the current sample
	open(VCF, "<$file_hash{$key}->{vcf}") or die("Couldn't open VCF file: $file_hash{$key}->{vcf}\n");
	my %vcf_entries;
	while(<VCF>){
		chomp;
		next if(/^#/);
		my @entries = split(/\t/);
		my $ref_header = $entries[0];
		my $ref_offset = $ref_offsets{$ref_header};
		my $corrected_pos = $entries[1]+$ref_offset;
		$vcf_entries{$corrected_pos} = 1;
	}
	close(VCF);

	# Store coverage info
	open(COV, "gunzip -c $file_hash{$key}->{coverage} |") or die("Couldn't open gunzip pipe: gunzip -c $file_hash{$key}->{coverage} |\nERROR: $!\n");
	chomp(my @coverage = <COV>);
	close(COV);

	# Create coverage stats for sample
	# These should be ignored if the sample is an assembled genome.
	my $cov_stat = Statistics::Descriptive::Full->new();
	$cov_stat->add_data(@coverage);
	my $cov_std  = $cov_stat->standard_deviation();
	my $cov_median = $cov_stat->median();
	my $cov_mean = $cov_stat->mean();

	# Register if sample is assembled
	my $is_assembled;
	if($coverage_means{$key}){
		$is_assembled = 0;
	}
	else{
		$is_assembled = 1;
	}

	# Create thresshold
	my $thresshold = 0;
	if(not $opt_depth and defined($opt_relative_depth)){
		$thresshold = $cov_mean*$opt_relative_depth;
		$thresshold = $opt_min_depth if($thresshold < $opt_min_depth);
	}
	else{
		$thresshold = $opt_depth
	}

	if($is_assembled){
		$thresshold = 1;
	}

	# Find the SNPs not in the current sample
	my $ignored_SNP_count = 0;
	foreach my $pos (keys(%kept_SNPs)){
		if(not $vcf_entries{$pos}){
			# Check if position is covered by reads in the current sample
			if($coverage[$pos-1] < $thresshold){
				# If not throw out SNP from collection of kept SNPs
				my $pos_deleted = delete( $kept_SNPs{$pos} );
				$ignored_SNPs_pr_isolate{$key}->{$pos}++;
				$ignored_SNP_count++;
			}
		}
	}

	# Stat output to be printed
	my($filename, $directories, $suffix) = fileparse($key, qr/\.[^.]*/);
	$cov_stats_output .= "$filename\t$cov_mean\t$cov_std\t$cov_median\t$thresshold\t$ignored_SNP_count\n";

	#
	# Calculate matrix
	#
	if($opt_diff){
		foreach my $key2 ( (@sample_files, @assembled_files) ){
			next if( defined($matrix{"$key;$key2"}) );
			if($key eq $key2){
				$matrix{"$key;$key2"} = 0;
				next;
			}

			my $snp_count = 0;
			my %shared_snps;

			foreach my $snp_1 ( keys( %{$kept_SNPs_pr_isolate{$key}} ) ){

				# Is position to be ignored in any of the two samples
				if($ignored_SNPs_pr_isolate{$key}->{$snp_1} or $ignored_SNPs_pr_isolate{$key2}->{$snp_1}){
					next;
				}

				# Does snp from isolate 1 exist in isolate 2
				if(not $kept_SNPs_pr_isolate{$key2}->{$snp_1}){
					$snp_count++;
					next;
				}

				# SNP position exist in both isolates - Doesn't have to be SAME SNP!!!
				$shared_snps{$snp_1} = 1;

				# Check if the SNP is identical
				my $bp_1;
				if($kept_SNPs_pr_isolate{$key}->{$snp_1}->{G}){
					$bp_1 = "G";
				}
				elsif($kept_SNPs_pr_isolate{$key}->{$snp_1}->{C}){
					$bp_1 = "C";
				}
				elsif($kept_SNPs_pr_isolate{$key}->{$snp_1}->{A}){
					$bp_1 = "A";
				}
				elsif($kept_SNPs_pr_isolate{$key}->{$snp_1}->{T}){
					$bp_1 = "T";
				}

				my $bp_2;
				if($kept_SNPs_pr_isolate{$key2}->{$snp_1}->{G}){
					$bp_2 = "G";
				}
				elsif($kept_SNPs_pr_isolate{$key2}->{$snp_1}->{C}){
					$bp_2 = "C";
				}
				elsif($kept_SNPs_pr_isolate{$key2}->{$snp_1}->{A}){
					$bp_2 = "A";
				}
				elsif($kept_SNPs_pr_isolate{$key2}->{$snp_1}->{T}){
					$bp_2 = "T";
				}

				if($bp_1 ne $bp_2){
					$snp_count++;
				}
			}

			foreach my $snp_2 ( keys( %{$kept_SNPs_pr_isolate{$key2}} ) ){

				# Is position to be ignored in any of the two samples
				if($ignored_SNPs_pr_isolate{$key}->{$snp_2} or $ignored_SNPs_pr_isolate{$key2}->{$snp_2}){
					next;
				}

				# If SNP has already been processed
				if($shared_snps{$snp_2}){
					next;
				}

				# All common SNPs is taken care of in previous for-loop.
				$snp_count++;

				# Could implement check on position depth in isolate 1 at this SNP
				# This would require removal of the code that skips comparison of 2vs1
				# if 1vs2 has already been done.
			}

			$matrix{"$key;$key2"} = $snp_count;
			$matrix{"$key2;$key"} = $snp_count;
		}
	}
}

#
# Calculate matrix for assembled samples
#
if($opt_diff){
	foreach my $key (@assembled_files){
		foreach my $key2 (@assembled_files){
			next if( defined($matrix{"$key;$key2"}) );
			if($key eq $key2){
				$matrix{"$key;$key2"} = 0;
				next;
			}

			my $snp_count = 0;
			my %shared_snps;

			foreach my $snp_1 ( keys( %{$kept_SNPs_pr_isolate{$key}} ) ){

				# Does snp from isolate 1 exist in isolate 2
				if(not $kept_SNPs_pr_isolate{$key2}->{$snp_1}){
					$snp_count++;
					next;
				}

				# SNP position exist in both isolates - Doesn't have to be SAME SNP!!!
				$shared_snps{$snp_1} = 1;

				# Check if the SNP is identical
				my $bp_1;
				if($kept_SNPs_pr_isolate{$key}->{$snp_1}->{G}){
					$bp_1 = "G";
				}
				elsif($kept_SNPs_pr_isolate{$key}->{$snp_1}->{C}){
					$bp_1 = "C";
				}
				elsif($kept_SNPs_pr_isolate{$key}->{$snp_1}->{A}){
					$bp_1 = "A";
				}
				elsif($kept_SNPs_pr_isolate{$key}->{$snp_1}->{T}){
					$bp_1 = "T";
				}

				my $bp_2;
				if($kept_SNPs_pr_isolate{$key2}->{$snp_1}->{G}){
				$bp_2 = "G";
				}
				elsif($kept_SNPs_pr_isolate{$key2}->{$snp_1}->{C}){
					$bp_2 = "C";
				}
				elsif($kept_SNPs_pr_isolate{$key2}->{$snp_1}->{A}){
					$bp_2 = "A";
				}
				elsif($kept_SNPs_pr_isolate{$key2}->{$snp_1}->{T}){
					$bp_2 = "T";
				}

				if($bp_1 ne $bp_2){
					$snp_count++;
				}
			}

			foreach my $snp_2 ( keys( %{$kept_SNPs_pr_isolate{$key2}} ) ){

				# If SNP has already been processed
				if($shared_snps{$snp_2}){
					next;
				}

				# All common SNPs is taken care of in previous for-loop.
				$snp_count++;
			}

			$matrix{"$key;$key2"} = $snp_count;
			$matrix{"$key2;$key"} = $snp_count;
		}
	}

	#
	# Print matrix output
	#
	my $value_max = 0;
	my $value_min = 100000;

	open(TXT_OUT, ">$opt_output_dir/pairwise_snp_matrix.txt") or die("couldn't write to $opt_output_dir/pairwise_snp_matrix.txt\n");
	foreach ( (@sample_files, @assembled_files) ){
		my($filename, $directories, $suffix) = fileparse($_, qr/\.[^.]*/);
		print TXT_OUT ("\t$filename");
	}

	print TXT_OUT ("\n");
	foreach my $key ( (@sample_files, @assembled_files) ){
		my($filename, $directories, $suffix) = fileparse($key, qr/\.[^.]*/);
		print TXT_OUT ("$filename\t");
		foreach my $key2 ( (@sample_files, @assembled_files) ){
			my($filename2, $directories2, $suffix2) = fileparse($key2, qr/\.[^.]*/);
			my $value = $matrix{"$key;$key2"};
			print TXT_OUT ("$value\t");

			if($value > $value_max){
				$value_max = $value;
			}
			if($value < $value_min){
				$value_min = $value;
			}
		}
		print TXT_OUT ("\n");
	}
	print TXT_OUT ("min: $value_min max: $value_max\n");
	close(TXT_OUT);

	if($is_ps_installed){
		&createPS("$opt_output_dir/snp_matrix.txt");
	}
}

# Print stats to file
open(STATS, ">$opt_output_dir/coverage_stats.txt") or die("Couldn't write to stats file: $opt_output_dir/coverage_stats.txt\n");
print STATS ($cov_stats_output);
close(STATS);

#
# Create new filtered VCF files
#
print STDERR ("Assembled files: @assembled_files\n");
foreach my $key ( (@sample_files, @assembled_files) ){

	open(VCF, "<$file_hash{$key}->{vcf}") or die("Couldn't open VCF file: $file_hash{$key}->{vcf}\n");
	my $filtered_vcf_output = "";
	while(<VCF>){
		chomp;
		if(/^#/){
			if(/#CHROM\s+POS\s+ID\s+REF\s+ALT\s+QUAL\s+FILTER\s+INFO\s+FORMAT\s+(\S+)/){
				my($filename, $directories, $suffix) = fileparse($1, qr/\.[^.]*/);
				$filtered_vcf_output .= ("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT\t$filename\n");
			}
			else{
				$filtered_vcf_output .= "$_\n";
			}
			next;
		}
		my @entries = split(/\t/);
		my $ref_header = $entries[0];
		my $ref_offset = $ref_offsets{$ref_header};
		my $corrected_pos = $entries[1]+$ref_offset;

		# print only entries to VCF file, which exists in kept_SNPs hash
		if( defined($kept_SNPs{$corrected_pos}) && not defined($ignore_SNPs{$corrected_pos})){
			$filtered_vcf_output .= "$_\n";
		}
	}
	close(VCF);

	# Print filtered VCF file
	my($filename, $directories, $suffix) = fileparse($key, qr/\.[^.]*/);
	open(OUT, ">$opt_output_dir/$filename.filtered.vcf") or die("Couldn't write to filtered VCF file: $opt_output_dir/$filename.filtered.vcf");
	print OUT ($filtered_vcf_output);
	close(OUT);
}


exit;


#
# Functions
#

sub matrixSort {
	my($arg_matrix, $ref_index, $ref) = @_;
	my %matrix = %{$arg_matrix};

	my $a_entry = $matrix{"$ref-$a"};
	my $b_entry = $matrix{"$ref-$b"};

	my($a_val,$a_total,$a_percentage) = split(/,/, $a_entry);
	my($b_val,$b_total,$b_percentage) = split(/,/, $b_entry);

	if($a_val > $b_val){
		return -1;}
	elsif($a_val == $b_val){
		return 0;}
	else{
		return 1;}
}


sub createPS {
	print ("|  Creating PostScript file...\n");

	my $txt_matrix = shift;

	# Reads matrix from text file.
	open(TXT, "<$txt_matrix") or die("Couldn't open $txt_matrix\n");
	my $first_line = <TXT>;
	chomp($first_line);
	my @query_genomes = split(/\t/, $first_line);
	# First entry is empty
	shift(@query_genomes);
	my @subject_genomes;
	my %blast_matrix;
	my $max_name_length = 0;
	my ($percentage_max,$percentage_min);
	my $percentage_hom_max = 100;			# dummy values. NOT implemented!
	my $percentage_hom_min = 0;				# dummy values. NOT implemented!
	my @rgb_col = ();
	my @rgb_col_int = ();
	my $counter_row = 0;

	while(<TXT>){
		chomp;
		# Retrieves the min and max values for the pecentages
		if(/^min: (.+) max: (.+)$/){
			$percentage_min = $1;
			$percentage_max = $2;
		}
		elsif(/^homologs min: (.+) homologs max: (.+)$/){
			$percentage_hom_min = $1;
			$percentage_hom_max = $2;
		}
		# Retrieves the custom colors to use if specified.
		# If not found default colors are used.
		elsif(/^R\:(\d+)\,G\:(\d+)\,B\:(\d+)/){
			$rgb_col[0] = $1;
			$rgb_col[1] = $2;
			$rgb_col[2] = $3;
		}
		# Internal R:0,G:0,B:255
		elsif(/^Internal R\:(\d+)\,G\:(\d+)\,B\:(\d+)/){
			$rgb_col_int[0] = $1;
			$rgb_col_int[1] = $2;
			$rgb_col_int[2] = $3;
		}
		else{
			$counter_row++;
			my @row = split(/\t/);
			my $subject = shift(@row);

			for(my $i=0; $i<@row; $i++){
				my $row_entry = $row[$i];
				my $query = $query_genomes[$i];
				$blast_matrix{"$query;$subject"} = $row_entry;
			}

			push(@subject_genomes, $subject);

			# Max name length is used to determine width of output.
			if(length($subject) > $max_name_length){
				$max_name_length = length($subject);}
		}
	}
	close(TXT);

	if($opt_eps_sort){
		print ("|  -Soring to reference: $query_genomes[$opt_eps_ref-1]\n");
		@subject_genomes = sort{ &matrixSort(\%blast_matrix, $opt_eps_ref, $query_genomes[$opt_eps_ref-1]) } @subject_genomes;
	}

	# Setting values

	my $FONT_SIZE = 12;
	my $FONT = "Times-Roman";
	my @LEFT_RIGHT_MARGIN = (10,10);
	my @BOTTOM_TOP_MARGIN = (10,10);
	my $BOX_SIZE = 20;

	# Color of matrix boxes
	# Lowest percentage color (always grey)
	my ($RED_L, $GREEN_L, $BLUE_L) = (240, 240, 240);
	# Highest percentage color
	if(@rgb_col){
		print ("|  -Using custom colors for homology between proteomes RGB:@rgb_col\n");
	}
	# Setting default if not defined.
	else{
		$rgb_col[0] = 128;
		$rgb_col[1] = 0;
		$rgb_col[2] = 64;
	}
	my ($RED_H, $GREEN_H, $BLUE_H) = @rgb_col;
	my ($red_diff, $green_diff, $blue_diff) = ($RED_H-$RED_L, $GREEN_H-$GREEN_L, $BLUE_H-$BLUE_L);
	my $percentage_scale = $percentage_max-$percentage_min;

	# Color of homolog matrix boxes
	# Lowest homolog percentage color (always grey)
	my ($RED_HOM_L, $GREEN_HOM_L, $BLUE_HOM_L) = (240, 240, 240);
	# Highest percentage color
	if(@rgb_col_int){
		print ("|  -Using custom colors for homology within proteomes RGB:@rgb_col_int\n");
	}
	# Setting default if not defined.
	else{
		$rgb_col_int[0] = 0;
		$rgb_col_int[1] = 64;
		$rgb_col_int[2] = 128;
	}
	my ($RED_HOM_H, $GREEN_HOM_H, $BLUE_HOM_H) = @rgb_col_int;
	my ($red_hom_diff, $green_hom_diff, $blue_hom_diff) = ($RED_HOM_H-$RED_HOM_L, $GREEN_HOM_H-$GREEN_HOM_L, $BLUE_HOM_H-$BLUE_HOM_L);
	my $percentage_hom_scale = $percentage_hom_max-$percentage_hom_min;

	my $LEGEND_WIDTH = 200;
	my $LEGEND_HEIGHT = 10;
	my $LEGEND_STEPS = 20;
	# 0.3528 mm is the size of 1 pt (font point).
	my $legend_text_width = 0.3528*($FONT_SIZE*2)*3; # The potential three percentage positions.

	# Size of output is determined by max name length.
	my $name_width = $FONT_SIZE*0.3528*$max_name_length;
	# sin(45deg) ~ 0.7071
	my $name_height = $name_width*0.7071;

	my $pic_width = scalar(@query_genomes)*$BOX_SIZE+$LEFT_RIGHT_MARGIN[0]+$LEFT_RIGHT_MARGIN[1]+$name_width+$LEGEND_WIDTH+$legend_text_width;
	my $pic_height = scalar(@query_genomes)*$BOX_SIZE+$BOTTOM_TOP_MARGIN[0]+$BOTTOM_TOP_MARGIN[1]+$name_height;
	print STDERR ("|\tSize (mm): $pic_height X $pic_width\n");

	my $ps_pic = new PostScript::Simple(	units => "mm",
											colour => 1,
											eps => 1,
											xsize => $pic_width,
											ysize => $pic_height,
											reencode => "ISOLatin1Encoding");


	# Draws legend "Homology between proteins"

	my $legend_ofset_x = $pic_width-$LEFT_RIGHT_MARGIN[1]-$LEGEND_WIDTH-$legend_text_width;
	my $legend_ofset_y = $pic_height-$BOTTOM_TOP_MARGIN[1]-$LEGEND_HEIGHT-10;
	my $legend_step_width = $LEGEND_WIDTH/$LEGEND_STEPS;

	my $red_step = $red_diff/($LEGEND_STEPS-1);
	my $green_step = $green_diff/($LEGEND_STEPS-1);
	my $blue_step = $blue_diff/($LEGEND_STEPS-1);

	# Legend gradiant bar
	for(my $i=0; $i<$LEGEND_STEPS; $i++){
		$ps_pic->setcolour($RED_L+$red_step*$i, $GREEN_L+$green_step*$i, $BLUE_L+$blue_step*$i);
		my $legend_box_lower_left_x = $legend_ofset_x+$legend_step_width*$i;
		my $legend_box_lower_left_y = $legend_ofset_y;
		my $legend_box_upper_right_x = $legend_ofset_x+$legend_step_width*$i+$legend_step_width;
		my $legend_box_upper_right_y = $legend_ofset_y+$LEGEND_HEIGHT;
		$ps_pic->box({filled => 1}, $legend_box_lower_left_x,$legend_box_lower_left_y, $legend_box_upper_right_x,$legend_box_upper_right_y);
	}

	# Legend title
	$ps_pic->setfont($FONT, $FONT_SIZE*2);
	$ps_pic->setcolour("black");
	my $leg_name_x = $legend_ofset_x+($LEGEND_STEPS*$legend_step_width)/2;
	my $leg_name_y = $legend_ofset_y+$LEGEND_HEIGHT+5;
	my $leg_name = "SNP counts between genomes";
	$ps_pic->text({align => 'centre'}, $leg_name_x,$leg_name_y, $leg_name);

	# Legend min max text
	my $min_leg_x = $legend_ofset_x;
	my $min_leg_y = $legend_ofset_y-6;
	my $min_name = floor($percentage_min);
	$ps_pic->text({align => 'centre'}, $min_leg_x,$min_leg_y, $min_name);
	my $max_leg_x = $legend_ofset_x+$LEGEND_STEPS*$legend_step_width;
	my $max_leg_y = $legend_ofset_y-6;
	my $max_name = ceil($percentage_max);
	$ps_pic->text({align => 'centre'}, $max_leg_x,$max_leg_y, $max_name);


	# Draws legend: Homology within proteomes

	my $legend_hom_ofset_x = $legend_ofset_x;
	my $legend_hom_ofset_y = $legend_ofset_y-40;

	my $red_hom_step = $red_hom_diff/($LEGEND_STEPS-1);
	my $green_hom_step = $green_hom_diff/($LEGEND_STEPS-1);
	my $blue_hom_step = $blue_hom_diff/($LEGEND_STEPS-1);

	# Legend gradiant bar
	for(my $i=0; $i<$LEGEND_STEPS; $i++){
		$ps_pic->setcolour($RED_HOM_L+$red_hom_step*$i, $GREEN_HOM_L+$green_hom_step*$i, $BLUE_HOM_L+$blue_hom_step*$i);
		my $legend_box_lower_left_x = $legend_hom_ofset_x+$legend_step_width*$i;
		my $legend_box_lower_left_y = $legend_hom_ofset_y;
		my $legend_box_upper_right_x = $legend_hom_ofset_x+$legend_step_width*$i+$legend_step_width;
		my $legend_box_upper_right_y = $legend_hom_ofset_y+$LEGEND_HEIGHT;
		$ps_pic->box({filled => 1}, $legend_box_lower_left_x,$legend_box_lower_left_y, $legend_box_upper_right_x,$legend_box_upper_right_y);
	}

	# Legend title
	$ps_pic->setfont($FONT, $FONT_SIZE*2);
	$ps_pic->setcolour("black");
	my $leg_hom_name_x = $legend_hom_ofset_x+($LEGEND_STEPS*$legend_step_width)/2;
	my $leg_hom_name_y = $legend_hom_ofset_y+$LEGEND_HEIGHT+5;
	my $leg_hom_name = "Not yet implemented";
	$ps_pic->text({align => 'centre'}, $leg_hom_name_x,$leg_hom_name_y, $leg_hom_name);

	# Legend min max text
	my $min_hom_leg_x = $legend_hom_ofset_x;
	my $min_hom_leg_y = $legend_hom_ofset_y-6;
	my $min_hom_name = floor($percentage_hom_min);
	$ps_pic->text({align => 'centre'}, $min_hom_leg_x,$min_hom_leg_y, $min_hom_name);
	my $max_hom_leg_x = $legend_hom_ofset_x+$LEGEND_STEPS*$legend_step_width;
	my $max_hom_leg_y = $legend_hom_ofset_y-6;
	my $max_hom_name = ceil($percentage_hom_max);
	$ps_pic->text({align => 'centre'}, $max_hom_leg_x,$max_hom_leg_y, $max_hom_name);


	# Draws matrix

	my $box_ofset_x = $LEFT_RIGHT_MARGIN[0];
	my $box_ofset_y = $BOTTOM_TOP_MARGIN[0];

	if($opt_eps_large){
		open(NAMES, ">$opt_output_dir/matrix.names.txt") or die("Couldn't create file: $opt_output_dir/matrix.names.txt\n");}

	# Create matrix row by row (bottom -> up)
	for(my $y=0; $y<@subject_genomes; $y++){
		if($opt_eps_large){
			print NAMES (($y+1).":\t$subject_genomes[$y]\n");}

		my @row;
		foreach my $genome (@subject_genomes){
			my $entry = $blast_matrix{"$subject_genomes[$y];$genome"};
			push(@row, $entry);
		}

		for(my $x=0; $x<@row; $x++){

			# Create box for each entry
			my $color_scale;
			my $value = $row[$x];

			if($x == $y){
				unless($percentage_hom_scale == 0){
					$color_scale = ($value-$percentage_hom_min)/$percentage_hom_scale;}
				else{
					$color_scale = 0;}
				if($color_scale > 1){
					$color_scale = 1;}
				$ps_pic->setcolour($RED_HOM_L+$red_hom_diff*$color_scale, $GREEN_HOM_L+$green_hom_diff*$color_scale, $BLUE_HOM_L+$blue_hom_diff*$color_scale);
			}
			else{
				unless($percentage_scale == 0){
					$color_scale = ($value-$percentage_min)/$percentage_scale;}
				else{
					$color_scale = 0;}
				if($color_scale > 1){
					$color_scale = 1;}
				$ps_pic->setcolour($RED_L+$red_diff*$color_scale, $GREEN_L+$green_diff*$color_scale, $BLUE_L+$blue_diff*$color_scale);
			}

			# Draws box
			my $lower_left_corner_x = $box_ofset_x+$BOX_SIZE*$x;
			my $lower_left_corner_y = $box_ofset_y+$BOX_SIZE*$y;
			my $upper_right_corner_x = $box_ofset_x+$BOX_SIZE*$x+$BOX_SIZE;
			my $upper_right_corner_y = $box_ofset_y+$BOX_SIZE*$y+$BOX_SIZE;
			$ps_pic->box({filled => 1}, $lower_left_corner_x,$lower_left_corner_y, $upper_right_corner_x,$upper_right_corner_y);

			# Draw text inside box
			$ps_pic->setcolour("black");

			unless($opt_eps_large){
				$ps_pic->setfont($FONT, $FONT_SIZE);
				my $percentage_x = $upper_right_corner_x-($BOX_SIZE/2);
				my $percentage_y = $upper_right_corner_y-$BOX_SIZE*0.375;
				$ps_pic->text({align => 'centre'}, $percentage_x,$percentage_y, "$value");
				$ps_pic->setfont($FONT, $FONT_SIZE*0.75);
			}

			# After last box in row, write genome name.
			if($x == $#query_genomes){
				$ps_pic->setfont($FONT, $FONT_SIZE);
				my $name_x = $upper_right_corner_x+5;
				my $name_y = $upper_right_corner_y-($BOX_SIZE/2);

				my $name;
				unless($opt_eps_large){
					$name = $subject_genomes[$y];}
				else{
					$name = ($y+1);}
				$ps_pic->text($name_x,$name_y, $name);
			}

			# At the top of each column, write genome name.
			if($y == $#subject_genomes){
				$ps_pic->setfont($FONT, $FONT_SIZE);
				my $name_x = $upper_right_corner_x-($BOX_SIZE/2);
				my $name_y = $upper_right_corner_y+5;

				my $name;
				unless($opt_eps_large){
					$name = $subject_genomes[$x];
					$ps_pic->text({rotate => 45}, $name_x,$name_y, $name);
				}
				else{
					$name = ($x+1);
					$ps_pic->text($name_x,$name_y, $name);
				}
			}
		}
	}

	if($opt_eps_large){
		close(NAMES);}

	$ps_pic->output($opt_output_dir."/snp_matrix.eps");
	print("|\tWrote: $opt_output_dir/snp_matrix.eps\n");
}

sub checkpath {
	my $path = shift;
	unless( -s $path ){
		die("$path path not found!\n");
	}
	return 0;
}


sub usage {
	while (<DATA>) {
		print $_;
	}
	close DATA;
 exit 1;
}
