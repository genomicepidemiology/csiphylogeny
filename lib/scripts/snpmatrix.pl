#! /tools/bin/perl

use strict;
use warnings;
use diagnostics;
use Getopt::Long;


my $opt_input = "";
my $opt_output = "snp_matrix";
my $opt_help;

GetOptions(
	"i|input=s"       => \$opt_input,		  # Fasta aln
	"o|output=s"      => \$opt_output,		# Output txt file
	"h|help"          => \$opt_help			  # Prints help text and exits
);


if ($opt_help) {
	print("snpmatrix.pl -i FASTA_ALN -o OUTPUT\n");
	exit;
}


my @input_file;
if($opt_input){
	open(IN, "<$opt_input") or die("Couldn't open input file: $opt_input\n");
	chomp(@input_file = <IN>);
	close(IN);
}
else{
	die("You must specify an input file!\n");
}

my %alignment;
my @headers;
my $aln_size;
my $seq = "";
my $header;
foreach(@input_file){
	next unless $_;

	if(/>(.+)/){
		if($seq){
			$seq = uc($seq);
			my @seq_array = split(//, $seq);
			$alignment{$header} = \@seq_array;
			push(@headers, $header);
			$seq = "";
		}
		$header = $1;
	}
	else{
		$seq .= $_;
	}
}
if($seq){
	$seq = uc($seq);
	my @seq_array = split(//, $seq);
	$alignment{$header} = \@seq_array;
	push(@headers, $header);
	$aln_size = scalar(@seq_array);
	$seq = "";
}

my %output;
# Initialize output
foreach my $n (@headers){
	foreach my $m (@headers){
		$output{"$n;$m"} = 0;
	}
}


for(my $i=0; $i<$aln_size; $i++){
	foreach my $n (@headers){
		foreach my $m (@headers){
			next if( $n eq $m );
			if( ${$alignment{$n}}[$i] ne ${$alignment{$m}}[$i] ){
				$output{"$n;$m"} += 1;
			}
		}
	}
}

# Create text output

my $value_max = 0;
my $value_min = 100000;

open(TXT_OUT, ">$opt_output") or die("couldn't write to $opt_output\n");
foreach (@headers){
	print TXT_OUT ("\t$_");
}

print TXT_OUT ("\n");
for(my $n=0; $n<@headers; $n++){
	my $n_name = $headers[$n];
	print TXT_OUT ("$n_name\t");
	for(my $m=0; $m<@headers; $m++){
		my $m_name = $headers[$m];
		if($n_name eq $m_name){
			print TXT_OUT ("0\t");
		}
		else{
			my $value = $output{"$n_name;$m_name"};
			print TXT_OUT ("$value\t");

			if($value > $value_max){
				$value_max = $value;}
			if($value < $value_min){
				$value_min = $value;}
		}
	}
	print TXT_OUT ("\n");
}
print TXT_OUT ("min: $value_min max: $value_max\n");

close(TXT_OUT);

exit;
