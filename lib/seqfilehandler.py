#! /tools/bin/python3

from __future__ import print_function
from difflib import ndiff
import argparse
import os
import os.path
import gzip
import subprocess
import re
import sys
import hashlib


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


class SeqFormatError(Exception):
    """ Raise when neither FASTQ or FASTA is matched.
        Raised in: SeqFile.parse_files
    """
    def __init__(self, message, *args):
        self.message = message
        # allow users initialize misc. arguments as any other builtin Error
        super(SeqFormatError, self).__init__(message, *args)


class SeqFile():
    '''
    '''

    def __init__(self, seqfile, pe_file_reverse=None, phred_format=None):
        ''' Constructor.
        '''
        self.phred = phred_format

        seqfile = SeqFile.get_abs_path_and_check(seqfile)

        self.path = seqfile
        self.pe_file_reverse = None

        self.filename = SeqFile.get_read_filename(seqfile)
        self.filename_reverse = None

        self.trim_path = None
        self.trim_pe_file_reverse = None

        self.gzipped = SeqFile.is_gzipped(seqfile)

        if(pe_file_reverse):
            self.seq_format = "paired"
            if(self.phred is None):
                eprint("Warning: Phred format set to None. Set the phred format"
                       " or set it to 'unknown' to get rid of this warning.")
                self.phred = "unknown"

            # Add path to reverse pair file
            self.path_reverse = SeqFile.get_abs_path_and_check(pe_file_reverse)
            # pe_file_reverse is only here for legacy reasons
            self.pe_file_reverse = self.path_reverse

            self.filename_reverse = SeqFile.get_read_filename(
                self.pe_file_reverse)

            # Check if pair is gzipped
            if(self.gzipped != SeqFile.is_gzipped(pe_file_reverse)):
                print("ERROR: It seems that only one of the read pair files is\
                      gzipped.")
                quit(1)
        elif(phred_format):
            self.seq_format = "single"
        else:
            self.seq_format = "assembly"

    @staticmethod
    def get_abs_path_and_check(path, allow_exit=False):
        abs_path = os.path.abspath(path)
        if(not os.path.isfile(abs_path)):
            if(allow_exit):
                sys.exit("ERROR: Path not found: {}".format(path))
            else:
                raise FileNotFoundError("Sequence file not found: {}"
                                        .format(path))
        return abs_path

    def set_trim_files(self, path, path_reverse=None):
        self.trim_path = path
        self.trim_filename = SeqFile.get_read_filename(path)
        if(path_reverse):
            self.trim_pe_file_reverse = path_reverse
            self.trim_filename_reverse = SeqFile.get_read_filename(
                path_reverse)

    @staticmethod
    def get_read_filename(seq_path):
        ''' Removes path from given string and removes extensions:
            .fq .fastq .txt .gz and .trim
        '''
        seq_path = os.path.basename(seq_path)
        seq_path = seq_path.replace(".fq", "")
        seq_path = seq_path.replace(".fastq", "")
        seq_path = seq_path.replace(".txt", "")
        seq_path = seq_path.replace(".gz", "")
        seq_path = seq_path.replace(".trim", "")
        seq_path = seq_path.replace(".fasta", "")
        seq_path = seq_path.replace(".fna", "")
        seq_path = seq_path.replace(".fsa", "")
        seq_path = seq_path.replace(".fa", "")
        return seq_path.rstrip()

    @staticmethod
    def is_gzipped(file_path):
        ''' Returns True if file is gzipped and False otherwise.

            The result is inferred from the first two bits in the file read
            from the input path.
            On unix systems this should be: 1f 8b
            Theoretically there could be exceptions to this test but it is
            unlikely and impossible if the input files are otherwise expected
            to be encoded in utf-8.
        '''
        with open(file_path, mode='rb') as fh:
            bit_start = fh.read(2)
        if(bit_start == b'\x1f\x8b'):
            return True
        else:
            return False

    @staticmethod
    def predict_pair(file_path):
        ''' Based on filename, tries to predict pair no by looking for
            the regex patterns:
                "_R([1|2])[_|.]"            regular file
                "^[E|D|S]RR.+_([1|2])\."    file from ENA or SRA

            Method returns 0 if predicted to be single end.
        '''
        re_filename = re.compile(r"_R([1|2])[_|.]")
        re_seqdb_filename = re.compile(r"^[E|D|S]RR.+_([1|2])\.")

        filename = os.path.basename(file_path)
        match_filename = re_filename.search(filename)
        match_seqdb = re_seqdb_filename.search(filename)

        if(match_filename):
            return int(match_filename.group(1))
        elif(match_seqdb):
            return int(match_seqdb.group(1))
        else:
            return 0

    @staticmethod
    def group_fastqs(file_paths):
        '''
        '''

        re_filename = re.compile(r"(.+)_S\d+_L(\d+)(_.+)")
        re_seqdb_filename = re.compile(r"([E|D|S]RR\d+)(_\d+?\..+)")
        file_groups = {}

        for path in file_paths:

            filename = os.path.basename(path)
            match_filename = re_filename.search(filename)
            match_seqdb = re_seqdb_filename.search(filename)

            if(match_filename):
                name = match_filename.group(1)
                lane_no = match_filename.group(2)
                pair_id = match_filename.group(3)
                lane_no = int(lane_no)
                lane_nos = file_groups.get((name, pair_id), {})
                lane_nos[lane_no] = path
                file_groups[(name, pair_id)] = lane_nos
            elif(match_seqdb):
                # Assume that no lane number exist and are just provided an
                # artificial lane number.
                name = match_seqdb.group(1)
                lane_no = 1
                pair_id = match_seqdb.group(2)
                lane_nos = file_groups.get((name, pair_id), {})
                lane_nos[lane_no] = path
                file_groups[(name, pair_id)] = lane_nos
            else:
                eprint("Warning: Did not recognise filename: " + filename)

        return file_groups

    @staticmethod
    def concat_fastqs(file_paths, out_path=".", verbose=False):
        '''
        '''

        out_list = []
        file_groups = SeqFile.group_fastqs(file_paths)

        for (name, pair_id), lane_nos in file_groups.items():

            out_filename = SeqFile.get_read_filename(name + pair_id) + ".fq"
            sorted_lanes = sorted(lane_nos.keys())

            for lane_no in sorted_lanes:
                path = lane_nos[lane_no]
                if(SeqFile.is_gzipped(path)):
                    cmd = "gunzip -c "
                else:
                    cmd = "cat "
                cmd += path + " >> " + out_path + "/" + out_filename
                subprocess.run(cmd, shell=True)
            subprocess.run("gzip " + out_path + "/" + out_filename, shell=True)
            out_list.append(out_path + "/" + out_filename + ".gz")
            if(verbose):
                eprint("Wrote: " + out_path + "/" + out_filename + ".gz")

        return out_list

    @classmethod
    def parse_files(cls, file_paths, phred=None, headers2count=10, min_match=2,
                    force_neighbour=False, ignore_errors=False):
        '''
        '''
        re_win_newline = re.compile(r"\r\n")
        re_mac_newline = re.compile(r"\r")

        re_fastq_header = re.compile(r"(@.+)")

        re_sra = re.compile(r"^(@SRR\d+\.\d+)")
        re_ena = re.compile(r"^(@ERR\d+\.\d+)")
        re_ena2 = re.compile(r"^(@DRR\d+\.\d+)")

        re_length = re.compile(r"length=\d+")
        re_pair_no = re.compile(r"1|2")

        paired = {}
        single = {}
        assemblies = {}
        old_read_headers = {}
        prev_read_headers = {}
        old_org_headers = {}
        prev_org_headers = {}

        for path in file_paths:
            head_count = 0
            is_fastq = False
            is_fasta = False
            file_headers = []

            if(cls.is_gzipped(path)):
                fh = gzip.open(path, "rt", encoding="utf-8")
            else:
                fh = open(path, "r", encoding="utf-8")

            # Try reading a line to catch possible exceptions
            try:
                # Can throw UnicodeDecodeError
                line = fh.readline()
                line = re_win_newline.sub("\n", line)
                line = re_mac_newline.sub("\n", line)
                line = line.rstrip()
                # Can throw IndexError
                line_1s_letter = line[0]
                fh.seek(0, 0)
            except UnicodeDecodeError as e:
                if(ignore_errors):
                    eprint("ERROR: UnicodeDecodeError for {}".format(path))
                    continue
                else:
                    raise(e)
            except IndexError as e:
                if ignore_errors:
                    eprint("ERROR: IndexError for {}".format(path))
                    continue
                else:
                    raise(e)

            org_headers = {}

            for line in fh:
                line = re_win_newline.sub("\n", line)
                line = re_mac_newline.sub("\n", line)
                line = line.rstrip()

                if(not line):
                    continue

                line_1s_letter = line[0]

                # FASTA format
                if(line_1s_letter == ">"):
                    is_fasta = True
                # FASTQ format
                elif(line_1s_letter == "@"):
                    # match_fastq_header = re_fastq_header.search(line)
                    # fastq_header = match_fastq_header.group(1)
                    match_sra = re_sra.search(line)
                    match_ena = re_ena.search(line)
                    match_ena2 = re_ena2.search(line)

                    is_fastq = True
                    head_count += 1

                    # If data is obtained from SRA
                    if(match_sra):
                        header = match_sra.group(1)
                        # Remove length=int
                        line = re_length.sub("", line)
                        org_headers[header] = line
                    # If data is obtained from ENA
                    elif(match_ena):
                        header = match_ena.group(1)
                        # Remove length=int
                        line = re_length.sub("", line)
                        org_headers[header] = line
                    elif(match_ena2):
                        header = match_ena2.group(1)
                        # Remove length=int
                        line = re_length.sub("", line)
                        org_headers[header] = line
                    else:
                        # Masking 1s and 2s so that pairs will match
                        header = re_pair_no.sub("x", line)
                        org_headers[header] = line

                    file_headers.append(header)
                    if(head_count == headers2count):
                        break

            fh.close()

            if(is_fastq):
                if(not phred):
                    # TODO: Implement find phred function
                    phred = "unknown"
                    pass

                matches = 0
                read_file1 = ""
                read_file2 = ""
                # Check for mates in "neighbors"
                for header in file_headers:
                    if(header in prev_read_headers):

                        pair_no = cls.detect_pair_no(
                            org_headers[header], prev_org_headers[header])

                        if(pair_no is None):
                            # Correct pair numbering cannot be obtained
                            # from SRA headers and some ENA headers
                            # Attempt to get pair number from filenames
                            filename1 = cls.get_read_filename(path)
                            filename2 = cls.get_read_filename(
                                prev_read_headers[header])
                            pair_no = cls.detect_pair_no(filename1, filename2)

                        if(pair_no is not None):
                            matches += 1
                            if(pair_no == 2):
                                read_file1 = prev_read_headers[header]
                            else:
                                read_file1 = path
                                read_file2 = prev_read_headers[header]

                if(matches >= min_match):
                    if(read_file2):
                        paired[read_file1] = read_file2
                        del single[read_file2]
                    else:
                        paired[read_file1] = path
                elif(not force_neighbour):
                    matches = 0
                    read_file1 = ""
                    read_file2 = ""

                    for header in file_headers:
                        if(header in old_read_headers):

                            pair_no = cls.detect_pair_no(
                                org_headers[header],
                                old_org_headers[header]
                            )

                            if(pair_no is None):
                                # Correct pair numbering cannot be obtained
                                # from SRA headers and some ENA headers.
                                # Attempt to get pair number from filenames.
                                filename1 = cls.get_read_filename(path)
                                filename2 = cls.get_read_filename(
                                    old_read_headers[header])
                                pair_no = cls.detect_pair_no(filename1,
                                                             filename2)

                            if(pair_no is not None):
                                matches += 1
                                if(pair_no == 2):
                                    read_file1 = old_read_headers[header]
                                else:
                                    read_file1 = path
                                    read_file2 = old_read_headers[header]

                            # Check if there are more than one match
                            if(read_file1 and read_file1 in paired):
                                print("Header matches multiple read files.\n\
                                       Header: " + header + "\n\
                                       File 1: " + read_file1 + "\n\
                                       File 2: " + paired[read_file1] + "\n\
                                       File 3: " + path + "\n\
                                       DONE!")
                                quit(1)

                    if(matches >= min_match):
                        if(read_file2):
                            paired[read_file1] = read_file2
                            if read_file2 in single:
                                del single[read_file2]
                        else:
                            paired[read_file1] = path
                    else:
                        single[path] = 1

                else:
                    single[path] = 1

                # Moves neighbor headers to old headers
                for header in prev_read_headers.keys():
                    old_read_headers[header] = prev_read_headers[header]
                    old_org_headers[header] = prev_org_headers[header]

                # Resets neighbors
                prev_read_headers = {}
                prev_org_headers = {}
                for header in file_headers:
                    prev_read_headers[header] = path
                    prev_org_headers[header] = org_headers[header]
            elif(is_fasta):
                assemblies[path] = 1
            else:
                if(ignore_errors):
                    continue
                else:
                    raise(SeqFormatError("Sequence format not recognised. "
                                         "Expected either FASTA or FASTQ. "
                                         "File: {}".format(path)))

        output_seq_files = []

        for path in file_paths:
            if(path in paired):
                seq_file = SeqFile(path, pe_file_reverse=paired[path],
                                   phred_format=phred)
                output_seq_files.append(seq_file)
            elif(path in single):
                seq_file = SeqFile(path, phred_format=phred)
                output_seq_files.append(seq_file)
            elif(path in assemblies):
                seq_file = SeqFile(path)
                output_seq_files.append(seq_file)

        return output_seq_files

    @staticmethod
    def detect_pair_no(header1, header2):
        ''' Given two fastq headers, will output if header1 is either 1 or 2.
            If the headers do not match, method will return None
        '''
        head_diff = ndiff(header1, header2)
        mismatches = 0
        pair_no = None

        for s in head_diff:
            if(s[0] == "-"):
                if(s[2] == "2"):
                    pair_no = 2
                    mismatches += 1
                elif(s[2] == "1"):
                    pair_no = 1
                    mismatches += 1
                else:
                    mismatches += 1

        if(mismatches < 2):
            return pair_no
        else:
            return None

    @staticmethod
    def load_seq_files(file_paths):
        ''' Given a list of file paths, returns a list of SeqFile objects.
        '''
        file_paths_str = " ".join(file_paths)

        # Running parse_input
        # TODO: Should be rewritten as proper python class.
        parse_input_cmd = ("perl parse_input.pl " + file_paths_str
                           + " > parse_input.output.txt")
        print("PARSE CMD: " + parse_input_cmd)
        try:
            subprocess.check_call([parse_input_cmd], shell=True)
        except subprocess.CalledProcessError:
            print("ERROR: parse input call failed")
            print("CMD that failed: " + parse_input_cmd)
            quit(1)

        file_list = []

        with open("parse_input.output.txt", "r", encoding="utf-8") as input_fh:
            for line in input_fh:
                entries = line.split("\t")
                if(entries[0] == "paired"):
                    print("PAIRED")
                    file_list.append(
                        SeqFile(seqfile=entries[1].strip(),
                                pe_file_reverse=entries[3].strip(),
                                phred_format=entries[2].strip()))
                elif(entries[0] == "single"):
                    print("SINGLE")
                    file_list.append(SeqFile(seqfile=entries[1].strip(),
                                             phred_format=entries[2].strip()))
                elif(entries[0] == "assembled"):
                    print("ASSEMBLED")
                    file_list.append(SeqFile(seqfile=entries[1].strip()))

        return file_list

    @staticmethod
    def checksum(fname, method="sha1"):

        if(method == "sha1"):
            hash = hashlib.sha1()
        elif(method == "md5"):
            hash = hashlib.md5()

        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash.update(chunk)

        return hash.hexdigest()


if __name__ == '__main__':

    #
    # Handling arguments
    #
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("-c", "--concat",
                        help="Concat NextSeq split fastq files.",
                        nargs="+",
                        default=None,
                        metavar="FASTQ")
    parser.add_argument("-co", "--concat_outdir",
                        help="Concatenated files will be stored here.",
                        default=".",
                        metavar="DIR")
    parser.add_argument("--test",
                        help="Test module. Not complete!",
                        action="store_true",
                        default=False)

    args = parser.parse_args()

    if(args.test):
        test_dir = os.path.dirname(os.path.realpath(__file__)) + "/test/"

        #
        # Testing paired-end data
        #
        test_fq_files = [
            test_dir + "DTU2017-1116-PRJ1066-CPH-Sewage-142_R2_001.fq.gz",
            test_dir + "DTU2017-1115-PRJ1066-CPH-Sewage-141_R2_001.fq.gz",
            test_dir + "DTU2017-1115-PRJ1066-CPH-Sewage-141_R1_001.fq.gz",
            test_dir + "DTU2017-1116-PRJ1066-CPH-Sewage-142_R1_001.fq.gz",
            test_dir + "ERR1512990_2.fastq.gz",
            test_dir + "ERR1512999_2.fastq.gz",
            test_dir + "ERR1512999_1.fastq.gz",
            test_dir + "ERR1512990_1.fastq.gz",
            test_dir + "SRR5633389_2.fastq.gz",
            test_dir + "SRR5827429_2.fastq.gz",
            test_dir + "SRR5827429_1.fastq.gz",
            test_dir + "SRR5633389_1.fastq.gz",
            test_dir + "SRR5063020.fastq.gz"
        ]

        seqfiles = SeqFile.parse_files(test_fq_files)

        errors_found = False

        for fq_file in seqfiles:

            # Check pairring
            if(fq_file.seq_format == "paired"):
                if(fq_file.filename[0:12] == "DTU2017-1115"):
                    if(fq_file.filename_reverse[0:12] != "DTU2017-1115"):
                        eprint("ERROR! Incorrect pairring.")
                        errors_found = True

                    # Check checksums
                    sha1 = SeqFile.checksum(fq_file.path)
                    md5 = SeqFile.checksum(fq_file.path, method="md5")

                    if(sha1 != "7a229efa8562cfe1b89bd026ae6d8f3d3e069708"):
                        eprint("ERROR! Incorrect SHA1 checsum.")
                        errors_found = True
                    if(md5 != "774dee8195bfdcf9f90e5a4fda385e90"):
                        eprint("ERROR! Incorrect MD5 checsum.")
                        errors_found = True

                elif(fq_file.filename[0:12] == "DTU2017-1116"):
                    if(fq_file.filename_reverse[0:12] != "DTU2017-1116"):
                        eprint("ERROR! Incorrect pairring.")
                        errors_found = True

                elif(fq_file.filename[0:10] != fq_file.filename_reverse[0:10]):
                    eprint("ERROR! Incorrect pairring.")
                    errors_found = True

                # Check pair numbers
                if(fq_file.filename[-6:-4] != "R1"
                   and fq_file.filename[-2:] != "_1"):
                    eprint("ERROR! Incorrect pair numbering.")
                    errors_found = True

                if(fq_file.filename_reverse[-6:-4] != "R2"
                   and fq_file.filename_reverse[-2:] != "_2"):
                    eprint("ERROR! Incorrect pair numbering.")
                    errors_found = True

                pair_no = SeqFile.predict_pair(fq_file.path)
                pair_no_rev = SeqFile.predict_pair(fq_file.pe_file_reverse)
                if(pair_no != 1 or pair_no_rev != 2):
                    eprint("ERROR! Incorrect pair no prediction.")
                    eprint("\t Expected forward 1 == " + str(pair_no))
                    eprint("\t Expected reverse 2 == " + str(pair_no_rev))
                    errors_found = True

            else:
                if(fq_file.filename[0:10] != "SRR5063020"):
                    eprint("ERROR! Incorrect single-end label.")
                    errors_found = True

                pair_no = SeqFile.predict_pair(fq_file.path)
                if(pair_no != 0):
                    eprint("ERROR! Incorrect pair no prediction.")
                    eprint("\t Expected single end 0 == " + str(pair_no))
                    errors_found = True

        if(errors_found):
            eprint("Test: Failed")
            sys.exit(1)
        else:
            eprint("Test: OK")
            sys.exit(0)

    if(args.concat):
        args.concat_outdir = os.path.abspath(args.concat_outdir)
        os.makedirs(args.concat_outdir, exist_ok=True)
        SeqFile.concat_fastqs(args.concat, out_path=args.concat_outdir)
