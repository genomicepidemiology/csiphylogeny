task SNPCall {

    # Array[String] inputSample

    String sampleName
    String inputPath1
    String seqType
    String? inputPath2

    # SNP caller options
    Int filterPrune
	Int filterSNPQuality
	Int filterMapQuality
	Float filterZ
	Int filterRelativeDepth
    Boolean filterHeterozygous
	Int minDepth
    Int minAssemblyLength
    Int minAssemblyEndDistance

    String outDir
	String reference

    # Application paths
    String snpCaller
    String vcfutils
  	String bwa
  	String fastatool
  	String genomeCoverageBed
  	String samtools
  	String bcftools
  	String perl
    String snpCallerAssembly
    String nucmer
    String show_snps
    String show_coords
    String delta_filter

    # Not used directly but needed in order to run samtools.
    File samtoolsIndexFile

    command <<<
        inputArgsRaw="-fp ${filterPrune} \
            -fsq ${filterSNPQuality} \
            -fmq ${filterMapQuality} \
            -fz ${filterZ} \
            -frd ${filterRelativeDepth} \
            -md ${minDepth} \
            -r ${reference} \
            -S ${outDir}/${sampleName}.sam \
            -cl \
            --vcfutils ${vcfutils} \
            --bwa ${bwa} \
            --fastatool ${fastatool} \
            --genomeCoverageBed ${genomeCoverageBed} \
            --samtools ${samtools} \
            --bcftools ${bcftools} \
            --perl ${perl}
            --quiet "

        inputArgsAssembly="-p ${filterPrune} \
            -r ${reference} \
            --working_dir ${outDir} \
            --ignored \
            --end_distance ${minAssemblyEndDistance} \
            --length ${minAssemblyLength} \
            --nucmer ${nucmer} \
            --show_snps ${show_snps} \
            --show_coords ${show_coords} \
            --delta_filter ${delta_filter} \
            --fastatool ${fastatool} "

        if [ "${filterHeterozygous}" = "true" ]
        then
            inputArgsRaw+="--filter_heterozygous "
        fi

        if [ "${seqType}" = "assembly" ]
        then
            ${perl} ${snpCallerAssembly} $inputArgsAssembly ${inputPath1}
        fi

        if [ "${seqType}" = "paired" ] || [ "${seqType}" = "single" ]
        then
            ${perl} ${snpCaller} $inputArgsRaw ${inputPath1} ${inputPath2}
        fi

    >>>

    runtime {
        memory_mb: 3900.0
        cpu: 2
        walltime: "18:00:00"
    }

    output {
        File ignored_snps_file = "${outDir}/${sampleName}.ignored_snps.txt"
    }

}
