task FilterSNPs {

    Array[File] ignoredSnpsFiles
    String snpFilter
    Int filterSNPQuality
    Int filterMapQuality
    Int filterRelativeDepth
    Int minDepth
    String outDir
    String perl

    command <<<
        ${perl} ${snpFilter} \
            --diff \
            --redundant \
            --relative_depth ${filterRelativeDepth} \
            --min_depth ${minDepth} \
            --quality ${filterSNPQuality} \
            -mq ${filterMapQuality} \
            --output_dir ${outDir} \
            ${sep=" " ignoredSnpsFiles}
    >>>

    runtime {
        memory_mb: 3900.0
        cpu: 1
        walltime: "1:00:00:00"
    }

    output {
        Array[File] filteredVCFs = glob("${outDir}/*.vcf")
        File snpMatrix = "${outDir}/pairwise_snp_matrix.txt"
    }

}
