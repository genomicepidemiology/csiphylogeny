task IndexReference {

    String reference
    String aligner
    String alignerPath
    String samtoolsPath

    command <<<
        if [ "${aligner}" = "bwa" ]
        then
            ${alignerPath} index ${reference}
            # TODO: Check that bwt file exists
        fi

        ${samtoolsPath} faidx ${reference}
    >>>

    runtime {
        memory_mb: 3900.0
        cpu: 1
        walltime: "1:00:00"
    }

    output {
        File samtoolsIndexFile = "${reference}.fai"
    }
}
