task GenomeIntersection {

    Array[File] ignoredSnpsFiles
    String intersection
    String outDir
    String perl

    command <<<
        ${perl} ${intersection} \
            --output ${outDir}/snp \
            ${sep=" " ignoredSnpsFiles}
    >>>

    runtime {
        memory_mb: 1900.0
        cpu: 1
        walltime: "2:00:00"
    }

    output {
        File intersect_hist = "${outDir}/snp.genome_cov.hist.txt"
        File intersect_result = "${outDir}/snp.genome_cov.results.txt"
    }

}
