task SNPMatrix {
    String perl
    String snpMatrix
    String outDir
    File fastaAln

    command <<<
        ${perl} ${snpMatrix} -i ${fastaAln} -o ${outDir}/snp_matrix.txt

    >>>

    runtime {
        memory_mb: 1900.0
        cpu: 1
        walltime: "1:00:00"
    }

    output {
        File ignored_snps_file = "${outDir}/snp_matrix.txt"
    }

}
