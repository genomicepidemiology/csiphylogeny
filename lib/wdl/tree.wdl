task InferTree {

    Array[File] vcfs
    String tempDir
    String outDir
    String perl
    String snp_vcf2fasta
    String fastTree

    command <<<
        ${perl} ${snp_vcf2fasta} \
            --temp_dir ${tempDir} \
            --count 1 \
            --output "${outDir}/snp" \
            ${sep=" " vcfs}

        ${fastTree} -nt -gtr ${outDir}/snp.aln.fa > ${outDir}/snp_tree.newick
    >>>

    runtime {
        memory_mb: 3900.0
        cpu: 1
        walltime: "12:00:00"
    }

    output {
        File snpAln = "${outDir}/snp.aln.fa"
        File snpTree = "${outDir}/snp_tree.newick"
    }

}
