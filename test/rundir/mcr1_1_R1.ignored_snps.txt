# File containing SNPs that was filtered out
# Reference: /home/projects/cge/people/rkmo/temp/csi/test/data/cluster01/ref.fa
# Sample(s): /home/projects/cge/people/rkmo/temp/csi/test/data/cluster01/mcr1_1_R1.fq.gz, /home/projects/cge/people/rkmo/temp/csi/test/data/cluster01/mcr1_1_R2.fq.gz
# Sorted BAM file: /home/projects/cge/people/rkmo/temp/csi/test/rundir//mcr1_1_R1.sorted.bam
# VCF file of kept SNPs: /home/projects/cge/people/rkmo/temp/csi/test/rundir//mcr1_1_R1.flt.vcf
# Coverage file: /home/projects/cge/people/rkmo/temp/csi/test/rundir//mcr1_1_R1.coverage.txt.gz
# Coverage mean: 27.1137761377614
# Coverage median: 29
# Coverage standard deviation: 9.2293162148421
# SNPs filtered on qual column: 0
# SNPs filtered on Z-score: 0
# SNPs filtered on 0/1 flag in VCF file: 0
# INACTIVE FILTER: SNPs filtered on >y*10: 0
# INACTIVE FILTER: SNPs filtered on >y*5: 0
# INACTIVE FILTER: Ambigious SNPs filtered: 0
# Pruning set to: 10
# SNPs pruned: 0
# INDELS removed: 0
# Valid positions: 1504 (92.4969249692497%)
# Reference positions missing: 4
# Reference positions with insufficient depth: 107
# Reference positions with insufficient ref/alt Q13 base depth: 11
mcr-1.1_1_KP347127	1584
mcr-1.1_1_KP347127	1590
mcr-1.1_1_KP347127	1613
mcr-1.1_1_KP347127	21
mcr-1.1_1_KP347127	1610
mcr-1.1_1_KP347127	29
mcr-1.1_1_KP347127	1566
mcr-1.1_1_KP347127	1607
mcr-1.1_1_KP347127	1596
mcr-1.1_1_KP347127	1591
mcr-1.1_1_KP347127	1548
mcr-1.1_1_KP347127	1559
mcr-1.1_1_KP347127	1593
mcr-1.1_1_KP347127	1619
mcr-1.1_1_KP347127	1608
mcr-1.1_1_KP347127	23
mcr-1.1_1_KP347127	47
mcr-1.1_1_KP347127	11
mcr-1.1_1_KP347127	1620
mcr-1.1_1_KP347127	20
mcr-1.1_1_KP347127	1616
mcr-1.1_1_KP347127	40
mcr-1.1_1_KP347127	1570
mcr-1.1_1_KP347127	1617
mcr-1.1_1_KP347127	28
mcr-1.1_1_KP347127	2
mcr-1.1_1_KP347127	18
mcr-1.1_1_KP347127	1615
mcr-1.1_1_KP347127	15
mcr-1.1_1_KP347127	1556
mcr-1.1_1_KP347127	24
mcr-1.1_1_KP347127	39
mcr-1.1_1_KP347127	1554
mcr-1.1_1_KP347127	1560
mcr-1.1_1_KP347127	1581
mcr-1.1_1_KP347127	4
mcr-1.1_1_KP347127	46
mcr-1.1_1_KP347127	27
mcr-1.1_1_KP347127	1567
mcr-1.1_1_KP347127	1579
mcr-1.1_1_KP347127	26
mcr-1.1_1_KP347127	48
mcr-1.1_1_KP347127	1572
mcr-1.1_1_KP347127	1550
mcr-1.1_1_KP347127	1599
mcr-1.1_1_KP347127	1582
mcr-1.1_1_KP347127	1589
mcr-1.1_1_KP347127	1
mcr-1.1_1_KP347127	35
mcr-1.1_1_KP347127	1576
mcr-1.1_1_KP347127	1564
mcr-1.1_1_KP347127	1588
mcr-1.1_1_KP347127	1557
mcr-1.1_1_KP347127	32
mcr-1.1_1_KP347127	1618
mcr-1.1_1_KP347127	34
mcr-1.1_1_KP347127	1600
mcr-1.1_1_KP347127	5
mcr-1.1_1_KP347127	1598
mcr-1.1_1_KP347127	1614
mcr-1.1_1_KP347127	1561
mcr-1.1_1_KP347127	6
mcr-1.1_1_KP347127	1580
mcr-1.1_1_KP347127	13
mcr-1.1_1_KP347127	25
mcr-1.1_1_KP347127	14
mcr-1.1_1_KP347127	1609
mcr-1.1_1_KP347127	1601
mcr-1.1_1_KP347127	3
mcr-1.1_1_KP347127	1563
mcr-1.1_1_KP347127	1555
mcr-1.1_1_KP347127	1585
mcr-1.1_1_KP347127	12
mcr-1.1_1_KP347127	31
mcr-1.1_1_KP347127	1587
mcr-1.1_1_KP347127	43
mcr-1.1_1_KP347127	1575
mcr-1.1_1_KP347127	1573
mcr-1.1_1_KP347127	1578
mcr-1.1_1_KP347127	30
mcr-1.1_1_KP347127	1621
mcr-1.1_1_KP347127	1602
mcr-1.1_1_KP347127	16
mcr-1.1_1_KP347127	36
mcr-1.1_1_KP347127	1595
mcr-1.1_1_KP347127	9
mcr-1.1_1_KP347127	42
mcr-1.1_1_KP347127	1604
mcr-1.1_1_KP347127	8
mcr-1.1_1_KP347127	1594
mcr-1.1_1_KP347127	1597
mcr-1.1_1_KP347127	1574
mcr-1.1_1_KP347127	7
mcr-1.1_1_KP347127	41
mcr-1.1_1_KP347127	10
mcr-1.1_1_KP347127	1568
mcr-1.1_1_KP347127	38
mcr-1.1_1_KP347127	1531
mcr-1.1_1_KP347127	1565
mcr-1.1_1_KP347127	1569
mcr-1.1_1_KP347127	22
mcr-1.1_1_KP347127	1592
mcr-1.1_1_KP347127	1552
mcr-1.1_1_KP347127	45
mcr-1.1_1_KP347127	1583
mcr-1.1_1_KP347127	1611
mcr-1.1_1_KP347127	1562
mcr-1.1_1_KP347127	50
mcr-1.1_1_KP347127	37
mcr-1.1_1_KP347127	1605
mcr-1.1_1_KP347127	1603
mcr-1.1_1_KP347127	1612
mcr-1.1_1_KP347127	1577
mcr-1.1_1_KP347127	33
mcr-1.1_1_KP347127	19
mcr-1.1_1_KP347127	1606
mcr-1.1_1_KP347127	1558
mcr-1.1_1_KP347127	1586
mcr-1.1_1_KP347127	44
mcr-1.1_1_KP347127	17
mcr-1.1_1_KP347127	1546
mcr-1.1_1_KP347127	1571
