# File containing SNPs that was filtered out
# Reference: /home/projects/cge/people/rkmo/temp/csi/test/data/cluster01/ref.fa
# Sample(s): /home/projects/cge/people/rkmo/temp/csi/test/data/cluster01/mcr1_2b_R1.fq.gz, /home/projects/cge/people/rkmo/temp/csi/test/data/cluster01/mcr1_2b_R2.fq.gz
# Sorted BAM file: /home/projects/cge/people/rkmo/temp/csi/test/rundir//mcr1_2b_R1.sorted.bam
# VCF file of kept SNPs: /home/projects/cge/people/rkmo/temp/csi/test/rundir//mcr1_2b_R1.flt.vcf
# Coverage file: /home/projects/cge/people/rkmo/temp/csi/test/rundir//mcr1_2b_R1.coverage.txt.gz
# Coverage mean: 27.1211562115621
# Coverage median: 28
# Coverage standard deviation: 10.3871261214232
# SNPs filtered on qual column: 0
# SNPs filtered on Z-score: 0
# SNPs filtered on 0/1 flag in VCF file: 0
# INACTIVE FILTER: SNPs filtered on >y*10: 0
# INACTIVE FILTER: SNPs filtered on >y*5: 0
# INACTIVE FILTER: Ambigious SNPs filtered: 0
# Pruning set to: 10
# SNPs pruned: 0
# INDELS removed: 0
# Valid positions: 1471 (90.4674046740467%)
# Reference positions missing: 23
# Reference positions with insufficient depth: 124
# Reference positions with insufficient ref/alt Q13 base depth: 8
mcr-1.1_1_KP347127	45
mcr-1.1_1_KP347127	13
mcr-1.1_1_KP347127	1588
mcr-1.1_1_KP347127	48
mcr-1.1_1_KP347127	2
mcr-1.1_1_KP347127	1586
mcr-1.1_1_KP347127	73
mcr-1.1_1_KP347127	55
mcr-1.1_1_KP347127	1597
mcr-1.1_1_KP347127	84
mcr-1.1_1_KP347127	1580
mcr-1.1_1_KP347127	58
mcr-1.1_1_KP347127	62
mcr-1.1_1_KP347127	63
mcr-1.1_1_KP347127	1599
mcr-1.1_1_KP347127	66
mcr-1.1_1_KP347127	1563
mcr-1.1_1_KP347127	27
mcr-1.1_1_KP347127	53
mcr-1.1_1_KP347127	1559
mcr-1.1_1_KP347127	68
mcr-1.1_1_KP347127	1594
mcr-1.1_1_KP347127	1573
mcr-1.1_1_KP347127	4
mcr-1.1_1_KP347127	1556
mcr-1.1_1_KP347127	1567
mcr-1.1_1_KP347127	43
mcr-1.1_1_KP347127	1592
mcr-1.1_1_KP347127	44
mcr-1.1_1_KP347127	91
mcr-1.1_1_KP347127	41
mcr-1.1_1_KP347127	1571
mcr-1.1_1_KP347127	20
mcr-1.1_1_KP347127	1608
mcr-1.1_1_KP347127	12
mcr-1.1_1_KP347127	39
mcr-1.1_1_KP347127	77
mcr-1.1_1_KP347127	64
mcr-1.1_1_KP347127	18
mcr-1.1_1_KP347127	52
mcr-1.1_1_KP347127	1584
mcr-1.1_1_KP347127	1569
mcr-1.1_1_KP347127	1600
mcr-1.1_1_KP347127	59
mcr-1.1_1_KP347127	25
mcr-1.1_1_KP347127	92
mcr-1.1_1_KP347127	1564
mcr-1.1_1_KP347127	51
mcr-1.1_1_KP347127	54
mcr-1.1_1_KP347127	87
mcr-1.1_1_KP347127	75
mcr-1.1_1_KP347127	40
mcr-1.1_1_KP347127	1572
mcr-1.1_1_KP347127	70
mcr-1.1_1_KP347127	49
mcr-1.1_1_KP347127	1579
mcr-1.1_1_KP347127	1566
mcr-1.1_1_KP347127	1598
mcr-1.1_1_KP347127	61
mcr-1.1_1_KP347127	34
mcr-1.1_1_KP347127	1575
mcr-1.1_1_KP347127	22
mcr-1.1_1_KP347127	6
mcr-1.1_1_KP347127	38
mcr-1.1_1_KP347127	26
mcr-1.1_1_KP347127	32
mcr-1.1_1_KP347127	93
mcr-1.1_1_KP347127	1577
mcr-1.1_1_KP347127	29
mcr-1.1_1_KP347127	31
mcr-1.1_1_KP347127	1613
mcr-1.1_1_KP347127	94
mcr-1.1_1_KP347127	36
mcr-1.1_1_KP347127	86
mcr-1.1_1_KP347127	1603
mcr-1.1_1_KP347127	5
mcr-1.1_1_KP347127	1593
mcr-1.1_1_KP347127	57
mcr-1.1_1_KP347127	90
mcr-1.1_1_KP347127	71
mcr-1.1_1_KP347127	7
mcr-1.1_1_KP347127	1589
mcr-1.1_1_KP347127	1607
mcr-1.1_1_KP347127	1581
mcr-1.1_1_KP347127	1585
mcr-1.1_1_KP347127	9
mcr-1.1_1_KP347127	21
mcr-1.1_1_KP347127	1
mcr-1.1_1_KP347127	33
mcr-1.1_1_KP347127	85
mcr-1.1_1_KP347127	98
mcr-1.1_1_KP347127	1570
mcr-1.1_1_KP347127	1606
mcr-1.1_1_KP347127	1590
mcr-1.1_1_KP347127	1601
mcr-1.1_1_KP347127	1587
mcr-1.1_1_KP347127	1574
mcr-1.1_1_KP347127	65
mcr-1.1_1_KP347127	1595
mcr-1.1_1_KP347127	50
mcr-1.1_1_KP347127	82
mcr-1.1_1_KP347127	16
mcr-1.1_1_KP347127	47
mcr-1.1_1_KP347127	1605
mcr-1.1_1_KP347127	1596
mcr-1.1_1_KP347127	1614
mcr-1.1_1_KP347127	1578
mcr-1.1_1_KP347127	15
mcr-1.1_1_KP347127	1604
mcr-1.1_1_KP347127	76
mcr-1.1_1_KP347127	1562
mcr-1.1_1_KP347127	69
mcr-1.1_1_KP347127	1557
mcr-1.1_1_KP347127	1582
mcr-1.1_1_KP347127	1610
mcr-1.1_1_KP347127	88
mcr-1.1_1_KP347127	11
mcr-1.1_1_KP347127	3
mcr-1.1_1_KP347127	42
mcr-1.1_1_KP347127	1583
mcr-1.1_1_KP347127	1612
mcr-1.1_1_KP347127	100
mcr-1.1_1_KP347127	79
mcr-1.1_1_KP347127	28
mcr-1.1_1_KP347127	1549
mcr-1.1_1_KP347127	17
mcr-1.1_1_KP347127	14
mcr-1.1_1_KP347127	1576
mcr-1.1_1_KP347127	1568
mcr-1.1_1_KP347127	24
mcr-1.1_1_KP347127	95
mcr-1.1_1_KP347127	37
mcr-1.1_1_KP347127	46
mcr-1.1_1_KP347127	89
mcr-1.1_1_KP347127	1554
mcr-1.1_1_KP347127	67
mcr-1.1_1_KP347127	1602
mcr-1.1_1_KP347127	83
mcr-1.1_1_KP347127	19
mcr-1.1_1_KP347127	23
mcr-1.1_1_KP347127	80
mcr-1.1_1_KP347127	78
mcr-1.1_1_KP347127	30
mcr-1.1_1_KP347127	74
mcr-1.1_1_KP347127	72
mcr-1.1_1_KP347127	1609
mcr-1.1_1_KP347127	60
mcr-1.1_1_KP347127	56
mcr-1.1_1_KP347127	81
mcr-1.1_1_KP347127	10
mcr-1.1_1_KP347127	8
mcr-1.1_1_KP347127	1565
mcr-1.1_1_KP347127	1591
mcr-1.1_1_KP347127	35
mcr-1.1_1_KP347127	1611
